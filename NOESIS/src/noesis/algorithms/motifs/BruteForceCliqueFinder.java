package noesis.algorithms.motifs;

import java.util.HashSet;
import java.util.Set;

import noesis.Network;

/**
 * Brute force clique finder.
 * 
 * @author Victor Martinez
 * 
 */
public class BruteForceCliqueFinder extends CliqueFinder 
{

	@Override
	public Set<Set<Integer>> compute(Network network) 
	{
    	Set<Set<Integer>> cliques = new HashSet<Set<Integer>>();
    	Set<Set<Integer>> currentCliques = new HashSet<Set<Integer>>();
    	
    	// One-node cliques
    	for (int node=0; node<network.nodes(); node++) {
    		Set<Integer> clique = createClique(node);
    		cliques.add(clique);
    		currentCliques.add(clique);
    	}
    	
    	// Expand cliques
    	
    	while(!currentCliques.isEmpty()) {
    	
    		Set<Set<Integer>> nextCliques = new HashSet<Set<Integer>>();
    		
    		for (int node=0; node<network.nodes(); node++) {
    			for(Set<Integer> clique: currentCliques) {
    				if(!clique.contains(node) && isClique(network, clique, node)) {
    					Set<Integer> newClique = createClique(clique, node);
    					nextCliques.add(newClique);
    					cliques.add(newClique);
    				}    				
    			}
    		}
    		
    		currentCliques = nextCliques;
    	}
    	
    	return cliques;
    }
}
