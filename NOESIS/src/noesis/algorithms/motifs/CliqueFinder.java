package noesis.algorithms.motifs;

import java.util.HashSet;
import java.util.Set;

import noesis.Network;

/**
 * Clique finder base class.
 * 
 * @author Victor Martinez
 * 
 */
public abstract class CliqueFinder 
{    
    public abstract Set<Set<Integer>> compute(Network network);
    
    // Utility functions
    
    protected static boolean isClique (Network network, Set<Integer> clique, int newNode) 
    {
    	for (int node: clique)
    		if (!network.contains(newNode, node) && !network.contains(node, newNode))
    			return false;
    	
    	return true;
    }
    
    protected static Set<Integer> createClique (int... elements) 
    {
    	Set<Integer> clique = new HashSet<Integer>();
    
    	for (int element: elements) 
    		clique.add(element);
    	
    	return clique;
    }
    
    protected static Set<Integer> createClique (Set<Integer> clique, int element) 
    {
    	Set<Integer> newClique = new HashSet<Integer>(clique);
    	
    	newClique.add(element);
    	
    	return newClique;
    }
}
