package noesis.algorithms.motifs;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import noesis.Network;

/**
 * Bron-Kerbosch clique finder.
 * 
 * @author Victor Martinez
 * 
 */
public class BronKerboschCliqueFinder extends CliqueFinder 
{	
	@Override
	public Set<Set<Integer>> compute (Network network) 
	{
		Set R = new HashSet();
		Set X = new HashSet();
		
		Set<Integer> P = new HashSet<Integer>();
		
		for (int node=0; node<network.nodes(); node++)
			P.add(node);
		
		// Compute maximal cliques
    	
		Set<Set<Integer>> maxCliques = new HashSet<Set<Integer>>();
    	
		recursiveBronKerbosch(network, maxCliques, R, X, P);

		// Add subcliques
	
		Set<Set<Integer>> cliques = new HashSet<Set<Integer>>();
		
		for (Set<Integer> maxClique: maxCliques)
			addSubcliques(cliques, maxClique);
		
    	return cliques;
    }

	protected void recursiveBronKerbosch (Network network, Set<Set<Integer>> cliques, Set<Integer> R, Set<Integer> X, Set<Integer> P)
	{
		if (P.isEmpty() && X.isEmpty() && !R.isEmpty())
			cliques.add(R);
		
		Iterator<Integer> iter = P.iterator();
		
		while (iter.hasNext()) {
			
			int node = iter.next();
			
			Set<Integer> RUnode = createClique(R, node);
			Set<Integer> PN = neighIntersection(network, P, node);
			Set<Integer> XN = neighIntersection(network, X, node);
			
			recursiveBronKerbosch(network, cliques, RUnode, PN, XN);
			iter.remove();
			X.add(node);
		}
	}
	
	protected Set<Integer> neighIntersection(Network network, Set<Integer> set, int node) 
	{
		Set<Integer> newSet = new HashSet<Integer>();
	
		for (int link=0; link<network.outDegree(node); link++) {
			int neighbor = network.outLink(node, link);
			
			if (set.contains(neighbor)) 
				newSet.add(neighbor);
		}
		
		for (int link=0; link<network.inDegree(node); link++) {
			int neighbor = network.inLink(node, link);
			
			if (set.contains(neighbor)) 
				newSet.add(neighbor);
		}
		
		return newSet;
	}
	
	protected void addSubcliques (Set<Set<Integer>> cliques, Set<Integer> clique) 
	{
		if (!clique.isEmpty()) 
			cliques.add(clique);
		
        for (Integer node: clique) {
        	Set<Integer> newSet = setWithoutElem(clique, node);
        	addSubcliques(cliques, newSet);
        }
    }
	
	protected static Set<Integer> setWithoutElem (Set<Integer> set, int element) 
	{
    	Set<Integer> newSet = new HashSet<Integer>(set);
    	
    	newSet.remove(element);
    	
    	return newSet;
    }
}
