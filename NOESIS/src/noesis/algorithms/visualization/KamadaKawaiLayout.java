package noesis.algorithms.visualization;

import ikor.math.random.Random;
import noesis.LinkEvaluator;
import noesis.Network;
import noesis.algorithms.paths.AllPairsDijkstra;

/**
 * Kamada-Kawai graph layout algorithm
 * 
 * Kamada, T., & Kawai, S. (1989):
 * "An algorithm for drawing general undirected graphs" 
 * Information processing letters, 31(1):7-15
 * 
 * @author Victor Martinez (victormg@acm.org)
 */

public class KamadaKawaiLayout extends IterativeNetworkLayout 
{

	private static final int MAX_ITERATIONS = 1000;
	private static final double EPSILON = 0.0001;
	private static final double K = 1.0;
	
	// Number of iterations to check convergence
	private static final int WAIT_ITERATIONS = 50;
	
	public static final double MIN_TEMPERATURE = 0.005;
	public static final double INITIAL_TEMPERATURE = 1;
	public static final double COOLING_FACTOR = 0.95; // 0.95^100 ~ 0.005
	
	// Ratio of the diameter for disconnected pairs of nodes to regulate disconnected components distance
	private static final double DISCONNECTED_RATIO = 0.75;
	
	private int iteration;
	private double displaySideLength;
	private double distances[][];
	private double L;
	private double l[][];
	private double k[][];
	private double px[];
	private double py[];
	private boolean finished;
	private int waitedIterations;
	private double bestDelta;
	
	@Override
	public void init() 
	{
		waitedIterations = 0;
		bestDelta = Double.POSITIVE_INFINITY;
		
		// Compute node distances and diameter
		AllPairsDijkstra<?, ?> dijkstra = new AllPairsDijkstra(network, new ExistingLinkEvaluator(network));
		dijkstra.run();
		distances = dijkstra.distance();
		double diameter = 0;
		for (int i = 0; i < network.nodes(); i++)
			for (int j = i + 1; j < network.nodes(); j++)
				if (diameter < distances[i][j] && Double.isFinite(distances[i][j]))
					diameter = distances[i][j];
		
		for (int sourceNode = 0; sourceNode < network.nodes(); sourceNode++)
            for (int targetNode = 0; targetNode < network.nodes(); targetNode++) {
                if(distances[sourceNode][targetNode]==Double.POSITIVE_INFINITY)
                    distances[sourceNode][targetNode] = diameter*DISCONNECTED_RATIO;
            }
		
		// Get display size
		displaySideLength = 1 - 2 * MARGIN;

		// Compute L
		L = displaySideLength / diameter;

		// Compute l_ij and k_ij
		l = new double[network.nodes()][network.nodes()];
		k = new double[network.nodes()][network.nodes()];

		for (int sourceNode = 0; sourceNode < network.nodes(); sourceNode++) {
			for (int targetNode = 0; targetNode < network.nodes(); targetNode++) {
				if (sourceNode != targetNode) {
					l[sourceNode][targetNode] = L
							* distances[sourceNode][targetNode];
					k[sourceNode][targetNode] = K
							/ (distances[sourceNode][targetNode] * distances[sourceNode][targetNode]);
				}
			}
		}

		// Initialize node particles positions
		px = new double[network.size()];
		py = new double[network.size()];

		for (int i = 0; i < network.size(); i++) {
			px[i] = Random.random();
			py[i] = Random.random();
		}

		finished = false;
		iteration = 0;
	}

	@Override
	public boolean stop() 
	{
		return finished || !(iteration < MAX_ITERATIONS);
	}

	@Override
	public void iterate() 
	{
		finished = true;

		// Compute max delta
		double maxDelta = 0;
		double currentDelta = 0;
		int m = -1;
		for (int i = 0; i < network.nodes(); i++) {
			double diffX = diffX(i);
			double diffY = diffY(i);
			currentDelta = delta(i, diffX, diffY);
			if (currentDelta > maxDelta) {
				maxDelta = currentDelta;
				m = i;
			}
		}

		if (waitedIterations < WAIT_ITERATIONS &&
			maxDelta > EPSILON &&
			iteration < MAX_ITERATIONS) {

			if(maxDelta < bestDelta) {
				waitedIterations = 0;
				bestDelta = maxDelta;
			} else {
				waitedIterations++;
			}
			
			finished = false;
			int internalIterations = 0;
			double delta = 0;
			double diffXm = diffX(m);
			double diffYm = diffY(m);
			
			delta = delta(m, diffXm, diffYm);
			
			double localTemperature = INITIAL_TEMPERATURE;
			while (localTemperature > MIN_TEMPERATURE &&
					delta > EPSILON &&
					internalIterations < MAX_ITERATIONS) {

				// Compute system coefficients
				double diffX2m = 0;
				double diffY2m = 0;
				double diffXYm = 0;

				for (int i = 0; i < network.nodes(); i++) {
					if (i != m) {
						double dx = px[m] - px[i];
						double dy = py[m] - py[i];

						
						double dist = Math.sqrt(dx * dx + dy * dy);
						double denominator = dist * dist * dist;

						diffX2m += k[m][i]
								* (1 - (l[m][i] * dy * dy) / denominator);
						diffY2m += k[m][i]
								* (1 - (l[m][i] * dy * dy) / denominator);
						diffXYm += k[m][i]
								* (l[m][i] * dx * dy) / denominator;
					}
				}

				// Solve system
				double oDenominator = diffX2m * diffY2m - diffXYm * diffXYm;
				double ox = ((-diffXm) * diffY2m - diffXYm * (-diffYm)) / oDenominator;
				double oy = (diffX2m * (-diffYm) - (-diffXm) * diffXYm) / oDenominator;
				
				// Update coords
				px[m] = px[m] + localTemperature * ox;
				py[m] = py[m] + localTemperature * oy;
				
				localTemperature = cool(localTemperature);
				
				diffXm = diffX(m);
				diffYm = diffY(m);
				delta = delta(m, diffXm, diffYm);
				
				internalIterations++;
			}

		}

		iteration++;
	}

	@Override
	public void end() 
	{
		// Final scale

		double minX = 1;
		double maxX = -1;
		double minY = 1;
		double maxY = -1;

		for (int i = 0; i < network.size(); i++) {

			if (px[i] < minX)
				minX = px[i];

			if (px[i] > maxX)
				maxX = px[i];

			if (py[i] < minY)
				minY = py[i];

			if (py[i] > maxY)
				maxY = py[i];
		}

		double scaleX;
		double scaleY;

		if (maxX > minX)
			scaleX = (1 - 2 * MARGIN) / (maxX - minX);
		else
			scaleX = 1.0;

		if (maxY > minY)
			scaleY = (1 - 2 * MARGIN) / (maxY - minY);
		else
			scaleY = 1.0;

		// Final (x,y) coordinates

		for (int i = 0; i < network.size(); i++) {
			x.set(i, MARGIN + scaleX * (px[i] - minX));
			y.set(i, MARGIN + scaleY * (py[i] - minY));
		}

		distances = null;
		l = null;
		k = null;
		px = null;
		py = null;
	}

	
	private double diffX(int m) 
	{
		double value = 0;
		for (int i = 0; i < network.nodes(); i++) {
			if (i != m) {
				double dx = px[m] - px[i];
				double dy = py[m] - py[i];
				value += k[m][i]
						* (dx - (l[m][i] * dx / Math.sqrt(dx * dx + dy * dy)));

			}
		}
		return value;
	}

	private double diffY(int m) 
	{
		double value = 0;
		for (int i = 0; i < network.nodes(); i++) {
			if (i != m) {
				double dx = px[m] - px[i];
				double dy = py[m] - py[i];
				value += k[m][i]
						* (dy - (l[m][i] * dy / Math.sqrt(dx * dx + dy * dy)));

			}
		}
		return value;
	}

	private double delta(int m, double diffXm, double diffYm) 
	{
		return Math.sqrt(diffXm * diffXm + diffYm * diffYm);
	}
	
	private double cool (double t)
	{
		return t*COOLING_FACTOR;
	}
	
	// Link evaluator
	
	class ExistingLinkEvaluator implements LinkEvaluator
	{
		private Network net;
		
		public ExistingLinkEvaluator (Network net)
		{
			this.net = net;
		}
		
		@Override
		public double evaluate (int source, int destination) 
		{
			return net.contains(source, destination)? 1: Double.POSITIVE_INFINITY;
		}
	}	
}
