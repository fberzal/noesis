package noesis;

// Title:       Parameter annotation
// Version:     1.0
// Copyright:   2014
// Author:      Fernando Berzal
// E-mail:      berzal@acm.org
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Parameter annotation
 * 
 * @author Fernando Berzal (berzal@acm.org)
 */

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface Parameter 
{    
    double min() default -Double.MAX_VALUE;
    
    double max() default Double.MAX_VALUE;
    
    double defaultValue() default 0; 
}