package noesis.ui.model.parameters;

import ikor.model.ui.Action;
import ikor.model.ui.Application;
import ikor.model.ui.Editor;
import ikor.model.ui.Option;
import ikor.model.ui.Selector;
import ikor.model.ui.UIModel;
import noesis.Attribute;
import noesis.analysis.structure.RobustnessCoefficient;
import noesis.ui.model.NetworkFigure;
import noesis.ui.model.NetworkModel;
import noesis.ui.model.NodeAttributeObserver;
import noesis.ui.model.actions.ForwardAction;
import noesis.ui.model.data.Report;
import noesis.ui.model.data.ReportUIModel;

/**
 * Robustness coefficient dialog.
 * 
 * @author Victor Martinez (victormg@acm.org)
 */

public class RobustnessCoefficientUIModel extends UIModel
{
	NetworkFigure figure;
	Selector      attributes;
	UIModel       buttons;
	
	public RobustnessCoefficientUIModel (Application app, NetworkModel model, NetworkFigure figure)
	{
		super(app, "Robustness coefficient options");
		
		setIcon( app.url("icon.gif") );

		this.figure = figure;
		
		// Nested panels
		
		UIModel panel = new UIModel(app, "Container panel");

		panel.setAlignment( UIModel.Alignment.TRAILING );
		
		attributes = new Selector();
		panel.add( attributes );

		buttons = new UIModel(app, "Button bar");
		panel.add( buttons );
		
		this.add(panel);
		
		// Buttons
		
		buttons.setAlignment( UIModel.Alignment.ADJUST );
	
		Editor<Boolean> descendingOrder = new Editor<Boolean>("Descending order", Boolean.class);
		buttons.add(descendingOrder);
		
		UIModel modelUI = this;
		Option color = new Option("Compute");
		color.setIcon( app.url("icon.gif") );
		color.setAction(
			new Action() {
				@Override
				public void run() {
					
					if (attributes.getSelectedCount()==0) 
						return; // TODO !!!

					String attr = attributes.getSelected().get(0).getId();
					Attribute nodeAttribute = figure.getNetwork().getNodeAttribute(attr);
					
					Boolean desc = descendingOrder.getData();
					if (desc==null) 
						desc = false;
					
					RobustnessCoefficient robustnessCoefficient = new RobustnessCoefficient(figure.getNetwork(), nodeAttribute, desc);
					
					Report report = new Report();
					report.add("Robustness coefficient", robustnessCoefficient.getRobustness());
					
					ReportUIModel robustnessUI = new ReportUIModel(getApplication(), "Network robustness", report);
					
					Action forward = new ForwardAction(robustnessUI);
					forward.run();
					modelUI.exit();
				}
			}
		);
		buttons.add(color);
		
		// Observer
		
		figure.addObserver( new NodeAttributeObserver(app,figure,attributes) );
	}
	
}
