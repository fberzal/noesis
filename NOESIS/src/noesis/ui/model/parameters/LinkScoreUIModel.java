package noesis.ui.model.parameters;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import ikor.model.data.RealModel;
import ikor.model.data.annotations.Description;
import ikor.model.ui.Action;
import ikor.model.ui.Application;
import ikor.model.ui.Editor;
import ikor.model.ui.Option;
import ikor.model.ui.UIModel;
import noesis.Parameter;
import noesis.ui.model.NetworkModel;

/**
 * Link prediction and scoring parameter dialog.
 * 
 * @author Victor Martinez (victormg@acm.org)
 */

public class LinkScoreUIModel extends UIModel 
{
	// TODO task.getParameters() !!!
	
	public LinkScoreUIModel(Application app, Class method,
			NetworkModel model, Class<? extends Action> actionClass, Field[] paramFields) 
	{
		super(app, "Parameters");
		
		List<Editor> editors = new ArrayList<Editor>();
		for(Field paramField : paramFields) {
			Parameter parameter = paramField.getAnnotation(Parameter.class);
			RealModel dataModel = new RealModel();
			dataModel.setMinimumValue(parameter.min());
			dataModel.setMaximumValue(parameter.max());
			
			Description description = paramField.getAnnotation(Description.class);
			String label = Objects.nonNull(description) ? description.value() : paramField.getName();
			Editor<Double> editor = new Editor<Double>(label, dataModel);
			editor.setIcon( app.url("icons/calculator.png") );
			editor.setData(parameter.defaultValue());
			add(editor);
			editors.add(editor);
		}
		
		Option ok = new Option("Apply");
		ok.setIcon( app.url("icon.gif") );
		Action action = null;
		try {
			action = actionClass.getConstructor(Application.class, Optional.class, 
				NetworkModel.class, Class.class, Optional.class)
			.newInstance(app, Optional.of(this), model, method, Optional.of(editors));
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		ok.setAction(action);
		add(ok);	
	}

}
