package noesis.ui.model.data.set;

import ikor.model.data.DataModel;
import ikor.model.data.Dataset;
import noesis.AttributeNetwork;

public abstract class NetworkDataset extends Dataset 
{
	protected AttributeNetwork network;
	
	
	public AttributeNetwork getNetwork() 
	{
		return network;
	}

	public void setNetwork(AttributeNetwork network) 
	{
		this.network = network;
	}

	
	public abstract String getName (int column);
	
	public abstract DataModel getModel (int column);
}