package noesis.ui.model.data.set;

import ikor.model.data.DataModel;
import noesis.AttributeNetwork;

public class LinkDataset extends NetworkDataset	
{
	private static final DataModel ID_MODEL = new ikor.model.data.TextModel();
	
	
	@Override
	public String getName (int column) 
	{
		AttributeNetwork network = getNetwork();
		
		if (column==0)
			return "source";
		else if (column==1)
			return "destination";
		else if ((network!=null) && (column-2<network.getLinkAttributeCount())) 
			return network.getLinkAttribute(column-2).getID();
		else
			return null;
	}

	@Override
	public DataModel getModel(int column) {
		AttributeNetwork network = getNetwork();
	
		if ((column==0) || (column==1))
			return ID_MODEL;
		else if (network!=null) 
			return network.getLinkAttribute(column-2).getModel();
		else
			return null;
	}
	
	
	@Override
	public Object get(int row, int column) 
	{
		AttributeNetwork network = getNetwork();
		Object data = null;
	
		if (network!=null)  {
			
			if (column==0) {
				// Source
				data = network.getNodeAttribute("id").get( network.source(row) );
			} else if (column==1) {
				// Destination
				data = network.getNodeAttribute("id").get( network.destination(row) );
			} else if (column-2<network.getLinkAttributeCount()) {
				// Link attribute
				data = network.getLinkAttribute(column-2).get(row);
			}
		}
		
		return data;
	}

	@Override
	public void set(int row, int column, Object object) 
	{
		AttributeNetwork network = getNetwork();

		if ((network!=null) && (column>1))
			network.getLinkAttribute(column-2).set(row, object);
	}

	@Override
	public int getColumnCount() 
	{
		AttributeNetwork network = getNetwork();

		if (network!=null)
			return 2 + network.getLinkAttributeCount();
		else
			return 0;
	}

	@Override
	public int getRowCount() 
	{
		AttributeNetwork network = getNetwork();

		if (network!=null)
			return network.links();
		else
			return 0;
	}
	
	
}
