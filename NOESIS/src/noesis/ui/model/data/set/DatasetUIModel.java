package noesis.ui.model.data.set;

import ikor.model.data.Dataset;
import ikor.model.ui.Application;
import ikor.model.ui.DatasetComponent;
import ikor.model.ui.DatasetSelection;
import ikor.model.ui.DatasetViewer;
import ikor.model.ui.Menu;
import ikor.model.ui.UIModel;

public class DatasetUIModel extends UIModel 
{
	protected DatasetViewer control;

	public DatasetUIModel (Application app, Dataset dataset, String title)
	{
		this(app, dataset, title, null);
	}

	public DatasetUIModel (Application app, Dataset dataset, String title, String[] columns) 
	{
		super(app, title);
		setIcon( app.url("icons/chart.png") );
		
		// Data control
		
		control = new DatasetViewer("Data viewer", dataset.getModel());

		if (columns!=null) {
			control.clearHeaders();
			for(String column : columns)
				control.addHeader(column);
		}

		control.setData(dataset);
		
		add(control);
		
		// Menu
		
	    Menu menu = new DatasetUIMenu(this); 
		
		add(menu);	
	}

	
	public final DatasetComponent getDatasetComponent() 
	{
		return control;
	}
	
	
	@Override
	public void start ()
	{
		DatasetSelection selection = control.getSelection();
		
		if (selection!=null) {
			selection.notifyObservers(selection);
		}
	}
	
}
