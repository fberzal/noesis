package noesis.ui.model.data.set;

import ikor.model.ui.Application;
import ikor.model.ui.Menu;
import ikor.model.ui.Option;
import ikor.model.ui.Separator;
import noesis.ui.model.actions.ExitAction;

/**
 * UI menu for the dataset UI
 * 
 * @author Victor Martinez (victormg@acm.org)
 */

public class DatasetUIMenu extends Menu {

	private Menu data;
	
	/**
	 * Constructor
	 * 
	 * @param ui UI Model
	 */
	public DatasetUIMenu (DatasetUIModel ui) 
	{
		super("Dataset Menu");
		
		Application app = ui.getApplication();
		
		data = createDataMenu(app, ui);

		this.add(data);
	}

	// Data menu
	// ---------

	private Menu createDataMenu (Application app, DatasetUIModel ui) 
	{
		Menu data = new Menu("Data");

		data.setIcon(app.url("icons/download.png"));

		Option save = new Option("Save", new DatasetUISaveAction(app, ui, ui.getDatasetComponent(), "csv"));
		save.setIcon(app.url("icons/download.png"));
		data.add(save);

		data.add(new Separator());

		Option exit = new Option("Exit", new ExitAction(ui));
		exit.setIcon(app.url("icons/exit.png"));
		data.add(exit);

		return data;
	}
}
