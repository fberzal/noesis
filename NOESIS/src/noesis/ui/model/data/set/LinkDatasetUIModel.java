package noesis.ui.model.data.set;

import ikor.model.ui.Application;
import ikor.model.ui.DatasetSelection;
import ikor.model.ui.DatasetViewer;
import noesis.ui.model.NetworkModel;


public class LinkDatasetUIModel extends DatasetUIModel 
{

	public LinkDatasetUIModel (Application app, NetworkModel data)
	{
		this(app, data, "Network links");
	}
	
	public LinkDatasetUIModel (Application app, NetworkModel data, String title) 
	{
		super(app, data.getLinkDataset(), title);
		
		data.addObserver( new LinkDatasetObserver(data, data.getLinkDataset(), control) );		
	}
	
	
	class LinkDatasetObserver extends NetworkDatasetObserver 
	{
		public LinkDatasetObserver(NetworkModel data, NetworkDataset dataset, DatasetViewer control) 
		{
			super(data, dataset, control);
		}

		@Override
		public DatasetSelection getSelection() 
		{
			return data.getLinkSelection();
		}		
	}
	
}
