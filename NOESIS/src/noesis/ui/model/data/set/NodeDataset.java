package noesis.ui.model.data.set;

import ikor.model.data.DataModel;
import noesis.AttributeNetwork;

public class NodeDataset extends NetworkDataset	
{
	@Override
	public String getName(int column) 
	{
		AttributeNetwork network = getNetwork();
		
		if (network!=null)
			return network.getNodeAttribute(column).getID();
		else
			return null;
	}

	@Override
	public DataModel getModel(int column) 
	{
		AttributeNetwork network = getNetwork();

		if (network!=null)
			return network.getNodeAttribute(column).getModel();
		else
			return null;
	}
	
	@Override
	public Object get(int row, int column) 
	{
		AttributeNetwork network = getNetwork();
		Object data = null;
	
		if ((network!=null) && (column<network.getNodeAttributeCount()) && (row<network.size()))
			data = network.getNodeAttribute(column).get(row);
		
		return data;
	}

	@Override
	public void set(int row, int column, Object object) 
	{
		AttributeNetwork network = getNetwork();

		if (network!=null)
			network.getNodeAttribute(column).set(row, object);
	}

	@Override
	public int getColumnCount() 
	{
		AttributeNetwork network = getNetwork();

		if (network!=null)
			return network.getNodeAttributeCount();
		else
			return 0;
	}

	@Override
	public int getRowCount() 
	{
		AttributeNetwork network = getNetwork();

		if (network!=null)
			return network.size();
		else
			return 0;
	}	
}