package noesis.ui.model.data.set;

import ikor.model.ui.Application;
import ikor.model.ui.DatasetSelection;
import ikor.model.ui.DatasetViewer;
import noesis.ui.model.NetworkModel;


public class NodeDatasetUIModel extends DatasetUIModel 
{		
	public NodeDatasetUIModel (Application app, NetworkModel data) 
	{
		super(app, data.getNodeDataset(), "Network nodes");
		
		data.addObserver( new NodeDatasetObserver(data, data.getNodeDataset(), control) );
	}
	
	class NodeDatasetObserver extends NetworkDatasetObserver 
	{
		public NodeDatasetObserver(NetworkModel data, NetworkDataset dataset, DatasetViewer control) 
		{
			super(data, dataset, control);
		}

		@Override
		public DatasetSelection getSelection() 
		{
			return data.getNodeSelection();
		}		
	}
}
