package noesis.ui.model.data.set;

import java.io.FileWriter;
import java.io.IOException;

import ikor.model.data.Dataset;
import ikor.model.ui.Action;
import ikor.model.ui.Application;
import ikor.model.ui.DatasetComponent;
import ikor.model.ui.File;
import ikor.model.ui.UIModel;
import ikor.util.log.Log;
import noesis.io.dataset.DatasetWriter;


public class DatasetUISaveAction extends Action 
{
	private UIModel ui;
	private DatasetComponent component;
	private String format;
	private File file;
	
	public DatasetUISaveAction (Application app, UIModel ui, DatasetComponent component, String format)
	{
		this.ui = ui;
		this.component = component;
		this.format = format;
		this.file = new File(app, "Save data...", "Save", new FileCommandAction() );
	}
		
	@Override
	public void run() 
	{
		file.setUrl("data."+format);
		file.getApplication().run(file);
	}
	
	
	public class FileCommandAction extends Action
	{
		@Override
		public void run() 
		{
			String filename = file.getUrl();
			
			if (!checkFilename(filename))
				filename = null;
			
			if (filename!=null) {
				
				try {
					
					if ( format.equals("txt") || format.equals("csv") ) {
						Dataset dataset = component.getData();
						String[] columnNames = new String[dataset.getColumnCount()];
						for(int col=0;col<dataset.getColumnCount();col++)
							columnNames[col] = component.getHeader(col).getText();
						
						DatasetWriter datasetWriter = new DatasetWriter(new FileWriter(filename));
						
						datasetWriter.writeHeader(columnNames);
						datasetWriter.write(dataset);
						datasetWriter.close();
					} else {
						throw new IOException("Unknown file format.");
					}

				} catch (IOException ioe) {
					Log.error("IO error - "+ioe);
				}
				
			}
		}
		
		private boolean checkFilename (String filename) 
		{
			if (filename!=null) {
				
				java.io.File file = new java.io.File(filename);
				
				if (file.exists())
					return ui.confirm("Overwrite existing file?");
				else
					return true;
				
			} else {
				return false;
			}
		}
	}

}
