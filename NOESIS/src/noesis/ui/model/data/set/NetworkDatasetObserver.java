package noesis.ui.model.data.set;

import ikor.model.Observer;
import ikor.model.Subject;
import ikor.model.ui.DatasetSelection;
import ikor.model.ui.DatasetViewer;
import noesis.AttributeNetwork;
import noesis.ui.model.NetworkModel;


public abstract class NetworkDatasetObserver implements Observer<AttributeNetwork>
{
	protected NetworkModel data;
	protected NetworkDataset dataset;
	protected DatasetViewer control;
	
	public NetworkDatasetObserver (NetworkModel data, NetworkDataset dataset, DatasetViewer control)
	{
		this.data = data;
		this.dataset = dataset;
		this.control = control;
	}
	
	public abstract DatasetSelection getSelection (); 

	@Override
	public void update (Subject subject, AttributeNetwork network) 
	{
		dataset.setNetwork(network);

		// Update UI control
		
		control.setModel(dataset.getModel());

		control.clearHeaders();
		
		for (int i=0; i<dataset.getColumnCount(); i++)
			control.addHeader( dataset.getName(i));
		
		control.setData(dataset);

		DatasetSelection selection = getSelection();
		control.setSelection(selection);
		
		control.notifyObservers(dataset);
		selection.notifyObservers(selection);
	}
	
}
