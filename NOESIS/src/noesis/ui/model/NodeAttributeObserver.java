package noesis.ui.model;

import ikor.model.Observer;
import ikor.model.Subject;
import ikor.model.ui.Application;
import ikor.model.ui.Option;
import ikor.model.ui.Selector;
import noesis.AttributeNetwork;

// Observer design pattern

public class NodeAttributeObserver implements Observer<AttributeNetwork>
{
	private Application app;
	private NetworkFigure figure;
	private Selector attributes;

	public NodeAttributeObserver (Application app, NetworkFigure figure, Selector control)
	{
		this.app = app;
		this.figure = figure;
		this.attributes = control;
	}

	@Override
	public void update (Subject subject, AttributeNetwork object) 
	{
		AttributeNetwork network = figure.getNetwork();

		if (network==null) {

			attributes.clear();

		} else if (network.getNodeAttributeCount()!=attributes.getOptions().size()) {

			attributes.clear();
			attributes.setMultipleSelection(false);

			for (int i=0; i<network.getNodeAttributeCount(); i++) {
				Option option = new Option(network.getNodeAttribute(i).getID());  // ID vs. Description
				option.setIcon(app.url("icons/kiviat.png"));
				attributes.add(option);
			}
		}
	}
}	
