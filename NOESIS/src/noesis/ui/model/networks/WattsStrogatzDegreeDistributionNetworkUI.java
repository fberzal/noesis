package noesis.ui.model.networks;

import ikor.collection.List;
import ikor.math.statistics.Distribution;
import ikor.math.statistics.ExponentialDistribution;
import ikor.math.statistics.NormalDistribution;
import ikor.math.statistics.UniformDistribution;
import ikor.model.data.IntegerModel;
import ikor.model.data.RealModel;
import ikor.model.ui.Action;
import ikor.model.ui.Application;
import ikor.model.ui.Editor;
import ikor.model.ui.Label;
import ikor.model.ui.Option;
import ikor.model.ui.Selector;
import noesis.AttributeNetwork;
import noesis.algorithms.visualization.RandomLayout;
import noesis.model.random.WattsStrogatzDegreeDistributionNetwork;


public class WattsStrogatzDegreeDistributionNetworkUI extends NewNetworkUI 
{
	Editor<Integer> nodeEditor, linkEditor;
	Editor<Double>  parameterEditor;
	Selector        distribution;
	
	Option exponentialOpt = new Option("Exponential distribution");
	Option normalOpt = new Option("Normal distribution");
	Option uniformOpt = new Option("Uniform distribution");
	
	public WattsStrogatzDegreeDistributionNetworkUI (Application app) 
	{
		super(app, "New Watts-Strogatz with degree distribution network...");
		
		setIcon( app.url("icon.gif") );
		
		IntegerModel nodeCountModel = new IntegerModel();
		nodeCountModel.setMinimumValue(0);
		nodeCountModel.setMaximumValue(1000);
		
		nodeEditor = new Editor<Integer>("Number of network nodes", nodeCountModel);
		nodeEditor.setIcon( app.url("icons/calculator.png") );
		nodeEditor.setData(50);
		add(nodeEditor);
		
		IntegerModel linkCountModel = new IntegerModel();
		linkCountModel.setMinimumValue(0);
		linkCountModel.setMaximumValue(10000);
		
		linkEditor = new Editor<Integer>("Number of network links", linkCountModel);
		linkEditor.setIcon( app.url("icons/calculator.png") );
		linkEditor.setData(150);
		add(linkEditor);

		Label label = new Label("Degree distribution");
		label.setIcon( app.url("icons/calculator.png") );
		add ( label );
		
		distribution = new Selector();
		distribution.setMultipleSelection(false);
		distribution.add(exponentialOpt);
		distribution.add(normalOpt);
		distribution.add(uniformOpt);
		add(distribution);
		
		RealModel parameterModel = new RealModel();
		
		parameterEditor = new Editor<Double>("Distribution parameter", parameterModel);
		parameterEditor.setIcon( app.url("icons/calculator.png") );
		parameterEditor.setData(1.0);
		add(parameterEditor);
		

		Option ok = new Option("Create");
		ok.setIcon( app.url("icon.gif") );
		ok.setAction( new WattsStrogatzWithDegreeDistributionNetworkAction(this) );
		add(ok);		
	}
	
	// Action
	
	public class WattsStrogatzWithDegreeDistributionNetworkAction extends Action  
	{
		private WattsStrogatzDegreeDistributionNetworkUI ui;

		public WattsStrogatzWithDegreeDistributionNetworkAction (WattsStrogatzDegreeDistributionNetworkUI ui) 
		{
			this.ui = ui;
		}

		@Override
		public void run()  
		{
			int nodes = ui.nodeEditor.getData();
			int links = ui.linkEditor.getData();
			double distParameter = ui.parameterEditor.getData();
			List<Option> selected = ui.distribution.getSelected();
			
			if (selected.size()==0) {
				
				ui.message("You must choose a degree distribution.");
				
			} else {
				
				Option selection = selected.get(0);
				Distribution distribution;
				
				if (selection==exponentialOpt) 
					distribution = new ExponentialDistribution(distParameter);
				else if (selection==normalOpt) 
					distribution = new NormalDistribution(0, distParameter);
				else 
					distribution = new UniformDistribution(0,1);
				
				WattsStrogatzDegreeDistributionNetwork random = new WattsStrogatzDegreeDistributionNetwork(nodes, links, distribution);
				AttributeNetwork network = createAttributeNetwork(random, "Watts-Strogatz's with degree distribution network", new RandomLayout ());			
				
				ui.set("network", network);
				ui.exit();
			}
		}	
	}	
}
