package noesis.ui.model.networks;

import ikor.model.data.IntegerModel;
import ikor.model.data.RealModel;
import ikor.model.ui.Action;
import ikor.model.ui.Application;
import ikor.model.ui.Editor;
import ikor.model.ui.Option;

import noesis.AttributeNetwork;

import noesis.algorithms.visualization.FruchtermanReingoldLayout;

import noesis.model.random.HolmeKimNetwork;


public class HolmeKimNetworkUI extends NewNetworkUI 
{
	Editor<Integer> nodeEditor;
	Editor<Integer> linkEditor;
	Editor<Double> probEditor;
	
	public HolmeKimNetworkUI (Application app) 
	{
		super(app, "New Holme-Kim network...");
		
		setIcon( app.url("icon.gif") );
		
		IntegerModel nodeCountModel = new IntegerModel();
		nodeCountModel.setMinimumValue(0);
		nodeCountModel.setMaximumValue(1000);
		
		nodeEditor = new Editor<Integer>("Number of network nodes", nodeCountModel);
		nodeEditor.setIcon( app.url("icons/calculator.png") );
		nodeEditor.setData(50);
		add(nodeEditor);
		
		IntegerModel degreeModel = new IntegerModel();
		degreeModel.setMinimumValue(0);
		degreeModel.setMaximumValue(100);
		
		linkEditor = new Editor<Integer>("Number of links for each new node", degreeModel);
		linkEditor.setIcon( app.url("icons/calculator.png") );
		linkEditor.setData(2);
		add(linkEditor);
		
		RealModel probModel = new RealModel();
		probModel.setMinimumValue(0.0);
		probModel.setMaximumValue(1.0);
		
		probEditor = new Editor<Double>("Probability of triad formation", probModel);
		probEditor.setIcon( app.url("icons/calculator.png") );
		probEditor.setData(0.5);
		add(probEditor);
		
		Option ok = new Option("Create");
		ok.setIcon( app.url("icon.gif") );
		ok.setAction( new HolmeKimNetworkAction(this) );
		add(ok);		
	}
	
	// Action
	
	public class HolmeKimNetworkAction extends Action 
	{
		private HolmeKimNetworkUI ui;

		public HolmeKimNetworkAction (HolmeKimNetworkUI ui)
		{
			this.ui = ui;
		}

		@Override
		public void run() 
		{
			int nodes = ui.nodeEditor.getData();
			int links = ui.linkEditor.getData();
			double probTF = ui.probEditor.getData();
			
			HolmeKimNetwork net = new HolmeKimNetwork(nodes, links, probTF);
			
			AttributeNetwork network = createAttributeNetwork(net, "Holme-Kim random network", new FruchtermanReingoldLayout() );			
			
			ui.set("network", network);
			ui.exit();
		}	
	}	

}
