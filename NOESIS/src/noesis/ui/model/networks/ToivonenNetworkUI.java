package noesis.ui.model.networks;

import ikor.model.data.IntegerModel;
import ikor.model.ui.Action;
import ikor.model.ui.Application;
import ikor.model.ui.Editor;
import ikor.model.ui.Option;

import noesis.AttributeNetwork;

import noesis.algorithms.visualization.FruchtermanReingoldLayout;

import noesis.model.random.ToivonenNetwork;


public class ToivonenNetworkUI extends NewNetworkUI 
{
	Editor<Integer> nodeEditor;
	Editor<Integer> initEditor;
	Editor<Integer> secondEditor;
	
	public ToivonenNetworkUI (Application app) 
	{
		super(app, "New Toivonen network...");
		
		setIcon( app.url("icon.gif") );
		
		IntegerModel nodeCountModel = new IntegerModel();
		nodeCountModel.setMinimumValue(0);
		nodeCountModel.setMaximumValue(1000);
		
		nodeEditor = new Editor<Integer>("Number of network nodes", nodeCountModel);
		nodeEditor.setIcon( app.url("icons/calculator.png") );
		nodeEditor.setData(50);
		add(nodeEditor);		
		
		IntegerModel initModel = new IntegerModel();
		initModel.setMinimumValue(0);
		initModel.setMaximumValue(100);
		
		initEditor = new Editor<Integer>("Initial-contact node count", initModel);
		initEditor.setIcon( app.url("icons/calculator.png") );
		initEditor.setData(1);
		add(initEditor);
		
		IntegerModel secondModel = new IntegerModel();
		secondModel.setMinimumValue(0);
		secondModel.setMaximumValue(100);
		
		secondEditor = new Editor<Integer>("Secondary-contact node count", secondModel);
		secondEditor.setIcon( app.url("icons/calculator.png") );
		secondEditor.setData(2);
		add(secondEditor);
				
		Option ok = new Option("Create");
		ok.setIcon( app.url("icon.gif") );
		ok.setAction( new ToivonenNetworkAction(this) );
		add(ok);		
	}
	
	// Action
	
	public class ToivonenNetworkAction extends Action 
	{
		private ToivonenNetworkUI ui;

		public ToivonenNetworkAction (ToivonenNetworkUI ui)
		{
			this.ui = ui;
		}

		@Override
		public void run() 
		{
			int nodes = ui.nodeEditor.getData();
			int init = ui.initEditor.getData();
			int second = ui.secondEditor.getData();
			
			ToivonenNetwork random = new ToivonenNetwork(nodes, init, second);
			AttributeNetwork network = createAttributeNetwork(random, "Toivonen random network", new FruchtermanReingoldLayout() );			
			
			ui.set("network", network);
			ui.exit();
		}	
	}	

}

