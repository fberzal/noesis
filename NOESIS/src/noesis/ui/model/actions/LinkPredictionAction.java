package noesis.ui.model.actions;

import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import ikor.math.Matrix;
import ikor.model.data.DataModel;
import ikor.model.data.Dataset;
import ikor.model.data.RealModel;
import ikor.model.data.TextModel;
import ikor.model.ui.Action;
import ikor.model.ui.Application;
import ikor.model.ui.Editor;
import ikor.model.ui.UIModel;
import noesis.Attribute;
import noesis.AttributeNetwork;
import noesis.Network;
import noesis.analysis.structure.links.prediction.NodePairMeasureTask;
import noesis.ui.model.NetworkModel;
import noesis.ui.model.data.set.DatasetUIModel;

/**
 * Link prediction actions.
 * 
 * @author Victor Martinez (victormg@acm.org)
 */


public class LinkPredictionAction extends Action {

	private Application app;
	private Class methodClass;
	private Optional<UIModel> ui;
	private Optional<List<Editor>> editors;
	
	public LinkPredictionAction (Application app, Optional<UIModel> ui, 
			NetworkModel model, Class methodClass, Optional<List<Editor>> editors)
	{
		this.app = app;
		this.ui = ui;
		this.methodClass = methodClass;
		this.editors = editors;
	}
	
	@Override
	public void run() {
		AttributeNetwork network = (AttributeNetwork) app.get("network");
				
		Stream valueStream = Stream.of(network);
		Stream classesStream = Stream.of(Network.class);
		if(editors.isPresent()) {
			valueStream = Stream.concat(valueStream, editors.get().stream().map(e->e.getData()));
			classesStream = Stream.concat(classesStream, editors.get().stream().map(e->e.getData().getClass()));
		}

		Object[] values = valueStream.toArray(Object[]::new);
		Class[] classes = (Class[]) classesStream.toArray(Class[]::new);
		
		NodePairMeasureTask method;
		try {
			method = (NodePairMeasureTask) methodClass.getConstructor(classes)
					.newInstance(values);
			
			Attribute idAttribute = network.getNodeAttribute("id");
			Matrix scores = method.call();
			
			String[] columns = {"source", "destination", "score"};
			Dataset dataset = new PredictedLinksDataset(idAttribute, scores);
			

			DatasetUIModel datasetModel = new DatasetUIModel(app, dataset, "Predicted links - "+method.getDescription(), columns);
			Action forward = new ForwardAction(datasetModel);
			forward.run();
			
			if(ui.isPresent())
				ui.get().exit();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private static class PredictedLinksDataset extends Dataset {

		private Matrix scores;
		private Attribute idAttribute;
		private final RealModel model = new RealModel();
		private final TextModel idModel = new TextModel();
		
		public PredictedLinksDataset(Attribute idAttribute, Matrix scores) {
			this.idAttribute = idAttribute;
			this.scores = scores;
		}
		
		@Override
		public Object get(int row, int column) {
			int nodes = idAttribute.size();
			int mrow = row / (nodes-1);
			int mcolumn = row % (nodes-1);
			if(mcolumn>=mrow) mcolumn++;
			if(column==0) return idAttribute.get(mrow);
			else if(column==1) return idAttribute.get(mcolumn);
			else return scores.get(mrow, mcolumn);
		}

		@Override
		public void set(int row, int column, Object object) {
			throw new UnsupportedOperationException();
		}

		@Override
		public int getColumnCount() {
			return 3;
		}

		@Override
		public int getRowCount() {
			return scores.columns()*(scores.rows()-1);
		}

		@Override
		public DataModel getModel(int column) {
			if(column==0 || column==1) return idModel;
			else return model;
		}
		
	}
	
}
