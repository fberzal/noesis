package noesis.ui.model.actions;

import java.lang.reflect.Constructor;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import ikor.model.data.IntegerModel;
import ikor.model.ui.Action;
import ikor.model.ui.Application;
import ikor.model.ui.Editor;
import ikor.model.ui.UIModel;
import ikor.util.log.Log;
import noesis.AttributeNetwork;
import noesis.LinkAttribute;
import noesis.Network;
import noesis.analysis.LinkScore;
import noesis.analysis.LinkScoreTask;
import noesis.analysis.structure.links.prediction.NodePairMeasureTask;
import noesis.analysis.structure.links.scoring.LinkScorer;
import noesis.ui.model.NetworkModel;
import noesis.ui.model.data.VectorUIModel;
import noesis.ui.model.data.set.LinkDatasetUIModel;


public class LinkScoringAction extends Action 
{
	private Application  application;
	private NetworkModel model;
	private Class        measureClass;
	private Optional<UIModel> 	 ui;
	private Optional<List<Editor>> editors;

	public LinkScoringAction (Application application, Optional<UIModel> ui, 
			NetworkModel model, Class metric, Optional<List<Editor>> editors)
	{
		this.application = application;
		this.ui = ui;
		this.model = model;
		this.measureClass = metric;
		this.editors = editors;
	}
	
	public LinkScoreTask instantiateTask (Network network, Optional<List<Editor>> editors)
	{
		LinkScoreTask task = null;
		
		try {
			Stream valueStream = Stream.of(network);
			Stream classesStream = Stream.of(Network.class);
			if(editors.isPresent()) {
				valueStream = Stream.concat(valueStream, editors.get().stream().map(e->e.getData()));
				classesStream = Stream.concat(classesStream, editors.get().stream().map(e->e.getData().getClass()));
			}

			Object[] values = valueStream.toArray(Object[]::new);
			Class[] classes = (Class[]) classesStream.toArray(Class[]::new);
		
			Constructor constructor = measureClass.getConstructor(classes);
			NodePairMeasureTask scoreTask = (NodePairMeasureTask) constructor.newInstance(values);
			task = new LinkScorer(network, scoreTask);
		} catch (Exception error) {
			
			Log.error ("LinkMeasure: Unable to instantiate "+measureClass);
		}
		
		return task;
	}

	@Override
	public void run() 
	{
		AttributeNetwork network = model.getNetwork();
		LinkScoreTask  task;
		LinkScore      metrics;
		LinkAttribute    attribute;
		String           id;
		
		if (network!=null) {
			
			task = instantiateTask(network, editors);
			
			if (task!=null) {
				
				metrics = task.getResult();
				
				id = metrics.getName();
				
				attribute = network.getLinkAttribute(id);

				if (attribute==null) {
					attribute = new LinkAttribute( network, metrics.getName(), metrics.getModel() );
					network.addLinkAttribute(attribute);
				}
				
				for (int i=0; i<network.links(); i++) {
					if (metrics.getModel() instanceof IntegerModel) {
						attribute.set (i, (int) metrics.get(i) );
					} else { // RealModel
						attribute.set(i, metrics.get(i) );
					}
				}
			
				model.setNetwork(network);
				
				// Summary
				
				VectorUIModel resultsUI = new VectorUIModel(application, metrics.getDescription(), metrics);
				Action forward = new ForwardAction(resultsUI);
				
				forward.run();
				
				// Link scores
				
				LinkDatasetUIModel linksetModel = new LinkDatasetUIModel(application, model, metrics.getDescription());
				
				model.notifyObservers(network);
				
				Action data = new ForwardAction(linksetModel);
				
				data.run();
				
				if(ui.isPresent())
					ui.get().exit();
			}
			
		}
	}			
	
}	
