package noesis.ui.model.actions;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.Objects;
import java.util.Optional;

import ikor.model.ui.Action;
import ikor.model.ui.Application;
import noesis.Parameter;
import noesis.analysis.structure.links.prediction.NodePairMeasureTask;
import noesis.ui.model.NetworkModel;
import noesis.ui.model.parameters.LinkScoreUIModel;

public class LinkPredictionScoringAction extends Action {

	private Application app;
	private Class method;
	private NetworkModel model;
	private Class<? extends Action> linkActionClass;
	
	public LinkPredictionScoringAction(Application app, 
			Class<? extends NodePairMeasureTask> method,
			NetworkModel model, Class<? extends Action> linkActionClass) {
		this.app = app;
		this.method = method;
		this.model = model;
		this.linkActionClass = linkActionClass;
	}
	
	@Override
	public void run() {
		Field[] parameters = Arrays.stream(method.getDeclaredFields())
				.filter(field->Objects.nonNull(field.getAnnotation(Parameter.class)))
				.toArray(Field[]::new);
							
			Action action = null;
			if(parameters.length==0) {
				try {
					action = linkActionClass.getConstructor(Application.class, Optional.class, 
						NetworkModel.class, Class.class, Optional.class)
					.newInstance(app, Optional.empty(), model, method, Optional.empty());
				} catch (Exception e) {
					e.printStackTrace();
				}
			} else {
				action = new ForwardAction( new LinkScoreUIModel(
						app, method, model, linkActionClass, parameters));
			}
			
			action.run();
	}

}
