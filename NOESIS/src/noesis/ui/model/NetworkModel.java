package noesis.ui.model;

import ikor.model.Subject;
import ikor.model.ui.DatasetSelection;
import noesis.AttributeNetwork;
import noesis.ui.model.data.set.LinkDataset;
import noesis.ui.model.data.set.NodeDataset;

// Network model in MVC

public class NetworkModel extends Subject<AttributeNetwork>
{
	private AttributeNetwork network;
	private DatasetSelection nodeSelection;
	private DatasetSelection linkSelection;
	private NodeDataset nodeDataset;
	private LinkDataset linkDataset;
	
	public NetworkModel (AttributeNetwork network)
	{
		this.network = network;
		this.nodeSelection = new ObservedDatasetSelection(this);
		this.linkSelection = new ObservedDatasetSelection(this);
		this.nodeDataset = new NodeDataset();
		this.linkDataset = new LinkDataset();
	}
	
	public DatasetSelection getNodeSelection() {
		return nodeSelection;
	}
	
	public DatasetSelection getLinkSelection() {
		return linkSelection;
	}
	
	public NodeDataset getNodeDataset() {
		return nodeDataset;
	}
	
	public LinkDataset getLinkDataset() {
		return linkDataset;
	}
	
	public AttributeNetwork getNetwork() 
	{
		return network;
	}

	public void setNetwork (AttributeNetwork network) 
	{
		if(this.network!=network) {
			nodeSelection.clearSelection();
			linkSelection.clearSelection();
			this.network = network;
		}
		notifyObservers(network);
	}
	
	private boolean updating = false;
	
	@Override
	public void update (Subject subject, AttributeNetwork object) 
	{
		if (!updating) {
			try {
				updating = true;
				if (object!=null)
					setNetwork( object );
				else
					notifyObservers(network);
			} finally {
		       updating = false;
			}
		}
	}
	
	private static class ObservedDatasetSelection extends DatasetSelection {
		
		private NetworkModel model;
		
		public ObservedDatasetSelection(NetworkModel model) {
			this.model = model;
		}
		
		@Override
		public void update (Subject subject, DatasetSelection object) {
			super.update(subject, object);
			model.notifyObservers(model.network);
		}
	}
	
}