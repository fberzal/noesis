package noesis.ui.console;

// Title:       Network format conversion utility
// Version:     1.0
// Copyright:   2012
// Author:      Fernando Berzal
// E-mail:      berzal@acm.org

import java.io.*;

import ikor.util.Benchmark;

import noesis.Network;

/**
 * Network format conversion.
 * 
 * @author Fernando Berzal
 */

public class NetworkFormat {

	/**
	 * @param args
	 */
	public static void main(String[] args)
		throws IOException
	{
	
		if (args.length<2) {
			
			System.err.println("NOESIS Network format conversion utility:");
			System.err.println();
			System.err.println("  java noesis.ui.console.NetworkFormat <input-file> <output-file>");
			
		} else {
			
			Benchmark  crono = new Benchmark();
			
			crono.start();
			
			// Input
						
			Network net = NetworkUtilities.read(args[0]);
			
			// Network information
			
			NetworkUtilities.printNetworkInformation(net);
			
			// Output
			
			NetworkUtilities.write(net, args[1]);

			// End
			
			crono.stop();
			
			System.out.println();
			System.out.println("Time: "+crono);		
		}
	}

}
