package noesis.ui.console;

// Title:       Network community detection algorithms
// Version:     1.0
// Copyright:   2017
// Author:      Fernando Berzal
// E-mail:      berzal@acm.org

import java.io.*;
import java.lang.reflect.Constructor;

import ikor.parallel.*;
import ikor.parallel.scheduler.*;
import ikor.util.Benchmark;
import ikor.util.log.Log;
import noesis.Network;
import noesis.algorithms.communities.CommunityDetector;
import noesis.algorithms.communities.CommunityDetectorTask;
import noesis.algorithms.traversal.ConnectedComponents;
import noesis.analysis.NodeScore;
import noesis.analysis.structure.communities.CohesionCoefficient;
import noesis.analysis.structure.communities.CoverageCoefficient;
import noesis.analysis.structure.communities.ModularityCoefficient;
import noesis.analysis.structure.communities.SeparationCoefficient;
import noesis.analysis.structure.communities.SilhouetteCoefficient;
import noesis.ui.model.data.Report;
import noesis.AttributeNetwork;

/**
 * Network community detection algorithms.
 * 
 * @author Fernando Berzal
 */

public class NetworkCommunities {

	/**
	 * @param args
	 */
	public static void main(String[] args)
		throws IOException
	{
		Scheduler.set ( new WorkStealingScheduler(16) );  // 8 (i5) vs. 16 (i7)
		// Scheduler.set ( new FutureScheduler(16) );
		// Scheduler.set ( new ThreadPoolScheduler() );
		// Scheduler.set ( new SequentialScheduler() );
		
		if (args.length<2) {
			
			System.err.println("NOESIS Network Community Detection:");
			System.err.println();
			System.err.println("  java noesis.ui.console.NetworkCommunities <file> <algorithm>");
			
		} else {
			
			Benchmark  crono = new Benchmark();
			
			crono.start();			
			
			// Load network data
			
			Network net = NetworkUtilities.read(args[0]);			

	        // Network statistics 
	        
			NetworkUtilities.printNetworkInformation(net);
			
			int loops = loops(net);
			System.out.println("- Loops: "+ loops);
			int directedLinks = directedLinks(net);
			System.out.println("- Directed links: "+directedLinks);

	        // Network normalization: Force the network to be undirected and remove loops
			
			if (directedLinks>0)
				net = undirected(net);
			
			if (loops>0)
				net = withoutLoops(net);
			

			if ((directedLinks>0) || (loops>0)) {
				System.out.println("AFTER NETWORK NORMALIZATION...");
				NetworkUtilities.printNetworkInformation(net);
			}
			
	        ConnectedComponents cc = new ConnectedComponents(net);
	        cc.compute();
	        System.out.println("- Connected components: "+ cc.components());			
				        
			// Community detection algorithm
			
			String algorithm = args[1];			
			CommunityDetectorTask task = null;
			
			try {
			
				Class detector = Class.forName(algorithm);
				Constructor constructor = detector.getConstructor(AttributeNetwork.class);
				task = new CommunityDetectorTask ( (CommunityDetector) constructor.newInstance(net), 
						                           (AttributeNetwork) net );
			
			} catch (Exception error) {
				
				Log.error ("Community detection algorithm: Unable to instantiate algorithm: "+algorithm);
				Log.error (error.toString());
			}
			
			
			// Community detection
			
			if (task!=null) {
				
				// Task execution

				System.out.println("COMMUNITY DETECTION");
				System.out.println("- Algorithm: "+algorithm);

				task.compute();				
				
				NodeScore assignment = task.getResult();
				
				// Output report
				
				Report report = new Report();

				
				report.add("Number of nodes", net.nodes() );
				
				report.add("Number of communities", (int) (assignment.max()-assignment.min()+1) );
				
				
		        ModularityCoefficient modularity = new ModularityCoefficient(net, assignment);		
				
				report.add("Modularity", modularity.overallValue() );	
				
				CohesionCoefficient cohesion = new CohesionCoefficient(net, assignment);
				
				report.add("Cohesion", cohesion.overallValue() );
				
				SeparationCoefficient separation = new SeparationCoefficient(net, assignment);
				
				report.add("Separation", separation.overallValue() );
				
				SilhouetteCoefficient silhouette = new SilhouetteCoefficient(net, assignment);
				
				report.add("Silhouette coefficient", silhouette.overallValue() );
				
				CoverageCoefficient coverage = new CoverageCoefficient(net, assignment);
				
				report.add("Coverage coefficient", coverage.overallValue());
				
				
				NetworkUtilities.printReport(report);		
			}
			

			// End
			
			crono.stop();
			
			System.out.println();
			System.out.println("Time: "+crono);
		}
	}
	
		
	// Make the network undirected
	
	private static Network undirected (Network net)
	{
		for (int i=0; i<net.size(); i++) {
			int degree = net.outDegree(i);

			for (int j=0; j<degree; j++) {
				
				if (!net.contains(net.outLink(i,j), i))
					net.add(net.outLink(i,j), i);
			}
		}

		return net;
	}

	// Directed links
	
	private static int directedLinks (Network net)
	{
		int directedLinks = 0;
		
		for (int i=0; i<net.size(); i++) {
			int degree = net.outDegree(i);

			for (int j=0; j<degree; j++) {
				
				if (!net.contains(net.outLink(i,j), i))
					directedLinks++;
			}
		}

		return directedLinks;
	}

	// Loops

	private static int loops (Network net)
	{
		int loops = 0;
		
		for (int i=0; i<net.size(); i++) {
			if (net.contains(i,i))
				loops++;
		}

		return loops;
	}

	private static Network withoutLoops (Network net)
	{
		for (int i=0; i<net.size(); i++) {
			if (net.contains(i,i))
				net.remove(i,i);
		}

		return net;
	}


}
