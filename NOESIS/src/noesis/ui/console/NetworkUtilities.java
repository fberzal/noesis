package noesis.ui.console;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import ikor.math.Decimal;
import noesis.AttributeNetwork;
import noesis.Network;
import noesis.io.ASCIINetworkReader;
import noesis.io.GDFNetworkReader;
import noesis.io.GDFNetworkWriter;
import noesis.io.GMLNetworkReader;
import noesis.io.GMLNetworkWriter;
import noesis.io.GraphMLNetworkReader;
import noesis.io.GraphMLNetworkWriter;
import noesis.io.NetworkReader;
import noesis.io.NetworkWriter;
import noesis.io.PajekNetworkReader;
import noesis.io.PajekNetworkWriter;
import noesis.io.SNAPGZNetworkReader;
import noesis.io.SNAPNetworkReader;
import noesis.ui.model.data.Report;
import noesis.ui.model.data.Report.ReportElement;

public class NetworkUtilities 
{
	/**
	 * Read network from data file.
	 * 
	 * @param filename File name
	 * @return Network
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	public static Network read(String filename)
		throws FileNotFoundException, IOException 
	{
		NetworkReader<String,Decimal> reader; 

		System.err.println("Reading network from "+filename);

		if (filename.endsWith(".net"))
			reader = new PajekNetworkReader(new FileReader(filename));
		else if (filename.endsWith(".dat"))
			reader = new ASCIINetworkReader(new FileReader(filename));
		else if (filename.endsWith(".txt"))
			reader = new SNAPNetworkReader(new FileReader(filename));
		else if (filename.endsWith(".gz"))
			reader = new SNAPGZNetworkReader(new FileInputStream(filename));
		else if (filename.endsWith(".gml"))
			reader = new GMLNetworkReader(new FileReader(filename));
		else if (filename.endsWith(".graphml"))
			reader = new GraphMLNetworkReader(new FileInputStream(filename));
		else if (filename.endsWith(".gdf"))
			reader = new GDFNetworkReader(new FileReader(filename));
		else
			throw new IOException("Unknown network file format.");

		
		reader.setType(noesis.ArrayNetwork.class);    // NDwww.net 5.2s
		//reader.setType(noesis.GraphNetwork.class);  // NDwww.net 9.6s

		Network net = reader.read();

		reader.close();
		
        // Network -> AttributeNetwork
        
        if (!(net instanceof AttributeNetwork))
        	net = new AttributeNetwork(net);		
		
		return net;
	}

	/**
	 * Write network to file using the file format associated to the file extension (.gdf, .gml, ,graphml, .net).
	 * 
	 * @param net Network
	 * @param filename File name
	 * @throws IOException
	 */
	public static void write(Network net, String filename)
		throws IOException 
	{
		NetworkWriter writer;

		if (filename.endsWith(".gdf"))
			writer = new GDFNetworkWriter(new FileWriter(filename));
		else if (filename.endsWith(".gml"))
			writer = new GMLNetworkWriter(new FileWriter(filename));
		else if (filename.endsWith(".graphml"))
			writer = new GraphMLNetworkWriter(new FileWriter(filename));
		else if (filename.endsWith(".net"))
			writer = new PajekNetworkWriter(new FileWriter(filename));
		else
			throw new IOException("Unknown output network file format.");

		writer.write(net);
		writer.close();
	}
	
	
	/**
	 * Print network information (ID, size & attributes).
	 * 
	 * @param net Network
	 */
	public static void printNetworkInformation(Network net) 
	{
		System.out.println("NETWORK INFORMATION");
		
		if (net.getID()!=null)
			System.out.println("- ID: "+net.getID());
		
		System.out.println("- Nodes: "+net.size());
		System.out.println("- Links: "+net.links());

		if (net instanceof AttributeNetwork)
			printNetworkAttributes((AttributeNetwork)net);
	}


	private static void printNetworkAttributes (AttributeNetwork net)
	{
		System.out.print("- "+ net.getNodeAttributeCount()+" node attributes:");
		
		for (int i=0; i<net.getNodeAttributeCount(); i++)
			System.out.print(" "+net.getNodeAttribute(i).getID());
		
		System.out.println();
		System.out.print("- "+ net.getLinkAttributeCount()+" link attributes:");

		for (int i=0; i<net.getLinkAttributeCount(); i++)
			System.out.print(" "+net.getLinkAttribute(i).getID());

		System.out.println();		
	}
	
	/**
	 * Output report.
	 * 
	 * @param report Report
	 */
	public static void printReport (Report report)
	{
		for (ReportElement item: report.getItems()) {
			System.out.println("- "+item.getLabel()+": "+item.getValue());
		}
	}
}
