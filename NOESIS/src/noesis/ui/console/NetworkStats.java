package noesis.ui.console;

// Title:       Network statistics
// Version:     1.0
// Copyright:   2012
// Author:      Fernando Berzal
// E-mail:      berzal@acm.org

import java.io.*;

import ikor.parallel.*;
import ikor.parallel.scheduler.*;
import ikor.util.Benchmark;
import noesis.Network;
import noesis.algorithms.traversal.ConnectedComponents;
import noesis.Attribute;
import noesis.AttributeNetwork;
import noesis.analysis.NodeScore;
import noesis.analysis.structure.Betweenness;
import noesis.analysis.structure.InDegree;
import noesis.analysis.structure.OutDegree;

/**
 * Network statistics.
 * 
 * @author Fernando Berzal
 */

public class NetworkStats {

	/**
	 * @param args
	 */
	public static void main(String[] args)
		throws IOException
	{
		Scheduler.set ( new WorkStealingScheduler(16) );  // 8 (i5) vs. 16 (i7)
		// Scheduler.set ( new FutureScheduler(16) );
		// Scheduler.set ( new ThreadPoolScheduler() );
		// Scheduler.set ( new SequentialScheduler() );
		
		if (args.length==0) {
			
			System.err.println("NOESIS Network Statistics:");
			System.err.println();
			System.err.println("  java noesis.ui.console.NetworkStats <file>");
			
		} else {
			
			Benchmark  crono = new Benchmark();
			
			crono.start();
			
			Network net = NetworkUtilities.read(args[0]);
			
			NetworkUtilities.printNetworkInformation(net);
			
			// Connected components
			
	        ConnectedComponents cc = new ConnectedComponents(net);
	        cc.compute();
	        System.out.println("- Connected components: "+ cc.components());			
			
			// Degree distributions
			
			OutDegree outDegrees = new OutDegree(net);
			InDegree  inDegrees = new InDegree(net);
			
			outDegrees.compute();
			inDegrees.compute();
			
			//saveInt("out/outDegrees.txt", outDegrees);
			//saveInt("out/inDegrees.txt",  inDegrees);
			
			System.out.println("Degree distributions");
			System.out.println("- Out-degrees: "+outDegrees.getResult());
			System.out.println("- In-degrees:  "+inDegrees.getResult());
			
			if (net instanceof AttributeNetwork) {
								
				System.out.println("Node of maximum out-degree:");
				printNode ( (AttributeNetwork) net, outDegrees.getResult().maxIndex());				

				System.out.println("Node of maximum in-degree:");
				printNode ( (AttributeNetwork) net, inDegrees.getResult().maxIndex());
			}
						
			// Betweenness 

			Betweenness betweenness = new Betweenness(net);
			betweenness.compute();
			System.out.println("Betweenness");
			System.out.println(betweenness.getResult());

			if (net instanceof AttributeNetwork) {
				System.out.println("Node of maximum betweenness:");
				printNode ( (AttributeNetwork) net, betweenness.getResult().maxIndex());				
			}

			crono.stop();
			
			System.out.println();
			System.out.println("Time: "+crono);
		}

	}


	public static void printNode (AttributeNetwork net, int index)
	{
		if (index<net.size()) {
			//System.out.println("- index: "+index);
			System.out.println("- out-degree: "+net.outDegree(index)+" out-links");
			System.out.println("- in-degree: "+net.inDegree(index)+" in-links");

			for (int i=0; i<net.getNodeAttributeCount(); i++) {
				Attribute attribute = net.getNodeAttribute(i);
				System.out.println("- "+attribute.getID()+": "+attribute.get(index));
			}
		}		
	}
	
	public static void saveInt (String file, NodeScore metrics)
		throws IOException
	{
		FileWriter writer = new FileWriter(file);

		for (int i=0; i<metrics.getNetwork().size(); i++)
			writer.write("\t" + (int)metrics.get(i));
		
		writer.close();
	}

}
