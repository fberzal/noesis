package noesis.analysis.structure;

import ikor.collection.DynamicSet;
import ikor.collection.Set;
import ikor.model.data.annotations.Description;
import ikor.model.data.annotations.Label;
import noesis.Network;
import noesis.analysis.NodeScore;
import noesis.analysis.NodeScoreTask;

/**
 * Local centrality.
 * 
 * @author Victor Martinez (victormg@acm.org)
 * 
 * References:
 *  - Duanbing Chena, Linyuan Lu, Ming-Sheng Shang, Yi-Cheng Zhang, Tao Zhou (2012):
 *    "Identifying influential nodes in complex networks"
 *    Physica a: Statistical mechanics and its applications 391(4):1777-1787
 *    https://doi.org/10.1016/j.physa.2011.09.017
 */

@Label("Local centrality")
@Description("Local centrality algorithm to quantify node influence.")
public class LocalCentrality extends NodeScoreTask 
{
		
	public LocalCentrality (Network network)
	{
		super(NodeScore.INTEGER_MODEL, network);
	}
	
	@Override
	public double compute (int node) 
	{
		Network network = getNetwork();
		int score = 0;
		
		for (int link=0; link<network.inDegree(node); link++) {
			int neighbor = network.inLink(node, link);
			score += scoreNeighbor(neighbor);
		}
		
		return score;
	}
	
	// Sum neighbor scores
	private int scoreNeighbor(int node)
	{
		int score = 0;
		Network network = getNetwork();
		
		for (int link=0; link<network.inDegree(node); link++) {
			int neighbor = network.inLink(node, link);
			score += countTwoStepNeighbors(neighbor);
		}
		
		return score;
	}
	
	// Returns the count of nearest and next nearest neighbors of a node
	private int countTwoStepNeighbors(int node)
	{
		Set<Integer> twoStepNeighbors = new DynamicSet<Integer>();
		Network network = getNetwork();
		
		for (int link=0; link<network.inDegree(node); link++) {
			int neighbor = network.inLink(node, link);
			twoStepNeighbors.add(neighbor);
			
			for (int link2=0; link2<network.inDegree(neighbor); link2++) {
				int neighbor2 = network.inLink(neighbor, link2);
				if (neighbor2!=node)
					twoStepNeighbors.add(neighbor2);
			}
			
		}
		
		return twoStepNeighbors.size();
	}
}