package noesis.analysis.structure.links.prediction;

import java.util.Arrays;
import java.util.Objects;
import java.util.stream.Collectors;

import ikor.math.Matrix;
import ikor.model.data.annotations.Description;
import ikor.model.data.annotations.Label;
import ikor.parallel.Task;
import noesis.Network;
import noesis.Parameter;

/**
 * Task for non-connected pairs of nodes (e.g., link prediction)
 * 
 * @author Victor Martinez (victormg@acm.org)
 */

public abstract class NodePairMeasureTask extends Task<Matrix> {
	private Network<?, ?> network;

	public NodePairMeasureTask(Network<?, ?> network) {
		this.network = network;
	}

	public final Network<?, ?> getNetwork() {
		return network;
	}

	// Computation template method

	protected Matrix measure = null;

	@Override
	public Matrix call() {
		compute();

		return measure;
	}

	public void checkDone() {
		if (measure == null)
			compute();
	}

	public abstract void compute();

	public String getName() {
		Class<? extends NodePairMeasureTask> type = this.getClass();
		
		String parameterTokens =
		Arrays.stream(type.getDeclaredFields()).map(field -> {
			Parameter parameter = field.getAnnotation(Parameter.class);
			String value = null;
			if (parameter != null) {
				field.setAccessible(true);
				try {
					value = field.getName()+"="+field.get(this).toString();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			return value;
		})
		.filter(value->Objects.nonNull(value))
		.collect(Collectors.joining(","));

		Label label = type.getAnnotation(Label.class);

		if (label != null) {
			if(parameterTokens.length()>0)
				return label.value()+"("+parameterTokens+")";
			else
				return label.value();
		} else
			return null;
	}

	public String getDescription() {
		Class type = this.getClass();
		Description description = (Description) type.getAnnotation(Description.class);

		if (description != null)
			return description.value();
		else
			return null;
	}
}
