package noesis.analysis.structure.links.prediction.local;

import ikor.model.data.annotations.Description;
import ikor.model.data.annotations.Label;
import noesis.Network;
import noesis.analysis.structure.links.prediction.DirectNeighborhoodOperations;

/**
 * Local Leicht-Holme-Newman score
 * 
 * @author Victor Martinez
 */

@Label("local-leicht-holm-new-score")
@Description("Local Leicht-Holme-Newman score")
public class LocalLeichtHolmeNewmanScore extends LocalUndirectedNodePairsMeasureTask {
	public LocalLeichtHolmeNewmanScore(Network<?,?> network) {
		super(network);
	}

	@Override
	public double compute(int sourceNode, int destinationNode) {
		Network<?,?> net = getNetwork();
		int sourceDegree = net.outDegree(sourceNode);
		int destinationDegree = net.outDegree(destinationNode);
		if(sourceDegree==0 || destinationDegree==0)
			return 0.0;
		DirectNeighborhoodOperations neighborhoodOperations = new DirectNeighborhoodOperations(net);
		int[] commonNeighbors = neighborhoodOperations.getIntersection(sourceNode, destinationNode);
		return commonNeighbors.length/((double)(sourceDegree*destinationDegree));
	}

}
