package noesis.analysis.structure.links.prediction.local;

import ikor.model.data.annotations.Description;
import ikor.model.data.annotations.Label;
import noesis.Network;
import noesis.analysis.structure.links.prediction.DirectNeighborhoodOperations;

/**
 * Hub promoted score
 * 
 * @author Victor Martinez
 */

@Label("hub-promo-score")
@Description("Hub promoted score")
public class HubPromotedScore extends LocalUndirectedNodePairsMeasureTask {
	public HubPromotedScore(Network<?,?> network) {
		super(network);
	}

	@Override
	public double compute(int sourceNode, int destinationNode) {
		Network<?,?> net = getNetwork();
		int sourceDegree = net.outDegree(sourceNode);
		int destinationDegree = net.outDegree(destinationNode);
		if(sourceDegree==0 || destinationDegree==0)
			return 0.0;
		DirectNeighborhoodOperations neighborhoodOperations = new DirectNeighborhoodOperations(net);
		int[] commonNeighbors = neighborhoodOperations.getIntersection(sourceNode, destinationNode);
		return commonNeighbors.length/((double)Math.min(sourceDegree, destinationDegree));
	}

}
