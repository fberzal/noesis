package noesis.analysis.structure.links.prediction.local;

import ikor.model.data.annotations.Description;
import ikor.model.data.annotations.Label;
import noesis.Network;
import noesis.analysis.structure.links.prediction.DirectNeighborhoodOperations;

/**
 * Common Neighbors score
 * 
 * @author Victor Martinez (victormg@acm.org)
 */

@Label("comm-neigh-score")
@Description("Common neighbors score")
public class CommonNeighborsScore extends LocalUndirectedNodePairsMeasureTask 
{
	public CommonNeighborsScore(Network<?,?> network)
	{
		super(network);
	}
	
	@Override
	public double compute (int sourceNode, int destinationNode) 
	{
		Network<?,?> net = getNetwork();
		DirectNeighborhoodOperations neighborhoodOperations = new DirectNeighborhoodOperations(net);
		return neighborhoodOperations.getIntersection(sourceNode, destinationNode).length;
	}

}
