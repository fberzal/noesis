package noesis.analysis.structure.links.prediction.local;

import ikor.model.data.annotations.Description;
import ikor.model.data.annotations.Label;
import noesis.Network;
import noesis.analysis.structure.links.prediction.DirectNeighborhoodOperations;

/**
 * Adamic and Adar score
 * 
 * @author Victor Martinez (victormg@acm.org)
 */

@Label("adamic-adar-score")
@Description("Adamic-Adar score")
public class AdamicAdarScore extends LocalUndirectedNodePairsMeasureTask {
	public AdamicAdarScore(Network<?,?> network) {
		super(network);
	}

	@Override
	public double compute(int sourceNode, int destinationNode) {
		Network<?,?> net = getNetwork();
		DirectNeighborhoodOperations neighborhoodOperations = new DirectNeighborhoodOperations(net);
		int[] commonNeighbors = neighborhoodOperations.getIntersection(sourceNode, destinationNode);
		double score = 0;
		for (int index = 0; index < commonNeighbors.length; index++)
			score += 1.0 / Math.log(net.outDegree(commonNeighbors[index]));
		return score;
	}

}
