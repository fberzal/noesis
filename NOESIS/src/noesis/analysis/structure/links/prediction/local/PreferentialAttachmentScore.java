package noesis.analysis.structure.links.prediction.local;

import ikor.model.data.annotations.Description;
import ikor.model.data.annotations.Label;
import noesis.Network;

/**
 * Preferential Attachment score
 * 
 * @author Victor Martinez (victormg@acm.org)
 */

@Label("pref-att-score")
@Description("Preferential attachment score")
public class PreferentialAttachmentScore extends LocalUndirectedNodePairsMeasureTask
{
	public PreferentialAttachmentScore(Network<?,?> network)
	{
		super(network);
	}
	
	@Override
	public double compute (int sourceNode, int destinationNode) 
	{
		Network<?,?> net = getNetwork();
		return net.outDegree(sourceNode)*net.outDegree(destinationNode);
	}

}
