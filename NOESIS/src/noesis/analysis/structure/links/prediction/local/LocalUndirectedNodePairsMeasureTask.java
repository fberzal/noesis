package noesis.analysis.structure.links.prediction.local;

import ikor.math.SparseMatrix;
import noesis.Network;
import noesis.analysis.structure.links.prediction.NodePairMeasureTask;

public abstract class LocalUndirectedNodePairsMeasureTask extends NodePairMeasureTask {

	public LocalUndirectedNodePairsMeasureTask(Network<?,?> network) {
		super(network);
	}
	
	@Override
	public void compute() {
		Network<?, ?> network = getNetwork();
		int numNodes = network.nodes();
		measure = new SparseMatrix(numNodes, numNodes);

		for (int sourceNodeIndex = 0; sourceNodeIndex < numNodes; sourceNodeIndex++) {

			int[] sourceNeighbors = network.outLinks(sourceNodeIndex);
			if (sourceNeighbors != null) {
				for (int sourceNeighborIndex : sourceNeighbors) {

					int[] firstNeighbors = network.outLinks(sourceNeighborIndex);
					if (firstNeighbors != null) {
						for (int firstNeighborIndex : firstNeighbors) {
							if (firstNeighborIndex < sourceNodeIndex && 
									measure.get(sourceNodeIndex, firstNeighborIndex) == 0) {
								double computedScore = compute(sourceNodeIndex, firstNeighborIndex);
								measure.set(sourceNodeIndex, firstNeighborIndex, computedScore);
								measure.set(firstNeighborIndex, sourceNodeIndex, computedScore);
							}

						}
					}

				}
			}
		}

	}
	
	public abstract double compute(int sourceNode, int destinationNode);
}
