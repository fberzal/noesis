package noesis.analysis.structure.links.prediction.local;

import ikor.model.data.annotations.Description;
import ikor.model.data.annotations.Label;
import noesis.Network;
import noesis.analysis.structure.links.prediction.DirectNeighborhoodOperations;

/**
 * Jaccard score
 * 
 * @author Victor Martinez (victormg@acm.org)
 */

@Label("jaccard-score")
@Description("Jaccard score")
public class JaccardScore extends LocalUndirectedNodePairsMeasureTask {
	public JaccardScore(Network<?,?> network) {
		super(network);
	}

	@Override
	public double compute(int sourceNode, int destinationNode) {
		Network<?,?> net = getNetwork();
		DirectNeighborhoodOperations neighborhoodOperations = new DirectNeighborhoodOperations(net);
		int[] commonNeighbors = neighborhoodOperations.getIntersection(sourceNode, destinationNode);
		int[] allNeighbors = neighborhoodOperations.getUnion(sourceNode, destinationNode);
		if(allNeighbors.length==0)
			return 0.0;
		else
			return (double)commonNeighbors.length/(double)allNeighbors.length;
	}

}
