package noesis.analysis.structure.links.prediction.global;

import ikor.math.Matrix;
import ikor.math.SparseMatrix;
import ikor.model.data.annotations.Description;
import ikor.model.data.annotations.Label;
import noesis.Network;
import noesis.Parameter;

/**
 * Random Walk with restart score
 * 
 * @author Victor Martinez
 */

@Label("random-walk-restart-score")
@Description("Random walk with restart score")
public class RandomWalkWithRestartScore extends RandomWalkScore {

	private static final double DEFAULT_ALPHA = 0.9;
	private static final double DEFAULT_EPSILON = 0.001;
	private static final int DEFAULT_MAX_ITERATIONS = 150;
	
	@Parameter(min=0.0, max=1.0, defaultValue=DEFAULT_ALPHA)
	@Description("Alpha value")
	private double alpha;
	private double epsilon;
	private int maxIterations;

	public RandomWalkWithRestartScore(Network<?, ?> network) {
		this(network, DEFAULT_ALPHA);
	}
	
	public RandomWalkWithRestartScore(Network<?, ?> network, Double alpha) {
		this(network, alpha, DEFAULT_EPSILON);
	}

	public RandomWalkWithRestartScore(Network<?, ?> network, Double alpha, Double epsilon) {
		this(network, alpha, epsilon, DEFAULT_MAX_ITERATIONS);
	}
	
	public RandomWalkWithRestartScore(Network<?, ?> network, Double alpha, Double epsilon, Integer maxIterations) {
		super(network);
		this.alpha = alpha;
		this.epsilon = epsilon;
		this.maxIterations = maxIterations;
	}

	@Override
	public Matrix computeMatrix() {
		Network<?, ?> net = getNetwork();
		int numNodes = net.nodes();
		
		Matrix transitionMatrix = buildTransitionMatrix(net);
		Matrix probabilityMatrix = new SparseMatrix(numNodes, numNodes);

		for (int node1 = 0; node1 < numNodes; node1++) {
			if(net.outDegree(node1)>0) {
				Matrix probabilityVector = new SparseMatrix(numNodes, 1);
				Matrix oldprobabilityVector = null;
				probabilityVector.set(node1, 0, 1.0);
	
				Matrix seedVector = new SparseMatrix(numNodes, 1);
				seedVector.set(node1, 0, 1.0);
				
				double diff = 0.0;
				int iter = 0;
				
				do {
					iter++;
					oldprobabilityVector = probabilityVector;
					probabilityVector = transitionMatrix.multiply(probabilityVector);
					
					probabilityVector = transitionMatrix.multiply(probabilityVector).multiply(alpha).add(seedVector.multiply(1.0-alpha));
					
					diff = 0.0;
					
					for (int i = 0; i < numNodes; i++)
						diff += Math.pow(probabilityVector.get(i, 0) - oldprobabilityVector.get(i, 0), 2);
					
				} while (diff > epsilon && iter < maxIterations);
				
				for (int node2=0; node2 < numNodes; node2++) {
					if(node1!=node2) {
						probabilityMatrix.set(node1, node2, probabilityMatrix.get(node1, node2) + probabilityVector.get(node2, 0));
						probabilityMatrix.set(node2, node1, probabilityMatrix.get(node2, node1) + probabilityVector.get(node2, 0));
					}
				}
			}
		}

		return probabilityMatrix;
	}
}

