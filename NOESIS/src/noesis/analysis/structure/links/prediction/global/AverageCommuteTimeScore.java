package noesis.analysis.structure.links.prediction.global;

import ikor.math.Matrix;
import ikor.math.SparseMatrix;
import ikor.model.data.annotations.Description;
import ikor.model.data.annotations.Label;
import noesis.Network;

/**
 * Average commute time score
 * 
 * @author Victor Martinez
 */

@Label("avg-comm-time-score")
@Description("Average commute time score")
public class AverageCommuteTimeScore extends GlobalUndirectedNodePairsMeasureTask {
	
	public AverageCommuteTimeScore(Network<?,?> network) {
		super(network);
	}
	
	@Override
	public Matrix computeMatrix() {
		
		Network<?,?> net = getNetwork();
		int numNodes = net.nodes();
		
		Matrix laplacianMatrix = new LaplacianMatrix(net);
		
		Matrix inverseLaplacian = new MatrixPseudoinversion(laplacianMatrix).call();

		Matrix similarity = new SparseMatrix(numNodes, numNodes);
		
		for (int node1 = 0; node1 < numNodes; node1++) {
			for (int node2 = 0; node2 < numNodes; node2++) {
				double denominator = inverseLaplacian.get(node1, node1)+inverseLaplacian.get(node2, node2)-2*inverseLaplacian.get(node1, node2);
				
				if(denominator>0) {
					double score = 1.0/denominator;
					similarity.set(node1, node2, score);
				}
			}
		}
		
		return similarity;
	}

}
