package noesis.analysis.structure.links.prediction.global;

import ikor.math.Matrix;
import ikor.math.SparseMatrix;
import ikor.model.data.annotations.Description;
import ikor.model.data.annotations.Label;
import noesis.Network;

/**
 * Pseudoinverse Laplacian score
 * 
 * @author Victor Martinez
 */

@Label("pseudoinverse-laplacian-score")
@Description("Pseudoinverse Laplacian score")
public class PseudoinverseLaplacianScore extends GlobalUndirectedNodePairsMeasureTask {
	
	public PseudoinverseLaplacianScore(Network<?,?> network) {
		super(network);
	}
	
	@Override
	public Matrix computeMatrix() {
		
		Network<?,?> net = getNetwork();
		int numNodes = net.nodes();
		
		Matrix laplacianMatrix = new LaplacianMatrix(net);
		
		Matrix inverseLaplacian = new MatrixPseudoinversion(laplacianMatrix).call();

		Matrix similarity = new SparseMatrix(numNodes, numNodes);
		
		for (int node1 = 0; node1 < numNodes; node1++) {
			for (int node2 = 0; node2 < numNodes; node2++) {
				double denominator = inverseLaplacian.get(node1, node1) * inverseLaplacian.get(node2, node2);
				
				if(denominator>0) {
					double score = inverseLaplacian.get(node1, node2)/Math.sqrt(denominator);
					similarity.set(node1, node2, score);
				}
			}
		}
		
		return similarity;
	}

}
