package noesis.analysis.structure.links.prediction.global;

import ikor.math.Matrix;
import ikor.math.SparseMatrix;
import ikor.model.data.annotations.Description;
import ikor.model.data.annotations.Label;
import noesis.Network;
import noesis.network.AdjacencyMatrix;

/**
 * Flow propagation score
 * 
 * @author Victor Martinez
 */

@Label("flow-propagation-score")
@Description("Flow propagation score")
public class FlowPropagationScore extends RandomWalkScore {

	private static final double DEFAULT_EPSILON = 0.001;
	private static final int DEFAULT_MAX_ITERATIONS = 150;

	public FlowPropagationScore(Network<?, ?> network) {
		this(network, DEFAULT_EPSILON, DEFAULT_MAX_ITERATIONS);
	}

	public FlowPropagationScore(Network<?, ?> network, Double epsilon) {
		this(network, epsilon, DEFAULT_MAX_ITERATIONS);
	}
	
	public FlowPropagationScore(Network<?, ?> network, Double epsilon, Integer maxIterations) {
		super(network, epsilon, maxIterations);
	}

	@Override
	protected Matrix buildTransitionMatrix(Network<?,?> network) {
		int numNodes = network.nodes();
		Matrix adjacencyMatrix = new AdjacencyMatrix(network);
		
		Matrix d = new SparseMatrix(numNodes, numNodes);

		for (int actualNodeIndex=0; actualNodeIndex < numNodes; actualNodeIndex++) {
			double denominator = network.outDegree(actualNodeIndex) + 1;
			adjacencyMatrix.set(actualNodeIndex, actualNodeIndex, 1.0);
			
			if(network.outDegree(actualNodeIndex)>0)
				d.set(actualNodeIndex, actualNodeIndex, 1.0/Math.sqrt(denominator));
		}
		Matrix transitionMatrix = (d.multiply(adjacencyMatrix)).multiply(d);
		
		return transitionMatrix;
	}
}

