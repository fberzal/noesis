package noesis.analysis.structure.links.prediction.global;

import ikor.math.Matrix;
import ikor.math.SparseMatrix;
import ikor.model.data.annotations.Description;
import ikor.model.data.annotations.Label;
import noesis.Network;

/**
 * Random forest kernel score
 * 
 * @author Victor Martinez
 */

@Label("rand-forest-kern-score")
@Description("Random forest kernel score")
public class RandomForestKernelScore extends GlobalUndirectedNodePairsMeasureTask {
	
	public RandomForestKernelScore(Network<?,?> network) {
		super(network);
	}
	
	@Override
	public Matrix computeMatrix() {
		Network<?,?> net = getNetwork();
		int numNodes = net.nodes();
		
		LaplacianMatrix laplacianMatrix = new LaplacianMatrix(net);
		Matrix matrix = new SparseMatrix(numNodes, numNodes);
		for(int node1=0;node1<numNodes;node1++)
			for(int node2=0;node2<numNodes;node2++) {
				double value = laplacianMatrix.get(node1, node2);
				if(node1==node2)
					value += 1;
				matrix.set(node1, node2, value);
			}
			
		return matrix.inverse();
	}

}
