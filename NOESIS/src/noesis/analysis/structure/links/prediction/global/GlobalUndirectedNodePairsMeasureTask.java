package noesis.analysis.structure.links.prediction.global;

import ikor.math.Matrix;
import noesis.Network;
import noesis.analysis.structure.links.prediction.NodePairMeasureTask;

public abstract class GlobalUndirectedNodePairsMeasureTask extends NodePairMeasureTask {

	public GlobalUndirectedNodePairsMeasureTask(Network<?,?> network) {
		super(network);
	}

	protected abstract Matrix computeMatrix();
	
	@Override
	public void compute() {
		Matrix result = computeMatrix();
		measure = result;
	}
}
