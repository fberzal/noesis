package noesis.analysis.structure.links.prediction.global;

import ikor.math.EigenvectorDecomposition;
import ikor.math.Matrix;
import ikor.math.SparseMatrix;
import ikor.model.data.annotations.Description;
import ikor.model.data.annotations.Label;
import noesis.Network;
import noesis.network.AdjacencyMatrix;

/**
 * Global Leicht-Holme-Newman score
 * 
 * @author Victor Martinez
 */

@Label("global-leicht-holm-new-score")
@Description("Global Leicht-Holme-Newman score")
public class GlobalLeichtHolmeNewmanScore extends GlobalUndirectedNodePairsMeasureTask {
	
	private static final double DEFAULT_BETA = 0.1;
	private static final int DEFAULT_MAX_ITERATIONS = 100;
	private static final double DEFAULT_EPSILON = 0.001;
	
	private double epsilon;
	private double beta;
	private int maxIterations;
	
	public GlobalLeichtHolmeNewmanScore(Network<?,?> network) {
		this(network, DEFAULT_BETA, DEFAULT_MAX_ITERATIONS, DEFAULT_EPSILON);
	}
	
	public GlobalLeichtHolmeNewmanScore(Network<?,?> network, Double beta) {
		this(network, beta, DEFAULT_MAX_ITERATIONS, DEFAULT_EPSILON);
	}
	
	public GlobalLeichtHolmeNewmanScore(Network<?,?> network, Double beta, Integer maxIterations) {
		this(network, beta, maxIterations, DEFAULT_EPSILON);
	}
	
	public GlobalLeichtHolmeNewmanScore(Network<?,?> network, Double beta, Integer maxIterations, Double epsilon) {
		super(network);
		this.beta = beta;
		this.maxIterations = maxIterations;
		this.epsilon = epsilon;
	}

	@Override
	public Matrix computeMatrix() {
		Network<?,?> net = getNetwork();
		int numNodes = net.nodes();

		Matrix adjacencyMatrix = new AdjacencyMatrix(net);
		EigenvectorDecomposition eigen = new EigenvectorDecomposition(adjacencyMatrix);
		
		double dominantEigenvalue = eigen.getRealEigenvalues().max();
		Matrix oldS;
		Matrix S = new SparseMatrix(numNodes,numNodes);
		
		int iter = 0;
		double diff;
		do {
			oldS = S;
			S = adjacencyMatrix.multiply(beta/dominantEigenvalue).multiply(S);
			for(int i=0;i<numNodes;i++)
				S.set(i, i, S.get(i, i) + 1.0);
			
			diff = 0.0;
			for (int i = 0; i < numNodes; i++)
				for(int j = 0; j < numNodes; j++)
					diff += Math.pow(S.get(i, j) - oldS.get(i, j), 2);
			
			iter++;
		} while(diff>epsilon && iter<maxIterations);
		
		return S;
	}

}

