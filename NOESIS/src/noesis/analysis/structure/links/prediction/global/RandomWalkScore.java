package noesis.analysis.structure.links.prediction.global;

import ikor.math.Matrix;
import ikor.math.SparseMatrix;
import ikor.model.data.annotations.Description;
import ikor.model.data.annotations.Label;
import noesis.Network;
import noesis.network.AdjacencyMatrix;

/**
 * Random Walk score
 * 
 * @author Victor Martinez
 */

@Label("random-walk-score")
@Description("Random walk score")
public class RandomWalkScore extends GlobalUndirectedNodePairsMeasureTask {

	private static final double DEFAULT_EPSILON = 0.001;
	private static final int DEFAULT_MAX_ITERATIONS = 150;
	
	private double epsilon;
	private int maxIterations;

	public RandomWalkScore(Network<?, ?> network) {
		this(network, DEFAULT_EPSILON, DEFAULT_MAX_ITERATIONS);
	}

	public RandomWalkScore(Network<?, ?> network, Double epsilon) {
		this(network, epsilon, DEFAULT_MAX_ITERATIONS);
	}
	
	public RandomWalkScore(Network<?, ?> network, Double epsilon, Integer maxIterations) {
		super(network);
		this.epsilon = epsilon;
		this.maxIterations = maxIterations;
	}

	@Override
	public Matrix computeMatrix() {
		Network<?, ?> net = getNetwork();
		int numNodes = net.nodes();
		
		Matrix transitionMatrix = buildTransitionMatrix(net);
		Matrix probabilityMatrix = new SparseMatrix(numNodes, numNodes);

		for (int node1 = 0; node1 < numNodes; node1++) {
			if(net.outDegree(node1)>0) {
				Matrix probabilityVector = new SparseMatrix(numNodes, 1);
				Matrix oldprobabilityVector = null;
				probabilityVector.set(node1, 0, 1.0);
	
				double diff = 0.0;
				int iter = 0;
				
				do {
					iter++;
					oldprobabilityVector = probabilityVector;
					probabilityVector = transitionMatrix.multiply(probabilityVector);
					diff = 0.0;
					
					for (int i = 0; i < numNodes; i++)
						diff += Math.pow(probabilityVector.get(i, 0) - oldprobabilityVector.get(i, 0), 2);

				} while (diff > epsilon && iter < maxIterations);
				
				for (int node2=0; node2 < numNodes; node2++) {
					if(node1!=node2) {
						probabilityMatrix.set(node1, node2, probabilityMatrix.get(node1, node2) + probabilityVector.get(node2, 0));
						probabilityMatrix.set(node2, node1, probabilityMatrix.get(node2, node1) + probabilityVector.get(node2, 0));
					}
				}
			}
		}

		return probabilityMatrix;
	}

	
	protected Matrix buildTransitionMatrix(Network<?,?> network) {
		int numNodes = network.nodes();
		Matrix adjacencyMatrix = new AdjacencyMatrix(network);
		Matrix transitionMatrix = new SparseMatrix(numNodes, numNodes);
		
		for (int node1 = 0; node1 < numNodes; node1++) {
			double denominator = network.outDegree(node1) + 1;
			transitionMatrix.set(node1, node1, 1.0 / denominator);
					
			for (int node2 = 0; node2 < numNodes; node2++) {
				if (adjacencyMatrix.get(node1, node2) == 1.0) {
					transitionMatrix.set(node1, node2, adjacencyMatrix.get(node1, node2) / denominator);
				}
			}
		}
		
		return transitionMatrix;
	}
}

