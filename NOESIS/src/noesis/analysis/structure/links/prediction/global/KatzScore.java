package noesis.analysis.structure.links.prediction.global;

import ikor.math.Matrix;
import ikor.math.SparseMatrix;
import ikor.model.data.annotations.Description;
import ikor.model.data.annotations.Label;
import noesis.Network;
import noesis.Parameter;

/**
 * Katz score
 * 
 * @author Victor Martinez
 */

@Label("katz-score")
@Description("Katz score")
public class KatzScore extends GlobalUndirectedNodePairsMeasureTask {
	
	private static final double DEFAULT_BETA = 0.1;
	
	@Parameter(min=0.0, max=1.0, defaultValue=DEFAULT_BETA)
	@Description("Beta value")
	private double beta;
	
	public KatzScore(Network<?,?> network) {
		super(network);
		this.beta = DEFAULT_BETA;
	}
	
	public KatzScore(Network<?,?> network, Double beta) {
		super(network);
		this.beta = beta;
	}

	@Override
	protected Matrix computeMatrix() {
		Network<?,?> net = getNetwork();
		int numNodes = net.nodes();
		SparseMatrix matrix = new SparseMatrix(numNodes, numNodes);
		for(int i=0;i<numNodes;i++) {
			for(int j=0;j<numNodes;j++) {
				if(i==j) {
					matrix.set(i, i, 1.0);
				} else {
					if(net.contains(i, j)) {
						matrix.set(i, j, -beta);
					}
				}
			}
		}
		
		Matrix result = matrix.inverse();
		
		for(int i=0;i<numNodes;i++) {
			result.set(i, i, matrix.get(i, i) + 1.0);
		}
		
		return result;
	}

}
