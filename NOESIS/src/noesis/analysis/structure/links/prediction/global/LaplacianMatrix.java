package noesis.analysis.structure.links.prediction.global;

import ikor.math.Matrix;
import noesis.Network;

public class LaplacianMatrix extends Matrix {
	
	private Network<?,?> net;
	
	// Constructor
	
	public LaplacianMatrix(Network<?,?> net)
	{
		this.net = net;
	}

	// Matrix interface
	
	@Override
	public int rows() 
	{
		return net.size();
	}

	@Override
	public int columns() 
	{
		return net.size();
	}

	@Override
	public double get(int i, int j) 
	{
		if(i==j) {
			return net.outDegree(i);
		} else {
			if (net.contains(i,j))
				return -1;
			else
				return 0;
		}
	}

	@Override
	public void set(int i, int j, double v) 
	{
		throw new UnsupportedOperationException("Networks cannot be modified through their Laplacian matrix.");
	}

}
