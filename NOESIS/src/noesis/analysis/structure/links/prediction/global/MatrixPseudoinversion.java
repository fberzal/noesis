package noesis.analysis.structure.links.prediction.global;

import ikor.math.Matrix;
import ikor.math.SingularValueDecomposition;
import ikor.math.SparseMatrix;
import ikor.parallel.Task;

public class MatrixPseudoinversion extends Task<Matrix> {

	private static final double DEFAULT_EPSILON = 0.001;
	
	private double epsilon;
	private Matrix matrix;
	
	public MatrixPseudoinversion(Matrix matrix) {
		this(matrix, DEFAULT_EPSILON);
	}
	
	public MatrixPseudoinversion(Matrix matrix, double epsilon) {
		this.matrix = matrix;
		this.epsilon = epsilon;
	}
	
	@Override
	public Matrix call() {
		int size = matrix.rows();
		SingularValueDecomposition svdDecomposition = new SingularValueDecomposition(matrix);
		Matrix Um = svdDecomposition.getU();
		Matrix Sm = svdDecomposition.getS();
		Matrix Vm = svdDecomposition.getV();
		
		for(int i=0;i<size;i++)
			for(int j=0;j<size;j++) {
				Um.set(i, j, Math.abs(Um.get(i, j))<epsilon ? 0.0 : Um.get(i, j));
				Sm.set(i, j, Math.abs(Sm.get(i, j))<epsilon ? 0.0 : Sm.get(i, j));
				Vm.set(i, j, Math.abs(Vm.get(i, j))<epsilon ? 0.0 : Vm.get(i, j));
			}
		
		for(int i=0;i<size;i++) {
			double val = Sm.get(i,i);
			if(val!=0.0)
				Sm.set(i, i, 1.0/val);
		}
		
		Matrix resultm = Vm.multiply(Sm.multiply(Um.transpose()));
		
		Matrix result = new SparseMatrix(size, size);
		
		for(int i=0;i<size;i++)
			for(int j=0;j<size;j++) {
				result.set(i, j, resultm.get(i, j));
			}
		
		return result;
	}

}
