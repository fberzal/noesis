package noesis.analysis.structure.links.prediction;

import ikor.collection.DynamicList;
import noesis.Network;

/**
 * Intersection and union for node neighbourhoods
 * 
 * @author Victor Martinez (victormg@acm.org)
 */

public class DirectNeighborhoodOperations  {
	
	private Network<?,?> network;
	
	public DirectNeighborhoodOperations(Network network)
	{
		this.network = network;
	}

	// Get shared neighbours count
	public int getIntersectionCount(int firstNode, int secondNode) {
		int commonCount = 0;
		
		for(int firstLink:network.outLinks(firstNode))
			for(int secondLink:network.outLinks(secondNode))
				if(firstLink==secondLink)
					commonCount++;
		
		return commonCount;
	}
	
	// Get intersection of neighbours
	public final int[] getIntersection(int firstNode, int secondNode) {
		
		DynamicList<Integer> commonNeighborsList = new DynamicList<Integer>();
		
		if(network.outDegree(firstNode)==0 || network.outDegree(secondNode)==0) {
			return new int[0];
		}
		
		for(int firstLink:network.outLinks(firstNode))
			for(int secondLink:network.outLinks(secondNode))
				if(firstLink==secondLink)
					commonNeighborsList.add(firstLink);
		
		int [] commonNeighbors = new int[commonNeighborsList.size()];
		for(int index=0;index<commonNeighborsList.size();index++)
			commonNeighbors[index] = commonNeighborsList.get(index);
		return commonNeighbors;
	}
	
	// Get union of neighbours
	public final int[] getUnion(int firstNode, int secondNode) {
		DynamicList<Integer> allNeighborsList = new DynamicList<Integer>();

		int firstNodeDegree = network.outDegree(firstNode);
		int secondNodeDegree = network.outDegree(secondNode);

		for(int firstNodeLinkIndex=0; firstNodeLinkIndex<firstNodeDegree; firstNodeLinkIndex++)
			allNeighborsList.add(network.outLink(firstNode, firstNodeLinkIndex));
		
		for(int secondNodeLinkIndex=0; secondNodeLinkIndex<secondNodeDegree; secondNodeLinkIndex++)
			if(!allNeighborsList.contains(network.outLink(secondNode, secondNodeLinkIndex)))
				allNeighborsList.add(network.outLink(secondNode, secondNodeLinkIndex));
				
		int [] allNeighbors = new int[allNeighborsList.size()];
		for(int index=0;index<allNeighborsList.size();index++)
			allNeighbors[index] = allNeighborsList.get(index);
		return allNeighbors;
	}
	
}
