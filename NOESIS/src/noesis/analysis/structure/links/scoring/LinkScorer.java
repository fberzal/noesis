package noesis.analysis.structure.links.scoring;

import ikor.math.Matrix;
import noesis.Network;
import noesis.analysis.LinkScoreTask;
import noesis.analysis.structure.links.prediction.NodePairMeasureTask;
import noesis.analysis.structure.links.prediction.local.LocalUndirectedNodePairsMeasureTask;

public class LinkScorer extends LinkScoreTask {

	private Matrix computedScores;
	private NodePairMeasureTask score;
	
	public LinkScorer(Network network, NodePairMeasureTask score) {
		super(network);
		this.score = score;
	}

	@Override
	public double compute(int source, int destination) {
		if(score instanceof LocalUndirectedNodePairsMeasureTask) {
			LocalUndirectedNodePairsMeasureTask localScore = (LocalUndirectedNodePairsMeasureTask) score;
			return localScore.compute(source, destination);
		} else {
			if(computedScores==null)
				computedScores = score.call();
			return computedScores.get(source, destination);
		}
	}

	@Override
	public String getName ()
	{
		return score.getName();
	}
	
	@Override
	public String getDescription()
	{
		return score.getDescription();
	}
}
