package noesis.analysis.structure;

import ikor.model.data.annotations.Description;
import ikor.model.data.annotations.Label;
import noesis.Network;
import noesis.analysis.NodeScoreTask;

/**
 * LeaderRank.
 * 
 * @author Victor Martinez (victormg@acm.org)
 * 
 * References:
 * 	- Linyuan L�, Yi-Cheng Zhang , Chi Ho Yeung, Tao Zhou (2011):
 * 	  "Leaders in Social Networks, the Delicious Case"
 *    PLoS ONE 6(6):e21202
 *    https://doi.org/10.1371/journal.pone.0021202
 */

@Label("LeaderRank")
@Description("LeaderRank adaptive and parameter-free algorithm to quantify user influence.")
public class LeaderRank  extends NodeScoreTask
{
	public static final double EPSILON = 1e-4;
	

	public LeaderRank (Network network)
	{
		super(network);
	}

	
	@Override
	public void compute ()
	{
		Network net = getNetwork();
		int     size = net.size();
		double  leaderRank[] = new double[size];
		double  oldLeaderRank[] = new double[size];
		double  olderLeaderRank[] = new double[size];
		double  groundRank;
		double  oldGroundRank;
		double  olderGroundRank;
		boolean changes;

		// Initialization

		for (int i=0;i<size;i++) {
			leaderRank[i] = 1.0;
			oldLeaderRank[i] = 1.0;
		}
		
		groundRank = 0.0;
		oldGroundRank = 0.0;
		olderGroundRank = 0.0;

		// Iterative algorithm

		do {

			// Store current ranks

			for (int i=0; i<size;i++) {
				olderLeaderRank[i] = oldLeaderRank[i];
				oldLeaderRank[i] = leaderRank[i];
			}
			
			olderGroundRank = oldGroundRank;
			oldGroundRank = groundRank;

			// Update ranks

			groundRank = 0;

			for (int node=0; node<size; node++) {

				double value = 0;

				for (int link=0; link<net.inDegree(node); link++) {
					int neighbor = net.inLink(node, link);
					value += oldLeaderRank[neighbor]/(net.outDegree(neighbor)+1);
				}

				value += oldGroundRank/size;
				leaderRank[node] = value;

				groundRank += oldLeaderRank[node]/(net.outDegree(node)+1);
			}

			// Check for convergence
			
			changes = false;

			for (int i=0; i<size; i++)
				if (  Math.abs(leaderRank[i]-oldLeaderRank[i])>EPSILON     // Convergence
				   && Math.abs(leaderRank[i]-olderLeaderRank[i])>EPSILON ) // Absence of oscillation
					changes = true;

			if (  Math.abs(groundRank-oldGroundRank)>EPSILON       // Convergence
			   && Math.abs(groundRank-olderGroundRank)>EPSILON )   // Absence of oscillation due to isolate nodes
				changes = true;

		} while (changes);

		// Final scores
		
		for (int node=0; node<size; node++)
			leaderRank[node] += groundRank/size;

		setResult(leaderRank);
	}	
	

	@Override
	public double compute(int node) 
	{
		checkDone();	
		return getResult(node);
	}	
	
}