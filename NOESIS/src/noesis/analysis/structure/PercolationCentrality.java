package noesis.analysis.structure;

import ikor.collection.Dictionary;
import ikor.collection.DynamicDictionary;
import ikor.collection.List;
import ikor.math.Vector;
import ikor.model.data.annotations.Description;
import ikor.model.data.annotations.Label;
import ikor.parallel.Kernel;
import ikor.parallel.Parallel;
import ikor.parallel.combiner.VectorAccumulator;
import noesis.Network;
import noesis.analysis.NodeScore;
import noesis.analysis.NodeScoreTask;

/**
 * Percolation centrality.
 * 
 * NOTE: This centrality is independently computed for each component in the network.
 * 		 The node centrality in those components with less than two nodes with non-zero percolation
 * 		 values is set to zero.
 * 
 * @author Victor Martinez (victormg@acm.org)
 * 
 * References:
 *  - Mahendra Piraveenan, Mikhail Prokopenko, Liaquat Hossain (2013):
 *    "Percolation centrality: quantifying graph-theoretic impact of nodes during percolation in networks"
 *    PloS one 8(1):e53095
 *    https://doi.org/10.1371/journal.pone.0053095
 */

@Label("Percolation centrality")
@Description("Percolation centrality to quantify node impact during percolation.")
public class PercolationCentrality extends NodeScoreTask 
{
	private List<? extends Number> percolation;
	
	public PercolationCentrality (Network network, List<? extends Number> percolation) 
	{
		super(network);
		this.percolation = percolation;
	}
	
	@Override
	public double compute(int node)  
	{
		checkDone();	
		return getResult(node);
	}

	@Override
	public void compute () 
	{
		Network net = getNetwork();
		int     size = net.size();
		
		// Compute connected components and their size
		ConnectedComponents connectedComponentsTask = new ConnectedComponents(net);
		List<NodeScore> connectedComponents = connectedComponentsTask.call();
		NodeScore components = connectedComponents.get(0);
		NodeScore componentSizes = connectedComponents.get(1);
		
		// Count the number of non-zero percolation nodes and sum them according to their component
		Dictionary<Integer, Integer> componentPercolationCount = new DynamicDictionary();
		Dictionary<Integer, Double> componentPercolationSum = new DynamicDictionary();
		
		for (int node=0; node<size; node++) {
			int component = (int) components.get(node);
			double percolationValue = percolation.get(node).doubleValue();
			
			if ( !componentPercolationCount.contains(component) ) {
				componentPercolationCount.set(component, 0);
				componentPercolationSum.set(component, 0.0);
			}
			
			if ( percolationValue > 0 ) {
				componentPercolationCount.set(component, componentPercolationCount.get(component) + 1);
				componentPercolationSum.set(component, componentPercolationSum.get(component) + percolationValue);
			}
		}
		
		// Compute the centrality of each node
		Kernel<Vector> scoreKernel = new ScoreKernel(net, percolation, components, componentPercolationSum);
		Vector score = (Vector) Parallel.reduce(scoreKernel, new VectorAccumulator(size), 0, size-1);
		
		// Assign scaled final scores for nodes...
		NodeScore measure = new NodeScore(this,net);
		for (int node=0; node<size; node++) {
			int component = (int) components.get(node);
			int componentPercolatednodes = componentPercolationCount.get(component);
			int componentSize = (int) componentSizes.get(node);
			
			// ...only in components satisfying the constraints
			if ((componentSize>2) && (componentPercolatednodes>=2)) {
				measure.set (node, score.get(node)/(componentSize-2) );
			}
		}
		
		setResult(measure);
	}	
	
	// Kernel for computing node scores using betweenness score
	class ScoreKernel implements Kernel<Vector> 
	{	
		private Network net;
		private List<? extends Number> percolation;
		private Dictionary<Integer, Double> componentPercolationSum;
		private NodeScore components;
		
		public ScoreKernel (Network net, List<? extends Number> percolation,
				NodeScore components, Dictionary<Integer, Double> componentPercolationSum) 
		{
			this.net = net;
			this.percolation = percolation;
			this.componentPercolationSum = componentPercolationSum;
			this.components = components;
		}
		
		@Override
		public Vector call (int index) 
		{
			BetweennessScore score = new BetweennessScore(net, index);
			NodeScore result = score.call();
			
			for (int node=0; node<net.nodes(); node++) {
				
				if ((index==node) || (result.get(node)==0.0)) {
					result.set(node, 0);
				} else {
					int component = (int) components.get(index);

					double numerator = (result.get(node)-1)*percolation.get(index).doubleValue();
					double denominator = componentPercolationSum.get(component)-percolation.get(node).doubleValue();
					
					result.set(node, numerator/denominator);
				}
				
			}
			return result;
		}
	}		
}
