package noesis.analysis.structure;

import java.util.Comparator;

import ikor.collection.CollectionFactory;
import ikor.collection.DynamicList;
import ikor.collection.List;

import ikor.model.data.annotations.Description;
import ikor.model.data.annotations.Label;
import noesis.AttributeNetwork;
import noesis.Network;
import noesis.analysis.NodeScore;
import noesis.analysis.NodeScoreTask;

/**
 * Robustness coefficient.
 * 
 * @author Victor Martinez (victormg@acm.org)
 * 
 * References:
 * 	- Mahendra Piraveenan, Gnana Thedchanamoorthy, Shahadat Uddin, Kon Shing Kenneth Chung:
 *    "Quantifying topological robustness of networks under sustained targeted attacks."
 *    Social Network Analysis and Mining
 *    December 2013, Volume 3, Issue 4, pp 939�952
 *    https://doi.org/10.1007/s13278-013-0118-8
 */

@Label("Robustness coefficient")
@Description("Robustness coefficient based on the changing size of the largest component as the network goes through disintegration.")
public class RobustnessCoefficient extends NodeScoreTask 
{
	private List    orderValues;
	private boolean descending;

	public RobustnessCoefficient (Network network, List values) 
	{
		this(network, values, false);
	}

	public RobustnessCoefficient (Network network, List values, boolean descending) 
	{
		super(network);
		this.orderValues = values;
		this.descending = descending;
	}
	
	public RobustnessCoefficient (Network network, NodeScore score) 
	{
		this(network, score, false);
	}

	public RobustnessCoefficient (Network network, NodeScore score, boolean descending) 
	{
		this(network, score.toList(), descending);
	}
	
	@Override
	public void compute ()
	{
		Network  net = new AttributeNetwork(getNetwork());
		int      nodes = net.size();
		double[] robustness = new double[nodes];
		
		DynamicList<Integer> indices = (DynamicList<Integer>) CollectionFactory.createList();
		
		for (int i=0; i<nodes; i++) 
			indices.add(i);

		indices.sort( 
				new Comparator<Integer>() {
					@Override
					public int compare(Integer index1, Integer index2) {
						Comparable obj1 = (Comparable) orderValues.get(index1);
						Comparable obj2 = (Comparable) orderValues.get(index2);

						int comp = obj1.compareTo(obj2);
						return descending ? -comp : comp;
					}
				});
		
		for (int node: indices) {
			
			int[] out = net.outLinks(node);
			
			if (out!=null) 
				for (int link: out) 
					net.remove(node, link);
		
			int[] in = net.inLinks(node);
			
			if (in!=null) 
				for (int link: in) 
					net.remove(link, node);
			
			robustness[node] = getLargestComponentSize(net);
		}
		
		setResult(robustness);
	}
	
	
	public double getRobustness() 
	{
		checkDone();
		
		Network net = getNetwork();
		int nodes = net.size();
		double NxN = nodes*nodes;
		
		double S0 = getLargestComponentSize(net);
		
		NodeScore robustnessScores = this.getResult();
		
		return (2*(robustnessScores.sum()-1)+S0)/NxN;
	}
	
	
	private double getLargestComponentSize (Network network) 
	{
		ConnectedComponents compTask = new ConnectedComponents(network);
		List<NodeScore> scores = compTask.call();
		
		return scores.get(ConnectedComponents.COMPONENT_SIZE).max();
	}
	
	@Override
	public double compute (int node) 
	{
		checkDone();
		return getResult(node);
	}

}
