package noesis.analysis.structure;

import java.util.Set;

import ikor.model.data.annotations.Description;
import ikor.model.data.annotations.Label;
import noesis.Network;
import noesis.algorithms.motifs.BronKerboschCliqueFinder;
import noesis.algorithms.motifs.CliqueFinder;
import noesis.analysis.NodeScore;
import noesis.analysis.NodeScoreTask;

/**
 * Clique centrality.
 * 
 * @author Victor Martinez
 * 
 * Reference:
 *  - Faghani, M. R., & Nguyen, U. T. (2013).
 *    A study of XSS worm propagation and detection mechanisms in online social networks.
 *    IEEE Transactions on Information Forensics and Security, 8(11), 1815-1826.
 */

@Label("cross-clique")
@Description("Cross-clique connectivity")
public class CrossCliqueConnectivity  extends NodeScoreTask
{
	private CliqueFinder cliqueFinder;
	
	public CrossCliqueConnectivity (Network network, CliqueFinder cliqueFinder)
	{
		super(NodeScore.INTEGER_MODEL, network);
		this.cliqueFinder = cliqueFinder;
	}
	
	public CrossCliqueConnectivity (Network network)
	{
		this(network, new BronKerboschCliqueFinder());
	}

	@Override
	public void compute ()
	{
		Network net = getNetwork();
		int nodes = net.nodes();
		double[] result = new double[nodes];
		
		Set<Set<Integer>> cliques = cliqueFinder.compute(net);
		
		for(Set<Integer> clique: cliques)
			for(Integer node: clique)
				result[node]++;
		
		setResult(result);
	}	
		
	@Override
	public double compute(int node) 
	{
		checkDone();
		
		return getResult().get(node);
	}		
}
