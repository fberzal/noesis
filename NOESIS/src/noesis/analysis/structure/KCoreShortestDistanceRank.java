package noesis.analysis.structure;

import ikor.collection.Dictionary;
import ikor.collection.DynamicDictionary;
import ikor.collection.DynamicList;
import ikor.collection.DynamicSet;
import ikor.collection.Set;
import ikor.model.data.annotations.Description;
import ikor.model.data.annotations.Label;
import ikor.parallel.Kernel;
import ikor.parallel.Parallel;
import noesis.LinkEvaluator;
import noesis.Network;
import noesis.algorithms.paths.DijkstraShortestPathFinder;
import noesis.analysis.NodeScore;
import noesis.analysis.NodeScoreTask;

/**
 * K-core shortest distance rank
 * 
 * NOTE: This implementation assigns higher rank values to most influential nodes for the sake of
 * 		 consistency with other influence-related metrics. Nodes with the same influence get the
 * 		 same rank. Since the original paper only considers a connected component, this
 * 		 implementation assigns independent ranks for nodes in each connected component.
 * 		 
 * 
 * @author Victor Martinez (victormg@acm.org)
 * 
 * References:
 *  - Jian-Guo Liu, Zhuo-Ming Ren, Qiang Guo (2013):
 *    "Ranking the spreading influence in complex networks"
 *    Physica A: Statistical Mechanics and its Applications 392(18):4154-4159
 *    https://doi.org/10.1016/j.physa.2013.04.037
 *    
 *  - Joonhyun Bae, Sangwook Kim (2014):
 *    "Identifying and ranking influential spreaders in complex networks by neighborhood coreness"
 *    Physica A: Statistical Mechanics and its Applications 395:549-559
 *    https://doi.org/10.1016/j.physa.2013.10.047
 *    
 */

@Label("K-core shortest distance rank")
@Description("K-core shortest distance rank to quantify spreading influence.")
public class KCoreShortestDistanceRank extends NodeScoreTask 
{
	public KCoreShortestDistanceRank (Network network)
	{
		super(NodeScore.INTEGER_MODEL, network);
	}
	
	@Override
	public double compute(int node) 
	{
		checkDone();
		return getResult(node);
	}
	
	@Override
	public void compute ()
	{
		Network network = getNetwork();
		int nodes = network.nodes();
		double[] scores = new double[nodes];
		
		// Compute coreness and components
		
		Coreness corenessTask = new Coreness(network);
		NodeScore coreness = corenessTask.call();
		
		ConnectedComponents componentsTask = new ConnectedComponents(network);
		NodeScore components = componentsTask.call().get(0);
		
		// Get component largest coreness
		
		Dictionary<Integer,Integer> componentMaxCoreness = new DynamicDictionary();
		
		for (int node=0; node<nodes; node++) {
		
			int nodeComponent = (int) components.get(node);
			int nodeCoreness = (int) coreness.get(node);
			
			if ( !componentMaxCoreness.contains(nodeComponent) || componentMaxCoreness.get(nodeComponent) < nodeCoreness ) {
				componentMaxCoreness.set(nodeComponent, nodeCoreness);
			}
		}
		
		// Get core nodes for each component
		
		Dictionary<Integer,Set<Integer>> componentsCoreNodes = new DynamicDictionary();
		
		for (int node=0; node<nodes; node++) {
			
			int nodeComponent = (int) components.get(node);
			int nodeCoreness = (int) coreness.get(node);
			
			Set<Integer> coreNodes = null;
			
			if (componentsCoreNodes.contains(nodeComponent)) {
				coreNodes = componentsCoreNodes.get(nodeComponent);
			} else {
				coreNodes = new DynamicSet();
				componentsCoreNodes.set(nodeComponent, coreNodes);
			}
			
			if (nodeCoreness == componentMaxCoreness.get(nodeComponent))
				coreNodes.add(node);
		}
		
		// Compute score
		
		ScoreKernel kernel = new ScoreKernel(network, scores, components, componentMaxCoreness, coreness, componentsCoreNodes);
		Parallel.map(kernel, 0, nodes-1);
		
		// Final ranking
		
		// Sort nodes in descending order
		
		DynamicList<Integer> nodeOrder = new DynamicList<Integer>();
		
		for (int node=0; node<nodes; node++)
			nodeOrder.add(node);
		
		nodeOrder.sort((node1,node2)->-Double.compare(scores[node1], scores[node2]));
		
		// Assign component-wise ranks
		
		Dictionary<Integer,Integer> componentCurrentRank = new DynamicDictionary();
		Dictionary<Integer,Double> componentLastScore = new DynamicDictionary();
		
		for (Integer component: componentMaxCoreness.keys()) {
			componentCurrentRank.set(component, 0);
			componentLastScore.set(component, Double.NEGATIVE_INFINITY);
		}
		
		double[] rank = new double[nodes];
		
		for (Integer node: nodeOrder) {
			int nodeComponent = (int) components.get(node);
			int currentRank = componentCurrentRank.get(nodeComponent);
			
			double lastScore = componentLastScore.get(nodeComponent);
			
			if (scores[node] != lastScore) {
				currentRank++;
				componentCurrentRank.set(nodeComponent, currentRank);
				componentLastScore.set(nodeComponent, scores[node]);
			}

			rank[node] = currentRank;
		}
		
		setResult(rank);
	}


	protected class ScoreKernel implements Kernel<Double> 
	{
		private Network network;
		private double[] scores;
		private LinkEvaluator linkEvaluator;
		private NodeScore components;
		private NodeScore coreness;
		private Dictionary<Integer,Integer> componentMaxCoreness;
		private Dictionary<Integer,Set<Integer>> componentCoreNodes;
		
		public ScoreKernel(Network network, double[] scores,  
				NodeScore components, Dictionary<Integer,Integer> componentMaxCoreness, 
				NodeScore coreness, Dictionary<Integer,Set<Integer>> componentsCoreNodes) 
		{	
			this.network = network;
			this.scores = scores;
			this.linkEvaluator = new UnweightedLinkEvaluator(network);
			this.components = components;
			this.coreness = coreness;
			this.componentMaxCoreness = componentMaxCoreness;
			this.componentCoreNodes = componentsCoreNodes;
		}
		
		@Override
		public Double call(int node) 
		{
			int nodeComponent = (int) components.get(node);
			int nodeCoreness = (int) coreness.get(node);
			
			// Compute distance to core nodes
			
			DijkstraShortestPathFinder shortestPathFinder = new DijkstraShortestPathFinder(network, node, linkEvaluator);
			shortestPathFinder.run();
			double[] distances = shortestPathFinder.distance();
			
			double distanceSum = 0;
			for (Integer coreNode: componentCoreNodes.get(nodeComponent))
				distanceSum += distances[coreNode];
			
			// Compute score
			
			scores[node] = (componentMaxCoreness.get(nodeComponent)-nodeCoreness+1) * distanceSum;
			
			return scores[node];
		}
	}
	
	protected class UnweightedLinkEvaluator implements LinkEvaluator 
	{	
		private Network<?,?> net;
		
		public UnweightedLinkEvaluator (Network<?,?> net)
		{
			this.net = net;
		}
		
		@Override
		public double evaluate (int source, int destination) 
		{
			return net.get(source,destination)==null ? Double.POSITIVE_INFINITY : 1;
		}
	}
}