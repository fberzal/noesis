package noesis.analysis.structure;

import java.util.Iterator;

import ikor.collection.DynamicSet;
import ikor.collection.Set;
import ikor.model.data.annotations.Description;
import ikor.model.data.annotations.Label;
import noesis.Network;
import noesis.analysis.NodeScore;
import noesis.analysis.NodeScoreTask;

/**
 * Coreness.
 * 
 * The k-core of a graph is the subgraph composed of nodes with degree equal or greater than k (in the subgraph).
 * The k-shell of a graph is the subgraph composed of nodes in the k-core, but not in the (k+1)-core.
 * 
 * The coreness of a node is equal to the k value of its k-shell.
 * 
 * @author Victor Martinez (victormg@acm.org)
 * 
 * References:
 * 	- Sergey N. Dorogovtsev, Alexander V. Goltsev, J. F. F. Mendes (2006):
 * 	  "k-Core Organization of Complex Networks"
 *    Physical review letters, 96(4), 040601
 *    https://doi.org/10.1103/PhysRevLett.96.040601
 * 
 */

@Label("Coreness")
@Description("Node core number based on k-shell decomposition.")
public class Coreness extends NodeScoreTask 
{
	public Coreness (Network network)
	{
		super(NodeScore.INTEGER_MODEL, network);
	}
	
	@Override
	public double compute(int node) 
	{
		checkDone();
		return getResult(node);
	}
	
	
	@Override
	public void compute ()
	{
		Network network = getNetwork();
		
		int nodes = network.nodes();
		int currentShell = 0;
		double shell[] = new double[nodes];
		
		// Create remaining node set 
		
		Set<Integer> remainingNodes = new DynamicSet<Integer>();
		for (int node=0; node<nodes; node++)
			remainingNodes.add(node);
		
		// As long as there are nodes in the set of remaining nodes...
		
		while (remainingNodes.size()>0) {
			
			Iterator<Integer> nodeIter = remainingNodes.iterator();
			boolean anyRemoved = false;
			
			while (nodeIter.hasNext()) {
				
				int node = nodeIter.next();

				// ... compute node in-degree ...
				
				int inDegree = 0;
				
				for (int link=0; link<network.inDegree(node); link++) {	
					int neighbor = network.inLink(node, link);
					if (remainingNodes.contains(neighbor))
						inDegree++;
				}
				
				// ... and remove when necessary
				
				if (inDegree <= currentShell) {					
					shell[node] = currentShell;
					nodeIter.remove();
					anyRemoved = true;					
				}				
			}
			
			// Increase current shell if no node was removed
			
			if( !anyRemoved ) {
				currentShell++;
			}
		}
		
		setResult(shell);
	}
}