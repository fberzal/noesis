package noesis.model.random;

// Title:       Holme-Kim network model
// Version:     1.0
// Copyright:   2018
// Author:      Victor Martinez
// E-mail:      victormg@acm.org

import ikor.math.random.Random;

import ikor.model.data.annotations.Description;
import ikor.model.data.annotations.Label;

import noesis.Network;
import noesis.Parameter;

/**
 * Holme-Kim network model.
 * 
 * @author Victor Martinez
 * 
 * Reference:
 *  - Holme, P., & Kim, B. J. (2002).
 *    "Growing scale-free networks with tunable clustering"
 *    Physical Review E, 65(2), 026107.
 *    https://doi.org/10.1103/PhysRevE.65.026107 
 *    
 */

public class HolmeKimNetwork extends RandomNetwork 
{
	private static final int DEFAULT_NODES = 50;
	private static final int DEFAULT_LINKS_PER_NODE = 3;
	private static final double DEFAULT_TRIAD_PROBABILITY = 0.25;
	
	@Label("Nodes")
	@Description("Number of network nodes")
	@Parameter(min=0, defaultValue=DEFAULT_NODES)
	private int nodes;
	
	@Label("Links")
	@Description("Links for each new node")
	@Parameter(min=0, defaultValue=DEFAULT_LINKS_PER_NODE)
	private int linksPerNode;
	
	@Label("Triad probability")
	@Description("Triad formation probability")
	@Parameter(min=0, max=1, defaultValue=DEFAULT_TRIAD_PROBABILITY)
	private double triadProbability;
	
	public HolmeKimNetwork ()
	{
		this(DEFAULT_NODES, DEFAULT_LINKS_PER_NODE, DEFAULT_TRIAD_PROBABILITY);
	}
	
	public HolmeKimNetwork (int nodes, int linksPerNode, double triadProbability)
	{
		this.nodes = nodes;
		this.linksPerNode = linksPerNode;
		this.triadProbability = triadProbability;
		
		this.create();
	}
	
	
	public Network create()
	{
		this.setID("HOLME-KIM NETWORK (n="+nodes+", c="+linksPerNode+", p="+triadProbability+")");
		this.setSize(nodes);
		
		int ends[] = new int[2*nodes*linksPerNode];
		int linkCount = 0;
		
		for (int node=0; node<nodes; node++) {
			int lastNeighbor = -1;
			int targetNode;
			boolean doPAStep;
			
			for (int link=0; link<linksPerNode; link++) {
			
				targetNode=-1;
				
				if(lastNeighbor==-1)
					doPAStep = true;
				else
					doPAStep = false;
				
				if (!doPAStep && Random.random()<=triadProbability) {
					int unconnectedNodes = getUnconnectedNodes(this, node, lastNeighbor);
					
					if (unconnectedNodes>0) {
						targetNode = triadFormationStep(this, node, lastNeighbor, unconnectedNodes);
					} else {
						doPAStep = true;
					}
				}
				
				if (doPAStep) {
					targetNode = preferentialAttachmentStep(this, node, ends, linkCount);
					lastNeighbor = targetNode;
				}
				
				if (targetNode!=-1) {
					this.add2(node, targetNode);
					ends[linkCount] = node;
					ends[linkCount+1] = targetNode;
					linkCount+=2;
				}				
			}
		}
		
		return this;
	}

	// Ancillary methods
	
	private int getUnconnectedNodes (Network net, int node, int neighbor)
	{
		int unconnectedNodes = 0;
		
		for (int link=0; link<net.outDegree(neighbor); link++) {
			int targetNode = net.outLink(neighbor, link);
			if (!net.contains(node, targetNode) && targetNode!=node)
				unconnectedNodes++;
		}
		
		return unconnectedNodes;
	}
	
	private int triadFormationStep (Network net, int node, int neighbor, int unconnectedNodes)
	{
		int skipNodes = Random.random(unconnectedNodes);
		int targetLink = 0;
		int targetNode = net.outLink(neighbor, targetLink);
			
		while (skipNodes>0 || net.contains(node,targetNode)) {
			if (!net.contains(node,targetNode))
				skipNodes--;
			targetLink++;
			targetNode = net.outLink(neighbor, targetLink);
		}
			
		return targetNode;
	}
	
	private int preferentialAttachmentStep (Network net, int node, int ends[], int linkCount)
	{
		int unconnectedNodes = node-net.outDegree(node);
		int target = -1;
		
		if (unconnectedNodes>0) {
			
			do {
				target = ends[Random.random(linkCount)];
			} while (net.contains(node, target) && node!=target);			
		}
		
		return target;
	}
}
