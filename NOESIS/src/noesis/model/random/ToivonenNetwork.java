package noesis.model.random;

// Title:       Toivonen network model
// Version:     1.0
// Copyright:   2018
// Author:      Victor Martinez
// E-mail:      victormg@acm.org

import ikor.math.random.Random;

import ikor.model.data.annotations.Description;
import ikor.model.data.annotations.Label;

import noesis.Network;
import noesis.Parameter;

/**
 * Toivonen network.
 * 
 * @author Victor Martinez
 * 
 * Reference:
 *  - Toivonen, R., Onnela, J. P., Saramaki, J., Hyvonen, J., & Kaski, K. (2006):
 *    "A model for social networks"
 *    Physica A: Statistical Mechanics and its Applications, 371(2), 851-860.
 *    http://doi.org/10.1016/j.physa.2006.03.050
 * 
 */

public class ToivonenNetwork extends RandomNetwork 
{
	private static final int DEFAULT_NODES = 50;
	private static final int DEFAULT_MAX_INITIAL_CONTACTS = 3;
	private static final int DEFAULT_MAX_SECONDARY_CONTACTS = 1;
	
	@Label("Nodes")
	@Description("Number of network nodes")
	@Parameter(min=0, defaultValue=DEFAULT_NODES)
	private int nodes;
	
	@Label("Max initial contacts")
	@Description("Maximum number of initial contacts")
	@Parameter(min=1, defaultValue=DEFAULT_MAX_INITIAL_CONTACTS)
	private int maxInitialContacts;
	
	@Label("Max secondary contacts")
	@Description("Maximum number of secondary contacts")
	@Parameter(min=0, defaultValue=DEFAULT_MAX_SECONDARY_CONTACTS)
	private int maxSecondaryContacts;
	
	public ToivonenNetwork ()
	{
		this(DEFAULT_NODES, DEFAULT_MAX_INITIAL_CONTACTS, DEFAULT_MAX_SECONDARY_CONTACTS);
	}
	
	public ToivonenNetwork (int nodes, int maxInitialContacts, int maxSecondaryContacts)
	{
		this.nodes = nodes;
		this.maxInitialContacts = maxInitialContacts;
		this.maxSecondaryContacts = maxSecondaryContacts;
		
		this.create();
	}
	
	public Network create()
	{
		this.setID("TOIVONEN NETWORK (n="+nodes+", i="+maxInitialContacts+", s="+maxSecondaryContacts+")");
		this.setSize(nodes);
		
		for (int node=0; node<nodes; node++) {
			
			// Add initial contacts
			int initialContactCount = Random.random(maxInitialContacts)+1;
			for (int i=0; i<initialContactCount; i++) {
				addInitialContact(this, node);
			}
			
			// Add secondary contacts
			int[] initialContacts = this.outLinks(node);
			if (initialContacts!=null) {
				for (int initialContact: initialContacts) {
					int secondaryContactCount = Random.random(maxSecondaryContacts+1);
					for (int j=0; j<secondaryContactCount; j++) {
						addSecondaryContact(this, node, initialContact);
					}
				}
			}			
		}
		
		return this;
	}
	
	// Ancillary methods
	
	private void addInitialContact (Network net, int node)
	{
		int unconnectedNodes = node-net.outDegree(node);
		
		if (unconnectedNodes>0) {
			int skipNodes = Random.random(unconnectedNodes);
			int targetNode = 0;
			
			while (skipNodes>0 || net.contains(node,targetNode)) {
				if (!net.contains(node,targetNode))
					skipNodes--;
				targetNode++;
			}
			
			net.add2(node, targetNode);
		}
	}
	
	private void addSecondaryContact(Network net, int node, int initialContact)
	{
		int unconnectedNodes = 0;
		
		for (int i=0; i<net.outDegree(initialContact); i++) {
			int targetNode = net.outLink(initialContact, i);
			if (!net.contains(node, targetNode) && targetNode!=node)
				unconnectedNodes++;
		}
		
		if (unconnectedNodes>0) {
			int skipNodes = Random.random(unconnectedNodes);
			int targetLink = 0;
			int targetNode = net.outLink(initialContact, targetLink);
			
			while (skipNodes>0 || net.contains(node,targetNode)) {
				
				if (!net.contains(node,targetNode))
					skipNodes--;
				
				targetLink++;
				targetNode = net.outLink(initialContact, targetLink);
			}
			
			net.add2(node, targetNode);
		}
	}
}
