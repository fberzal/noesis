package noesis.model.random;

import ikor.collection.DynamicSet;
import ikor.collection.Set;

import ikor.math.random.Random;
import ikor.math.statistics.Distribution;

/**
 * Watts-Strogatz with custom degree distribution.
 * 
 * @author Victor Martinez
 */
public class WattsStrogatzDegreeDistributionNetwork extends RandomNetwork 
{
	
	public WattsStrogatzDegreeDistributionNetwork (int nodes, int links, Distribution degreeDistribution) 
	{
		setID("WATTS-STROGATZ DEGREE-DISTRIBUTION NETWORK (n="+nodes+",m="+links+")");
		setSize(nodes);

		double[] ends = new double[nodes];
		
		// Generate random numbers and get min
		
		double min = Double.MAX_VALUE;
		
		for (int i=0; i<ends.length; i++) {
			ends[i] = degreeDistribution.random();
			if (min>ends[i]) 
				min = ends[i];
		}
		
		// Only positive values and compute sum

		double sum = 0;
		
		for (int i=0; i<ends.length; i++) {
			sum += ends[i];
			if (min<0) 
				ends[i] -= min;
		}
		
		// Scale
		
		for (int i=0; i<ends.length; i++) {
			if(sum==0) 
				ends[i] = Math.round(links/nodes);
			else 
				ends[i] = Math.round((ends[i]/sum)*links);
		}
		
		// Node wiring
		
		Set<Integer> activeNodes = new DynamicSet<Integer>();
		
		for (int node=0; node<nodes; node++) 
			if (ends[node]>0) 
				activeNodes.add(node);
		
		while (activeNodes.size()>1) {
			
			// Choose link ends
			int chosen1 = chooseRandomNode(activeNodes);
			int chosen2 = chooseUnconnectedRandomNode(activeNodes, chosen1);
			
			if (chosen2==-1) {
				
				activeNodes.remove(chosen1);
				
			} else {
			
				// Add link and reduce end count
				add2(chosen1, chosen2);
				ends[chosen1]--;
				ends[chosen2]--;

				// Check if node should be removed
				if(checkIfConnectedToAll(chosen1, activeNodes) || ends[chosen1]<=0) 
					activeNodes.remove(chosen1);
				if(checkIfConnectedToAll(chosen2, activeNodes) || ends[chosen2]<=0) 
					activeNodes.remove(chosen2);
			}
		}
	}
	
	
	private int chooseRandomNode (Set<Integer> activeSet) 
	{
		int chosen = Random.random(activeSet.size());
		
		for (int element: activeSet)
			if(chosen==0) 
				return element;
			else 
				chosen--;
		
		return -1;
	}
	
	private int chooseUnconnectedRandomNode (Set<Integer> activeSet, int node) 
	{
		int unconnected = 0;
		
		for (int element: activeSet)
			if (!contains(node, element) && node!=element)
				unconnected++;
		
		int chosen = Random.random(unconnected);
		
		for (int element : activeSet)
			if (!contains(node, element) && node!=element) {
				if (chosen==0) 
					return element;
				chosen--;
			}
		
		return -1;
	}
	
	private boolean checkIfConnectedToAll (int node, Set<Integer> activeSet) 
	{
		for (int element: activeSet)
			if (!contains(node, element) && node!=element)
				return false;
		
		return true;
	}
}
