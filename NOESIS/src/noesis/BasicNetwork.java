package noesis;

// Title:       Network ADT
// Version:     1.1
// Copyright:   2012-2015
// Author:      Fernando Berzal
// E-mail:      berzal@acm.org


/**
 * Basic network ADT implementation using arrays
 * 
 * @author Fernando Berzal
 */
public class BasicNetwork extends Network<Integer,Integer> 
{
	private int size;
	private int nlinks;
	
	private int[][] inLinks;
	private int[][] outLinks;

	/**
	 * Network nodes, O(1)
	 */
	@Override
	public int nodes() 
	{
		return size;
	}
	
	/**
	 * Network resizing, O(nodes)
	 */
	@Override
	public final void setSize(int size) 
	{
		int     oldSize;
		int[][] oldInLinks;
		int[][] oldOutLinks;
		
		oldSize     = this.size;
		oldInLinks  = this.inLinks;
		oldOutLinks = this.outLinks;

		this.size  = size;
		this.inLinks  = new int[size][]; 
		this.outLinks = new int[size][]; 
		
		if (oldInLinks!=null)
			System.arraycopy(oldInLinks,0,inLinks,0,oldSize);

		if (oldOutLinks!=null)
			System.arraycopy(oldOutLinks,0,outLinks,0,oldSize);
	}

	/**
	 * Network links, O(1)
	 */
	@Override
	public int links() 
	{
		return nlinks;
	}

	/**
	 * Node addition, including network resizing when needed, O(nodes)
	 */
	@Override
	public final int add(Integer node) 
	{
		int pos = node;
					
		if (pos>=size)
			setSize(pos+1);

		return pos;
	}
	
	/**
	 * Node removal, O(degree^2)
	 * @param node Node index
	 */
	@Override
	public boolean remove (int node) 
	{
		if ((node>=0) && (node<size)) {
		
			// Link removal
			
			for (int i=inDegree(node)-1; i>=0; i--)
				remove ( inLink(node,i), node);
					
			for (int i=outDegree(node)-1; i>=0; i--)
				remove ( node, outLink(node,i) );
			
			// Replace removed node with the last node
			
			inLinks[node] = inLinks[size-1];
			outLinks[node] = outLinks[size-1];
			
			size--;
			
			// Link update (last node => removed node position)
			
			for (int i=0; i<inDegree(node); i++)
				redirectOutLink ( inLink(node,i), size, node);
			
			for (int i=0; i<outDegree(node); i++)
				redirectInLink ( size, outLink(node,i), node);
			
			return true;
			
		} else {
			return false;
		}
	}
	
	// e.g. Google web (875k nodes, 5.1M links, 21MB GZIP, 75MB TXT )
	// - Incremental extension (+1):                            
	//     55s = 21s (I/O) + 13s (node creation) + 21s (link creation)
	//     (without additional memory requirements)
	// - Multiplicative extension with Pascal-like arrays (*2):
	//     41s = 21s (I/O) + 13s (node creation) + 7s (link creation)
	//     (2*size() additional integer values, i.e. inLinks[i][0] & outLinks[i][0])
	
	private final static int INITIAL_ARRAY_SIZE = 4;
	private final static int START_INDEX = 1;
	
	/** 
	 * Dynamic array extension, amortized O(1)
	 * @param array Original array
	 * @param value Value to be appended at the end of the array
	 * @return array after appending the provided value, resized when needed
	 */
	private final int[] extend (int[] array, int value)
	{
		int   dim;
		int[] newArray;
		
		if (array==null) {
			
			newArray = new int[INITIAL_ARRAY_SIZE];
			newArray[0] = 1;
			newArray[1] = value;
		
		} else {
			
			dim = array[0]+1;	
			
			if (dim<array.length) {
				newArray = array;
			} else {
				newArray = new int[2*array.length];
				System.arraycopy(array,0,newArray,0,array.length);
			}

			newArray[0] = dim; 
			newArray[dim] = value;	
		}		
		
		return newArray;
	}

	// Links
	
	/**
	 * Add new link, O(degree) to avoid link duplicates, amortized O(1) dynamic array resizing
	 * @param source Source node
	 * @param destination Destination node
	 */
	@Override
	public boolean add(int source, int destination) 
	{
		if (  (source>=0) 
		   && (source<size())
		   && (destination>=0)
		   && (destination<size())
		   && !contains(source,destination) ) {
			
			nlinks++;
			outLinks[source] = extend(outLinks[source], destination);
			inLinks[destination] = extend(inLinks[destination], source);
			return true;
			
		} else {
			
			return false;
		}
	}

	/**
	 * Add link with attached value
	 * @throws UnsupportedOperationException in basic networks
	 */
	@Override
	public final boolean add(int sourceIndex, int destinationIndex, Integer value) 
	{
		throw new UnsupportedOperationException("Unsupported operation on basic networks");
	}

	/**
	 * Link removal, O(degree)
	 * @param source Source node
	 * @param destination Destination node
	 */
	@Override
	public boolean remove (int source, int destination)
	{
		int indexOut = getOutLinkIndex(source, destination);
		int indexIn = getInLinkIndex(destination, source);
		
		if (indexOut!=-1) {  // && (indexIn!=-1)
			
			for (int i=indexOut; i<outDegree(source)-1; i++)
				setOutLink( source, i, outLink(source,i+1) );
			
			setOutDegree(source, outDegree(source)-1);
			
			for (int i=indexIn; i<inDegree(destination)-1; i++)
				setInLink( destination, i, inLink(destination,i+1) );
			
			setInDegree(destination, inDegree(destination)-1);
			
			nlinks--;
			
			return true;
			
		} else {
			return false;
		}
	}
	
	/**
	 * Out-link redirection, O(degree)
	 * @param source Source node
	 * @param destination Original destination node
	 * @param target New destination node
	 */
	private void redirectOutLink (int source, int destination, int target)
	{
		int index = getOutLinkIndex(source, destination);

		if (index!=-1)
			setOutLink( source, index, target );
	}
	
	/**
	 * In-link redirection, O(degree)
	 * @param source Original source node
	 * @param destination Destination node
	 * @param target New original node
	 */
	private void redirectInLink (int source, int destination, int target)
	{
		int index = getInLinkIndex(destination, source);
		
		if (index!=-1)
			setInLink( destination, index, target);
	}
	
	/**
	 * Network node, O(1)
	 */
	@Override
	public Integer get(int index) 
	{
		return index;
	}
	
	/**
	 * Set node value
	 * @throws UnsupportedOperationException in basic networks
	 */
	@Override
	public void set (int index, Integer value)
	{
		throw new UnsupportedOperationException("Unsupported operation on basic networks");		
	}
	

	/**
	 * Link index, O(degree)
	 * @param source Source node
	 * @param destination Destination node
	 * @return Link index
	 */
	public int getLinkIndex(int source, int destination)
	{
		return getOutLinkIndex(source,destination);
	}

	/**
	 * Out-link index, O(degree)
	 * @param source Source node
	 * @param destination Destination node
	 * @return Link index
	 */
	private int getOutLinkIndex(int source, int destination)
	{
		for (int i=0; i<outDegree(source); i++)
			if (outLink(source,i) == destination)
				return i;
		
		return -1;
	}
	
	/**
	 * In-link index, O(degree)
	 * @param destination Destination node
	 * @param source Source node
	 * @return Link index
	 */
	private int getInLinkIndex (int destination, int source)
	{
		for (int i=0; i<inDegree(destination); i++)
			if (inLink(destination,i) == source)
				return i;
		
		return -1;
	}
	
	/**
	 * Link access, O(degree)
	 */
	@Override
	public Integer get (int source, int destination) 
	{
		if (outLinks[source]!=null)
			for (int i=0; i<outLinks[source][0]; i++)
				if (outLink(source,i) == destination)
					return destination;
		
		return null;
	}

	/**
	 * Link lookup, O(degree)
	 */
	@Override
	public boolean contains (int source, int destination) 
	{
		if (outLinks[source]!=null)
			for (int i=0; i<outLinks[source][0]; i++)
				if (outLink(source,i) == destination)
					return true;
		
		return false;
	}
	

	@Override
	public boolean contains(Integer node) 
	{
		return (node>=0) && (node<size);
	}

	@Override
	public int index(Integer node) 
	{	
		return node;
	}


	@Override
	public int inDegree(int node) 
	{
		if ((inLinks!=null) && (inLinks[node]!=null))
			return inLinks[node][0];
		else
			return 0;
	}
	
	private void setInDegree (int node, int degree)
	{
		if ((inLinks!=null) && (inLinks[node]!=null))
			inLinks[node][0] = degree;
	}

	@Override
	public int outDegree(int node) 
	{
		if ((outLinks!=null) && (outLinks[node]!=null))
			return outLinks[node][0];
		else
			return 0;
	}
	
	private void setOutDegree (int node, int degree)
	{
		if ((outLinks!=null) && (outLinks[node]!=null))
			outLinks[node][0] = degree;
	}

	
	@Override
	public int outLink (int node, int index)
	{
		return outLinks[node][START_INDEX+index];
	}
	
	private void setOutLink (int node, int index, int destination)
	{
		outLinks[node][START_INDEX+index] = destination;
	}


	@Override
	public int inLink (int node, int index)
	{
		return inLinks[node][START_INDEX+index];
	}

	private void setInLink (int node, int index, int source)
	{
		inLinks[node][START_INDEX+index] = source;
	}

	
	@Override
	public String toString ()
	{
		return "["+super.toString()+"] "+size()+" nodes, "+links()+" links.";
	}

}
