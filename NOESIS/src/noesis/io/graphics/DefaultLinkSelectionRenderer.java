package noesis.io.graphics;

import java.awt.Color;

import ikor.model.graphics.Line;
import ikor.model.graphics.Style;
import ikor.model.ui.DatasetSelection;
import ikor.util.indexer.Indexer;

public class DefaultLinkSelectionRenderer extends LinkSelectionRenderer 
{
	public static final Style DEFAULT_UNSELECTED_STYLE = new Style(new Color(0x00, 0x00, 0x00, 0x00));
	public static final Color DEFAULT_SELECTED_COLOR = new Color(0xB0, 0x00, 0x00, 0xBB);
	public static final int   DEFAULT_SELECTED_WIDTH = 4;
	
	public DefaultLinkSelectionRenderer (NetworkRenderer networkRenderer, DatasetSelection linkSelection) 
	{
		super(networkRenderer, linkSelection);
	}

	@Override
	public void render (NetworkRenderer drawing, int source, int target) 
	{
		/*
		drawing.add( 
				new Line ( 
					getLinkSelectionID(drawing.getLinkId(source, target)), 
					getStyle(source, target),
					drawing.getX(source), drawing.getY(source), 
					drawing.getX(target), drawing.getY(target) 
				) );
		*/

		int linkIndex = networkRenderer.getNetwork().index(source, target);
		
		if (linkSelection.contains(linkIndex)) {

			Line link = (Line) drawing.getDrawingElement( drawing.getLinkId(source, target) );
		
			if (link!=null)
				link.setStyle( getStyle(source, target) );
		}
	}

	@Override
	public void update(NetworkRenderer drawing, int source, int target) 
	{
		/*
		Line link = (Line) drawing.getDrawingElement( getLinkSelectionID( drawing.getLinkId(source, target) ) );
		
		if (link!=null) {			
			link.setStartX( drawing.getX(source) );
			link.setStartY( drawing.getY(source) );
			link.setEndX( drawing.getX(target) );
			link.setEndY( drawing.getY(target) );
			link.setStyle( getStyle(source, target) );
		}
		*/
		
		int linkIndex = networkRenderer.getNetwork().index(source, target);
		
		if (linkSelection.contains(linkIndex)) {
			
			Line link = (Line) drawing.getDrawingElement( drawing.getLinkId(source, target) );
		
			if (link!=null)
				link.setStyle( getStyle(source, target) );
		}
	}
	
	// Selected link style

	private Indexer<Long> previousIndexer;
	private int previousWidth;
	private Style[] cache;	

	@Override
	public Style getStyle(int source, int target) 
	{
		int linkIndex = networkRenderer.getNetwork().index(source, target);
		
		if (linkSelection.contains(linkIndex)) {
			return getSelectedStyle(source,target);
		} else {
			return DEFAULT_UNSELECTED_STYLE;
		}
	}

	public Style getSelectedStyle (int source, int target) 
	{
		LinkRenderer  renderer = networkRenderer.getLinkRenderer();
		Indexer<Long> currentIndexer = renderer.getWidthIndexer();
		int           currentWidth = renderer.getWidth();
		
		if (cache==null || currentIndexer!=previousIndexer || currentWidth!=previousWidth) {
			int  range = (currentIndexer==null) ? 0 : currentIndexer.range();
			cache = new Style[currentWidth+range+1];
			previousIndexer = currentIndexer;
			previousWidth = currentWidth;
		}

		int linkWidth = renderer.getWidth(source, target);
		if (cache[linkWidth]==null) {
			cache[linkWidth] = new Style(DEFAULT_SELECTED_COLOR,  DEFAULT_SELECTED_WIDTH + linkWidth );
		}
		return cache[linkWidth];
	}
	
}
