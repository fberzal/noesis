package noesis.io.graphics;

import ikor.model.ui.DatasetSelection;

public abstract class LinkSelectionRenderer extends LinkRenderer 
{
	private static final String SELECTION_ID = "-selection";
	
	protected NetworkRenderer  networkRenderer;
	protected DatasetSelection linkSelection;
	
	public LinkSelectionRenderer(NetworkRenderer networkRenderer, DatasetSelection linkSelection) 
	{
		this.networkRenderer = networkRenderer;
		this.linkSelection = linkSelection;
	}
	
	public String getLinkSelectionID (String linkID) 
	{
		return linkID+SELECTION_ID;
	}
	
	public DatasetSelection getDatasetSelection() 
	{
		return linkSelection;
	}
	
	public void setDatasetSelection(DatasetSelection linkSelection) 
	{		
		this.linkSelection = linkSelection;
	}

	@Override
	public abstract void render(NetworkRenderer drawing, int source, int target);

	@Override
	public abstract void update(NetworkRenderer drawing, int source, int target);

}
