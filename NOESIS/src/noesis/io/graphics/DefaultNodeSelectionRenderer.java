package noesis.io.graphics;

import java.awt.Color;

import ikor.model.graphics.Circle;
import ikor.model.graphics.Style;
import ikor.model.ui.DatasetSelection;

public class DefaultNodeSelectionRenderer extends NodeSelectionRenderer 
{	
	public static final Style DEFAULT_BODY_STYLE = new Style ( new Color(0x00, 0x00, 0x00, 0x00));
	public static final Style DEFAULT_UNSELECTED_BORDER_STYLE = new Style ( new Color(0x00, 0x00, 0x00, 0x00), 0);
	public static final Style DEFAULT_SELECTED_BORDER_STYLE = new Style ( new Color(0xB0, 0x00, 0x00, 0xFF), 3);
	
	public DefaultNodeSelectionRenderer (NetworkRenderer networkRenderer, DatasetSelection nodeSelection) 
	{
		super(networkRenderer, nodeSelection);
	}

	@Override
	public void render (NetworkRenderer drawing, int node) 
	{
		int x = drawing.getX(node);
		int y = drawing.getY(node);
		Style borderStyle = getStyle(node);

		Circle selectionCircle = new Circle ( getNodeSelectionID(drawing.getNodeId(node)), DEFAULT_BODY_STYLE, 
				borderStyle, x, y, networkRenderer.getNodeRenderer().getSize(node));
		drawing.add ( selectionCircle );
	}

	@Override
	public void update(NetworkRenderer drawing, int node) 
	{
		int x = drawing.getX(node);
		int y = drawing.getY(node);
		
		Circle circle = (Circle) drawing.getDrawingElement( getNodeSelectionID(drawing.getNodeId(node)) );

		if (circle!=null) {
			Style borderStyle = getStyle(node);
			circle.setCenterX(x);
			circle.setCenterY(y);
			circle.setRadius( networkRenderer.getNodeRenderer().getSize(node) );
			circle.setStyle(DEFAULT_BODY_STYLE);
			circle.setBorder(borderStyle);
		}
	}

	@Override
	public Style getStyle(int node) 
	{
		return nodeSelection.contains(node)? DEFAULT_SELECTED_BORDER_STYLE : DEFAULT_UNSELECTED_BORDER_STYLE;
	}

}
