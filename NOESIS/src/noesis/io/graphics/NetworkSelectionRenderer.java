package noesis.io.graphics;

import ikor.model.ui.DatasetSelection;
import noesis.AttributeNetwork;

public class NetworkSelectionRenderer extends NetworkRenderer 
{
	private NodeSelectionRenderer nodeSelectionRenderer;
	private LinkSelectionRenderer linkSelectionRenderer;
	
	public NetworkSelectionRenderer(AttributeNetwork network, DatasetSelection nodeSelection, DatasetSelection linkSelection, int width, int height) 
	{
		super(network, width, height);
		this.nodeSelectionRenderer = new DefaultNodeSelectionRenderer(this, nodeSelection);
		this.linkSelectionRenderer = new DefaultLinkSelectionRenderer(this, linkSelection);
	}

	public NodeSelectionRenderer getNodeSelectionRenderer() 
	{
		return nodeSelectionRenderer;
	}

	public void setNodeSelectionRenderer(NodeSelectionRenderer nodeSelectionRenderer) 
	{
		this.nodeSelectionRenderer = nodeSelectionRenderer;
	}
	
	public LinkSelectionRenderer getLinkSelectionRenderer() 
	{
		return linkSelectionRenderer;
	}

	public void setLinkSelectionRenderer(LinkSelectionRenderer linkSelectionRenderer) 
	{
		this.linkSelectionRenderer = linkSelectionRenderer;
	}
	
	@Override
	public void render (int node)
	{
		super.render(node);
		if (getNetwork()!=null)
			nodeSelectionRenderer.render(this, node);
	}
	
	@Override
	public void update (int node)
	{
		super.update(node);
		if (getNetwork()!=null)
			nodeSelectionRenderer.update(this, node);
	}
	
	@Override
	public void render (int source, int target)
	{
		super.render(source, target);
		linkSelectionRenderer.render(this, source, target);
	}
	
	@Override
	public void update (int source, int target)
	{
		super.update(source, target);
		linkSelectionRenderer.update(this, source, target);
	}
}
