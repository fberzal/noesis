package noesis.io.graphics;

import ikor.model.ui.DatasetSelection;

public abstract class NodeSelectionRenderer extends NodeRenderer 
{
	private static final String SELECTION_ID = "-selection";
	
	protected NetworkRenderer  networkRenderer;
	protected DatasetSelection nodeSelection;
	
	public NodeSelectionRenderer(NetworkRenderer networkRenderer, DatasetSelection nodeSelection) 
	{
		this.networkRenderer = networkRenderer;
		this.nodeSelection = nodeSelection;
	}
	
	public String getNodeSelectionID (String nodeID) 
	{
		return nodeID+SELECTION_ID;
	}
	
	public DatasetSelection getDatasetSelection() 
	{
		return nodeSelection;
	}
	
	public void setDatasetSelection(DatasetSelection nodeSelection) 
	{
		this.nodeSelection = nodeSelection;
	}
	
	@Override
	public abstract void render (NetworkRenderer drawing, int node);
	
	@Override
	public abstract void update (NetworkRenderer drawing, int node);
	
}
