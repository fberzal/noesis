package noesis.io.dataset;

import java.io.Writer;
import java.io.BufferedWriter;
import java.io.IOException;

import ikor.model.data.DataModel;
import ikor.model.data.Dataset;
import ikor.model.data.TextModel;


public class DatasetWriter 
{
	public static final String DEFAULT_STRING_DELIMITER = "\"";
	public static final String DEFAULT_COLUMN_SEPARATOR = ",";
	public static final String DEFAULT_ROW_SEPARATOR = "\n";
	
	private BufferedWriter writer;
	private String columnSeparator;
	private String rowSeparator;
	private String stringDelimiter;
	
	public DatasetWriter (Writer writer, String columnSeparator, String rowSeparator, String stringDelimiter) 
	{
		this.writer = new BufferedWriter(writer);
		this.columnSeparator = columnSeparator;
		this.rowSeparator = rowSeparator;
		this.stringDelimiter = stringDelimiter;
	}
	
	public DatasetWriter(Writer writer) 
	{
		this(writer, DEFAULT_COLUMN_SEPARATOR, DEFAULT_ROW_SEPARATOR, DEFAULT_STRING_DELIMITER);
	}

	
	public void write (Dataset dataset) 
		throws IOException 
	{
		writeRows(dataset);
		writer.flush();
	}

	public void close() 
		throws IOException 
	{
		writer.close();
	}
	
	
	public void writeHeader (String[] headers) 
		throws IOException 
	{
		if (headers!=null) {

			for (int i=0; i<headers.length; i++) {

				if (i!=0)
					writer.write(columnSeparator);

				writer.write(formatString(headers[i]));
			}

			writer.write(rowSeparator);
			writer.flush();
		}
	}

	
	private void writeRows (Dataset dataset) 
		throws IOException 
	{
		for (int row=0; row<dataset.getRowCount(); row++) {
			for (int col=0; col<dataset.getColumnCount(); col++) {
				
				if (col!=0) 
					writer.write(columnSeparator);
				
				Object value = dataset.get(row, col);
				DataModel model = dataset.getModel(col);
				
				if (value!=null) {
					
					if (model instanceof TextModel) {
						writer.write(formatString(model.toString(value)));
					} else {
						writer.write(model.toString(value));
					}
				}
			}
			
			writer.write(rowSeparator);
		}
		
	}

	
	private String formatString (String string) 
	{
		return stringDelimiter + string.replace(stringDelimiter, "\\"+stringDelimiter) + stringDelimiter;
	}
	
}
