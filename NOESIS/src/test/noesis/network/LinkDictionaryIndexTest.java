package test.noesis.network;

import static org.junit.Assert.*;

import noesis.BasicNetwork;
import noesis.network.LinkDictionaryIndex;

import org.junit.Before;
import org.junit.Test;

public class LinkDictionaryIndexTest 
{
	BasicNetwork base;
	LinkDictionaryIndex index;
	
	public static final int NETWORK_SIZE = 10;
	
	@Before
	public void setUp() throws Exception 
	{
		base = new BasicNetwork();
		index = new LinkDictionaryIndex(base);
		
		base.setSize(NETWORK_SIZE);

		for (int i=0; i<NETWORK_SIZE; i++) {
			for (int j=i+1; j<NETWORK_SIZE; j++) {
				index.add(i, j);
				base.add(i, j);
			}
		}		
	}
	

	@Test
	public void testIndex() 
	{
		assertEquals(base, index.network());
		assertEquals(NETWORK_SIZE, index.nodes());
		assertEquals(NETWORK_SIZE*(NETWORK_SIZE-1)/2, index.links());
				
		// Links

		int link = 0;
		
		for (int i=0; i<NETWORK_SIZE; i++) {
			for (int j=i+1; j<NETWORK_SIZE; j++) {
				assertEquals(link, index.index(i, j));
				link++;
				assertEquals(-1, index.index(j, i)); // No reverse links
			}
			assertEquals(-1, index.index(i, i)); // No loops
		}		
	}

	
	@Test
	public void testSource() 
	{
		int link = 0;
		
		for (int i=0; i<NETWORK_SIZE; i++) {
			for (int j=i+1; j<NETWORK_SIZE; j++) {
				assertEquals(i, index.source(link));
				link++;
			}
		}
		
		assertEquals(-1, index.source(link));
	}

	@Test
	public void testDestination() 
	{
		int link = 0;
		
		for (int i=0; i<NETWORK_SIZE; i++) {
			for (int j=i+1; j<NETWORK_SIZE; j++) {
				assertEquals(j, index.destination(link));
				link++;
			}
		}		

		assertEquals(-1, index.destination(link));
	}	
	
	
	@Test
	public void testRemoveFirst ()
	{
		for (int i=1; i<NETWORK_SIZE; i++) {
			index.remove(0, i);
			base.remove(0, i);
		}
		
		assertEquals(NETWORK_SIZE, base.nodes());
		assertEquals(NETWORK_SIZE, index.nodes());
		assertEquals(NETWORK_SIZE*(NETWORK_SIZE-1)/2-(NETWORK_SIZE-1), base.links());
		assertEquals(NETWORK_SIZE*(NETWORK_SIZE-1)/2-(NETWORK_SIZE-1), index.links());

		int link = 0;

		for (int i=1; i<NETWORK_SIZE; i++)
			assertEquals(-1, index.index(0, i));
		
		for (int i=1; i<NETWORK_SIZE; i++) {
			for (int j=i+1; j<NETWORK_SIZE; j++) {
				assertEquals(link, index.index(i, j));
				assertEquals(i, index.source(link));
				assertEquals(j, index.destination(link));
				link++;
			}
		}				
	}
	
	@Test
	public void testRemoveLast ()
	{
		for (int i=0; i<NETWORK_SIZE-1; i++) {
			index.remove(i, NETWORK_SIZE-1);
			base.remove(i, NETWORK_SIZE-1);
		}
		
		assertEquals(NETWORK_SIZE, base.nodes());
		assertEquals(NETWORK_SIZE, index.nodes());
		assertEquals(NETWORK_SIZE*(NETWORK_SIZE-1)/2-(NETWORK_SIZE-1), base.links());
		assertEquals(NETWORK_SIZE*(NETWORK_SIZE-1)/2-(NETWORK_SIZE-1), index.links());

		int link = 0;

		for (int i=0; i<NETWORK_SIZE-1; i++)
			assertEquals(-1, index.index(i, NETWORK_SIZE-1));
		
		for (int i=0; i<NETWORK_SIZE; i++) {
			for (int j=i+1; j<NETWORK_SIZE-1; j++) {
				assertEquals(link, index.index(i, j));
				assertEquals(i, index.source(link));
				assertEquals(j, index.destination(link));
				link++;
			}
		}		
		
	}
	
}
