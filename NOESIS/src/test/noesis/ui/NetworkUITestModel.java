package test.noesis.ui;

import ikor.math.random.Random;

import ikor.model.ui.Action;
import ikor.model.ui.Application;
import ikor.model.ui.Image;
import ikor.model.ui.Option;
import ikor.model.ui.UIModel;

import noesis.Attribute;
import noesis.AttributeNetwork;

import noesis.ui.model.NetworkFigure;
import noesis.ui.model.NetworkModel;
import noesis.ui.model.actions.ExitAction;

public class NetworkUITestModel  extends UIModel
{
	private NetworkModel  model;
	private NetworkFigure figure;
	
	private UIModel       buttons;
	
	// Constructor

	public NetworkUITestModel (Application app, AttributeNetwork network)
	{
		super(app,"NOESIS Network Application");

		app.addObserver(this);
		
		// setAlignment( UIModel.Alignment.ADJUST );
		// setAlignment( UIModel.Alignment.LEADING );
		// setAlignment( UIModel.Alignment.TRAILING );
		
		add( new Option("$exit", new ExitAction(app) ) );
		
		add( new Image("$icon", app.url("icon.gif") ) );
		
		add( new Image("$background", app.url("logo.gif") ) );
		
		model = new NetworkModel( network );
		
		figure = new NetworkFigure(model);
			    
		// Nested panels
		
		UIModel panel = new UIModel(app, "Container panel");

		panel.setAlignment( UIModel.Alignment.TRAILING );		
	    panel.add(figure);

		buttons = new UIModel(app, "Button bar");
		panel.add( buttons );
		
		this.add(panel);
		
		// Buttons
		
		buttons.setAlignment( UIModel.Alignment.ADJUST );
	
		Option addNodeOption = new Option("Add node");
		addNodeOption.setIcon( app.url("icon.gif") );
		addNodeOption.setAction( new AddNodeAction(app) );
		buttons.add(addNodeOption);
		
		Option removeNodeOption = new Option("Remove node");
		removeNodeOption.setIcon( app.url("icon.gif") );
		removeNodeOption.setAction( new RemoveNodeAction(app) );
		buttons.add(removeNodeOption);
			    
		Option addLinkOption = new Option("Add link");
		addLinkOption.setIcon( app.url("icon.gif") );
		addLinkOption.setAction( new AddLinkAction(app) );
		buttons.add(addLinkOption);
		
		Option removeLinkOption = new Option("Remove link");
		removeLinkOption.setIcon( app.url("icon.gif") );
		removeLinkOption.setAction( new RemoveLinkAction(app) );
		buttons.add(removeLinkOption);

		// menu = new NetworkAppMenu(this);  // donde NetworkAppMenu es una subclase de Menu
		// add(menu);
	}	
	
	
	// Getters & setters
	
	public NetworkModel getModel() 
	{
		return model;
	}
	
	public void setModel(NetworkModel model) 
	{
		this.model = model;
	}

	public NetworkFigure getFigure() 
	{
		return figure;
	}

	public void setFigure(NetworkFigure figure) 
	{
		this.figure = figure;
	}
	
	
	// Actions
	
	public class AddNodeAction extends Action 
	{
		private Application app;
		private int nodes = 0;

		public AddNodeAction (Application app)
		{
			this.app = app;
		}	
		
		@Override
		public void run() 
		{
			AttributeNetwork net = (AttributeNetwork) app.get("network");
			int index = net.nodes();
			
			net.add(index);
			Attribute<String> id = net.getNodeAttribute("id");
			Attribute<Double> x = net.getNodeAttribute("x");
			Attribute<Double> y = net.getNodeAttribute("y");
			
			nodes++;
			
			id.set(index, ""+nodes );
			x.set(index, 0.1+0.8*Random.random() );
			y.set(index, 0.1+0.8*Random.random() );
			
			figure.render();
		}
	}

	
	public class RemoveNodeAction extends Action 
	{
		private Application app;

		public RemoveNodeAction (Application app)
		{
			this.app = app;
		}	
		
		@Override
		public void run() 
		{
			AttributeNetwork net = (AttributeNetwork) app.get("network");
			
			if (net.nodes()>0)
				net.remove( Random.random(net.nodes()) );
			
			figure.render();
		}
	}

	
	public class AddLinkAction extends Action 
	{
		private Application app;

		public AddLinkAction (Application app)
		{
			this.app = app;
		}	
		
		@Override
		public void run() 
		{
			AttributeNetwork net = (AttributeNetwork) app.get("network");
			
			if (net.nodes()>=2) {
				
				int source = Random.random(net.nodes());
				int destination;
				
				do {
					destination = Random.random(net.nodes());
				} while (destination==source);
				
				net.add2(source, destination);  // Bidirectional link
				
				figure.render();
			}
		}
	}

	
	public class RemoveLinkAction extends Action 
	{
		private Application app;

		public RemoveLinkAction (Application app)
		{
			this.app = app;
		}	
		
		@Override
		public void run() 
		{
			AttributeNetwork net = (AttributeNetwork) app.get("network");
			
			if (net.links()>0) {
				int index = Random.random(net.links());
				net.remove2( net.source(index), net.destination(index) );
				figure.render();
			}
		}
	}
	

}
