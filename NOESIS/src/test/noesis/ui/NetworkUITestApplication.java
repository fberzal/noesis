package test.noesis.ui;

import ikor.model.data.RealModel;
import ikor.model.ui.Application;
import ikor.model.ui.UIBuilder;
import ikor.model.ui.swing.SwingUIBuilder;

import javax.swing.UIManager;

import noesis.Attribute;
import noesis.AttributeNetwork;


public class NetworkUITestApplication extends Application
{
	// Application resources
	
	private static final String RESOURCE_PATH = "noesis/ui/resources/";
	
	public String url (String resource)
	{
		return RESOURCE_PATH+resource;
	}
	
	
	// Application state
	
	private AttributeNetwork network;
	
	public AttributeNetwork getNetwork()
	{
		return network;
	}

	public AttributeNetwork createNetwork ()
	{
		network = new AttributeNetwork();
		
		RealModel coordinateModel = new RealModel();
		coordinateModel.setMinimumValue(0.0);
		coordinateModel.setMaximumValue(1.0);										

		Attribute x = new Attribute<Double>("x", coordinateModel);
		Attribute y = new Attribute<Double>("y", coordinateModel);

		network.addNodeAttribute( new Attribute("id") );
		network.addNodeAttribute(x);
		network.addNodeAttribute(y);
	

		return network;
	}
	
	
	// Application entry point
	
	public static void main(String[] args) 
	{	
		// Windows look&feel
		
		try {
			UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
		} catch (Exception e) {
			e.printStackTrace();
		}
		   
		// UI generation
		
		UIBuilder builder = new SwingUIBuilder(); // new ConsoleUIBuilder();
		NetworkUITestApplication app = new NetworkUITestApplication();
		
		app.set("network", app.createNetwork() );
		app.setBuilder(builder);
		app.setStartup(new NetworkUITestModel(app, app.getNetwork()));
		app.run();
	}

}
