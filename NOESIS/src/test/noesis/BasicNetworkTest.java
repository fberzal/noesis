package test.noesis;

import static org.junit.Assert.*;

import noesis.BasicNetwork;

import org.junit.Before;
import org.junit.Test;


public class BasicNetworkTest 
{
	private static final int NETWORK_SIZE = 10;
	private static final int NETWORK_LINKS = NETWORK_SIZE*(NETWORK_SIZE-1)/2;

	BasicNetwork net;
	
	@Before
	public void setUp() throws Exception 
	{
		net = new BasicNetwork();
		
		for (int i=0; i<NETWORK_SIZE; i++)
			net.add(i);


		for (int i=0; i<NETWORK_SIZE; i++)
			for (int j=i+1; j<NETWORK_SIZE; j++)
				net.add(i,j);
	}
	
	// Constructor
	
	@Test
	public void testConstructor() 
	{
		assertEquals( NETWORK_SIZE, net.size() );
		assertEquals( NETWORK_LINKS, net.links() );
		
		for (int i=0; i<NETWORK_SIZE; i++) {
			assertEquals ( i, net.inDegree(i) );
			assertEquals ( NETWORK_SIZE-i-1, net.outDegree(i) );
			
			assertEquals ( i, net.index(i) );
			
			for (int j=0; j<net.inDegree(i); j++)
				assertEquals ( j, net.inLink(i,j));

			for (int j=0; j<net.outDegree(i); j++)
				assertEquals ( i+j+1, net.outLink(i,j));
		}
	}
	
	// Add

	@Test
	public void testAddNode ()
	{
		net.add(NETWORK_SIZE);
		
		assertEquals( NETWORK_SIZE+1, net.size() );
		assertEquals( NETWORK_LINKS, net.links() );
		
		for (int i=0; i<NETWORK_SIZE; i++) {
			assertEquals ( i, net.inDegree(i) );
			assertEquals ( NETWORK_SIZE-i-1, net.outDegree(i) );
		}
		
		assertEquals ( 0, net.inDegree(NETWORK_SIZE));
		assertEquals ( 0, net.outDegree(NETWORK_SIZE));
		assertEquals ( NETWORK_SIZE, net.index(NETWORK_SIZE) );
	}

	@Test
	public void testAddLinks ()
	{
		for (int i=1; i<NETWORK_SIZE; i++)
			net.add(i,0);
		
		assertEquals( NETWORK_SIZE, net.size() );
		assertEquals( NETWORK_LINKS + (NETWORK_SIZE-1), net.links() );
		
		assertEquals ( NETWORK_SIZE-1, net.inDegree(0) );
		assertEquals ( NETWORK_SIZE-1, net.outDegree(0) );
		
		for (int i=1; i<NETWORK_SIZE; i++) {
			assertEquals ( i, net.inDegree(i) );
			assertEquals ( NETWORK_SIZE-i, net.outDegree(i) );
		}
	}

	@Test
	public void testAddNodeWithLinks ()
	{
		net.add(NETWORK_SIZE);

		for (int i=0; i<NETWORK_SIZE; i++) {
			net.add(NETWORK_SIZE, i);
			net.add(i, NETWORK_SIZE);
		}
		
		assertEquals( NETWORK_SIZE + 1, net.size() );
		assertEquals( NETWORK_LINKS + 2*NETWORK_SIZE, net.links() );
		
		for (int i=0; i<NETWORK_SIZE; i++) {
			assertEquals ( i+1, net.inDegree(i) );
			assertEquals ( NETWORK_SIZE-i, net.outDegree(i) );
		}
		
		assertEquals ( NETWORK_SIZE, net.inDegree(NETWORK_SIZE));
		assertEquals ( NETWORK_SIZE, net.outDegree(NETWORK_SIZE));
	}

	// Remove
	
	@Test
	public void testRemoveNodeWithOutLinks ()
	{
		net.remove(0);
		
		assertEquals( NETWORK_SIZE-1, net.size() );
		assertEquals( NETWORK_LINKS -(NETWORK_SIZE-1), net.links() );

		// Previous last node, now at removed node position
		
		assertEquals ( (NETWORK_SIZE-1)-1, net.inDegree(0) );
		assertEquals ( 0, net.outDegree(0) );

		// Remaining nodes

		for (int i=1; i<NETWORK_SIZE-1; i++) {
			assertEquals ( i-1, net.inDegree(i) );
			assertEquals ( NETWORK_SIZE-i-1, net.outDegree(i) );
		}		
		
		// Adjusted links
		
		for (int i=0; i<net.inDegree(0); i++)
			assertEquals ( i+1, net.inLink(0,i));
		
		for (int i=1; i<NETWORK_SIZE-1; i++)
			assertEquals ( 0, net.outLink(i, net.outDegree(i)-1 ));
	}

	@Test
	public void testRemoveNodeWithInLinks ()
	{
		net.remove(NETWORK_SIZE-1);
		
		assertEquals( NETWORK_SIZE-1, net.size() );
		assertEquals( NETWORK_LINKS-(NETWORK_SIZE-1), net.links() );

		for (int i=0; i<NETWORK_SIZE-1; i++) {
			assertEquals ( i, net.inDegree(i) );
			assertEquals ( NETWORK_SIZE-i-2, net.outDegree(i) );
		}
	}
	
	@Test
	public void testClear ()
	{
		net.clear();

		assertEquals( 0, net.size() );
		assertEquals( 0, net.links() );
	}

	@Test
	public void testAddNodeAndRemove ()
	{
		net.add(NETWORK_SIZE);

		for (int i=0; i<NETWORK_SIZE; i++) {
			net.add(NETWORK_SIZE, i);
			net.add(i, NETWORK_SIZE);
		}

		assertEquals( NETWORK_SIZE+1, net.size() );
		assertEquals( NETWORK_LINKS+2*NETWORK_SIZE, net.links() );
		
		net.remove(NETWORK_SIZE);

		assertEquals( NETWORK_SIZE, net.size() );
		assertEquals( NETWORK_LINKS, net.links() );
		
		for (int i=0; i<NETWORK_SIZE; i++) {
			assertEquals ( i, net.inDegree(i) );
			assertEquals ( NETWORK_SIZE-i-1, net.outDegree(i) );
		}
	}
	
	@Test
	public void testAddNodeAndClear ()
	{
		net.add(NETWORK_SIZE);

		for (int i=0; i<NETWORK_SIZE; i++) {
			net.add(NETWORK_SIZE, i);
			net.add(i, NETWORK_SIZE);
		}

		assertEquals( NETWORK_SIZE+1, net.size() );
		assertEquals( NETWORK_LINKS+2*NETWORK_SIZE, net.links() );
		
		net.clear();

		assertEquals( 0, net.size() );
		assertEquals( 0, net.links() );
	}


	@Test
	public void testAddAndRemoveLinks ()
	{
		for (int i=1; i<NETWORK_SIZE; i++)
			net.add(i,0);

		assertEquals( NETWORK_SIZE, net.size() );
		assertEquals( NETWORK_LINKS+NETWORK_SIZE-1, net.links() );

		for (int i=1; i<NETWORK_SIZE; i++)
			net.remove(i,0);
		
		assertEquals( NETWORK_SIZE, net.size() );
		assertEquals( NETWORK_LINKS, net.links() );
		
		for (int i=0; i<NETWORK_SIZE; i++) {
			assertEquals ( i, net.inDegree(i) );
			assertEquals ( NETWORK_SIZE-i-1, net.outDegree(i) );
		}
	}

	@Test
	public void testRemoveFirstLinks ()
	{
		for (int i=0; i<NETWORK_SIZE-1; i++)
			net.remove(i, i+1);
		
		assertEquals( NETWORK_SIZE, net.size() );
		assertEquals( NETWORK_LINKS-(NETWORK_SIZE-1), net.links() );
		
		assertEquals ( 0, net.inDegree(0) );
		assertEquals ( NETWORK_SIZE-2, net.outDegree(0) );

		for (int i=1; i<NETWORK_SIZE-1; i++) {
			assertEquals ( i-1, net.inDegree(i) );
			assertEquals ( NETWORK_SIZE-i-2, net.outDegree(i) );
		}

		assertEquals ( NETWORK_SIZE-2, net.inDegree(NETWORK_SIZE-1) );
		assertEquals ( 0, net.outDegree(NETWORK_SIZE-1) );
	}

	@Test
	public void testRemoveLastLinks ()
	{
		for (int i=0; i<NETWORK_SIZE-1; i++)
			net.remove(i, NETWORK_SIZE-1);
		
		assertEquals( NETWORK_SIZE, net.size() );
		assertEquals( NETWORK_LINKS-(NETWORK_SIZE-1), net.links() );
		
		assertEquals ( 0, net.inDegree(0) );
		assertEquals ( NETWORK_SIZE-2, net.outDegree(0) );

		for (int i=1; i<NETWORK_SIZE-1; i++) {
			assertEquals ( i, net.inDegree(i) );
			assertEquals ( NETWORK_SIZE-i-2, net.outDegree(i) );
		}

		assertEquals ( 0, net.inDegree(NETWORK_SIZE-1) );
		assertEquals ( 0, net.outDegree(NETWORK_SIZE-1) );
	}
	
}
