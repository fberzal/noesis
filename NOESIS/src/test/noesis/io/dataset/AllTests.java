package test.noesis.io.dataset;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses( { DatasetWriterTest.class})
public class AllTests 
{
}
