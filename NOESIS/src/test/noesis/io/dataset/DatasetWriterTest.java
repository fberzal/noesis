package test.noesis.io.dataset;

import static org.junit.Assert.assertEquals;

import java.awt.Color;
import java.io.IOException;
import java.io.StringWriter;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.junit.Test;

import ikor.model.data.BooleanModel;
import ikor.model.data.ColorModel;
import ikor.model.data.DataModel;
import ikor.model.data.Dataset;
import ikor.model.data.DateModel;
import ikor.model.data.IntegerModel;
import ikor.model.data.RealModel;
import ikor.model.data.TextModel;

import noesis.io.dataset.DatasetWriter;


public class DatasetWriterTest 
{

	// Test dataset 
	
	private static class TestDataset extends Dataset 
	{	
		private static Date createDate(String dateString) 
		{
			DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
			Date date = null;
			try {
				date = dateFormat.parse(dateString);
			} catch (ParseException e) {
				e.printStackTrace();
			}
			return date;
		}

		private static final DataModel[] columnModels = {
				new BooleanModel(), 
				new ColorModel(), 
				new DateModel(),
				new IntegerModel(), 
				new RealModel(), 
				new TextModel()
		};

		private static final Object[][] rawData = {
				{true, Color.RED, createDate("06-04-1989"), 32, 64.3564, "test 1"},
				{false, null, createDate("22-04-1989"),  null, 12.4343, null},
				{null, Color.ORANGE, createDate("11-12-1845"), 64, 34.2343, "test \"2\""},
		};

		@Override
		public Object get(int row, int column) 
		{
			return rawData[row][column];
		}

		@Override
		public void set(int row, int column, Object object) 
		{ 
			throw new UnsupportedOperationException(); 
		}

		@Override 
		public int getColumnCount() 
		{ 
			return rawData[0].length; 
		}

		@Override 
		public int getRowCount() 
		{ 
			return rawData.length; 
		};

		@Override 
		public DataModel getModel(int column) 
		{ 
			return columnModels[column]; 
		}
	}
	
	
	private static final String[] columnNames =
		{"bool_col", "color_col", "date_col", "int_col", "real_col", "txt_col"};
	
	private static final Dataset dataset = new TestDataset();
	
	
	@Test
	public void testCSV() 
		throws IOException 
	{
		StringWriter sw = new StringWriter();
		DatasetWriter writer = new DatasetWriter(sw);
		
		writer.writeHeader(columnNames);
		writer.write(dataset);
		writer.close();
		
		String expected =
			"\"bool_col\",\"color_col\",\"date_col\",\"int_col\",\"real_col\",\"txt_col\"\n" +
			"true,#ff0000,6/04/89,32,64.356,\"test 1\"\n" +
			"false,,22/04/89,,12.434,\n" +
			",#ffc800,11/12/45,64,34.234,\"test \\\"2\\\"\"\n";

		assertEquals(expected, sw.toString());
	}

	@Test
	public void testCSVSingleQuote() 
		throws IOException 
	{
		StringWriter sw = new StringWriter();
		DatasetWriter writer = new DatasetWriter(sw, ",", "\n", "'");
		
		writer.writeHeader(columnNames);
		writer.write(dataset);
		writer.close();
		
		String expected =
			"'bool_col','color_col','date_col','int_col','real_col','txt_col'\n" +
			"true,#ff0000,6/04/89,32,64.356,'test 1'\n" +
			"false,,22/04/89,,12.434,\n" +
			",#ffc800,11/12/45,64,34.234,'test \"2\"'\n";

		assertEquals(expected, sw.toString());
	}

	@Test
	public void testTSV() 
		throws IOException 
	{
		StringWriter sw = new StringWriter();
		DatasetWriter writer = new DatasetWriter(sw, "\t", "\n", "\"");
		
		writer.writeHeader(columnNames);
		writer.write(dataset);
		writer.close();
		
		String expected =
			"\"bool_col\"\t\"color_col\"\t\"date_col\"\t\"int_col\"\t\"real_col\"\t\"txt_col\"\n" +
			"true\t#ff0000\t6/04/89\t32\t64.356\t\"test 1\"\n" +
			"false\t\t22/04/89\t\t12.434\t\n" +
			"\t#ffc800\t11/12/45\t64\t34.234\t\"test \\\"2\\\"\"\n";
		
		assertEquals(expected, sw.toString());
	}
	
}
