package test.noesis;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * NOESIS test suite
 * 
 * @author Fernando Berzal (berzal@acm.org)
 */
@RunWith(Suite.class)
@Suite.SuiteClasses( { test.noesis.BasicNetworkTest.class,
	                   test.noesis.DynamicNetworkTest.class,
	                   test.noesis.AttributeNetworkTest.class,
	                   test.noesis.SampleNetworkTests.class,
	                   test.noesis.ParameterTest.class,
	                   test.noesis.algorithms.AllTests.class,
					   test.noesis.analysis.AllTests.class,
					   test.noesis.io.AllTests.class,
					   test.noesis.model.AllTests.class,
					   test.noesis.network.AllTests.class,})
public class AllTests {

}
