package test.noesis.algorithms.motifs;

import static org.junit.Assert.assertEquals;

import java.util.HashSet;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;

import noesis.BasicNetwork;
import noesis.Network;
import noesis.algorithms.motifs.CliqueFinder;
import noesis.model.regular.CompleteNetwork;
import noesis.model.regular.StarNetwork;

public abstract class CliqueFinderTest 
{	
	private Network empty;
	private Network complete;
	private Network star;
	
	public abstract CliqueFinder createCliqueFinder();
	
	@Before
	public void setUp() throws Exception 
	{
		empty = new BasicNetwork();
		complete = new CompleteNetwork(4);
		star = new StarNetwork(4);
	}
	
	@Test
	public void testEmpty()
	{
		CliqueFinder cf = createCliqueFinder();
		Set<Set<Integer>> cliques = cf.compute(empty);
		
		assertEquals(0, cliques.size());
	}
	
	@Test
	public void testComplete()
	{
		CliqueFinder cf = createCliqueFinder();
		Set<Set<Integer>> cliques = cf.compute(complete);
		
		assertEquals(15, cliques.size());
		
		assertEquals(false, cliques.contains(createSet()));
		
		assertEquals(true, cliques.contains(createSet(0)));
		assertEquals(true, cliques.contains(createSet(1)));
		assertEquals(true, cliques.contains(createSet(2)));
		assertEquals(true, cliques.contains(createSet(3)));
		
		assertEquals(true, cliques.contains(createSet(0,1)));
		assertEquals(true, cliques.contains(createSet(0,2)));
		assertEquals(true, cliques.contains(createSet(0,3)));
		assertEquals(true, cliques.contains(createSet(1,2)));
		assertEquals(true, cliques.contains(createSet(1,3)));		
		assertEquals(true, cliques.contains(createSet(2,3)));
		
		assertEquals(true, cliques.contains(createSet(0,1,2)));
		assertEquals(true, cliques.contains(createSet(0,2,3)));
		assertEquals(true, cliques.contains(createSet(0,1,3)));
		assertEquals(true, cliques.contains(createSet(1,2,3)));
		
		assertEquals(true, cliques.contains(createSet(0,1,2,3)));
	}
	
	@Test
	public void testStar()
	{
		CliqueFinder cf = createCliqueFinder();
		Set<Set<Integer>> cliques = cf.compute(star);

		assertEquals(7, cliques.size());
		
		assertEquals(false, cliques.contains(createSet()));
		
		assertEquals(true, cliques.contains(createSet(0)));
		assertEquals(true, cliques.contains(createSet(1)));
		assertEquals(true, cliques.contains(createSet(2)));
		assertEquals(true, cliques.contains(createSet(3)));
		
		assertEquals(true, cliques.contains(createSet(0,1)));
		assertEquals(true, cliques.contains(createSet(0,2)));
		assertEquals(true, cliques.contains(createSet(0,3)));
	}
	
	protected Set<Integer> createSet(int... elements) 
	{
    	Set<Integer> clique = new HashSet<Integer>();
    	
    	for (int element: elements) 
    		clique.add(element);
    	
    	return clique;
    }
}
