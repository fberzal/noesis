package test.noesis.algorithms.motifs;

import noesis.algorithms.motifs.BronKerboschCliqueFinder;
import noesis.algorithms.motifs.CliqueFinder;

public class BronKerboschCliqueFinderTest extends CliqueFinderTest 
{
	@Override
	public CliqueFinder createCliqueFinder() 
	{
		return new BronKerboschCliqueFinder();
	}
}
