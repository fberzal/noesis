package test.noesis.algorithms.motifs;

import noesis.algorithms.motifs.BruteForceCliqueFinder;
import noesis.algorithms.motifs.CliqueFinder;

public class BruteForceCliqueFinderTest extends CliqueFinderTest 
{
	@Override
	public CliqueFinder createCliqueFinder() 
	{
		return new BruteForceCliqueFinder();
	}
}
