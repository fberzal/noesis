package test.noesis.algorithms.motifs;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses( { 
	BruteForceCliqueFinderTest.class,
	BronKerboschCliqueFinderTest.class
})
public class AllTests {

}
