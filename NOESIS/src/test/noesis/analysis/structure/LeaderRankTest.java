package test.noesis.analysis.structure;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

import noesis.BasicNetwork;
import noesis.Network;
import noesis.analysis.NodeScore;
import noesis.analysis.structure.LeaderRank;
import noesis.model.regular.BinaryTreeNetwork;
import noesis.model.regular.CompleteNetwork;
import noesis.model.regular.IsolateNetwork;
import noesis.model.regular.RingNetwork;
import noesis.model.regular.StarNetwork;


public class LeaderRankTest 
{
	public final double EPSILON = 1e-3;

	@Before
	public void setUp() throws Exception 
	{
	}
	
	private void checkLeaderRank (Network net, double[] rank)
	{
		LeaderRank task = new LeaderRank(net);
		NodeScore leaderRank = task.getResult();
		
		for (int i=0; i<net.size(); i++)
			assertEquals ( rank[i], leaderRank.get(i), EPSILON); 
	}
	
	
	@Test
	public void testCompleteNetwork ()
	{
		Network net = new CompleteNetwork(5);
		
		checkLeaderRank(net, new double[] {1.0, 1.0, 1.0, 1.0, 1.0});
	}

		
	@Test
	public void testDisconnectedNetwork ()
	{
		Network net = new IsolateNetwork(5);
		
		checkLeaderRank(net, new double[] {1.0, 1.0, 1.0, 1.0, 1.0});
	}

	@Test
	public void testStarNetwork5 ()
	{
		Network net = new StarNetwork(5);
		
		checkLeaderRank(net, new double[] {1.666, 0.833, 0.833, 0.833, 0.833});
	}

	@Test
	public void testStarNetwork10 ()
	{
		Network net = new StarNetwork(10);
		
		checkLeaderRank(net, new double[] {2.8947, 0.7895, 0.7895, 0.7895, 0.7895, 
				                           0.7895, 0.7895, 0.7895, 0.7895, 0.7895});
	}

	@Test
	public void testTreeNetwork ()
	{
		Network net = new BinaryTreeNetwork(7);
		
		checkLeaderRank(net, new double[] {1.077, 1.346, 1.346, 0.807, 0.807, 0.807, 0.807});
	}

	@Test
	public void testTriangleNetwork ()
	{
		Network net = new RingNetwork(3);
		
		checkLeaderRank(net, new double[] {1.000, 1.000, 1.000});
	}


	@Test
	public void testIsolateNode ()
	{
		Network net = new BasicNetwork();
		
		net.setSize(3);
		net.add2(0, 1);
		
		checkLeaderRank(net, new double[] {1.125, 1.125, 0.750});
	}

}
