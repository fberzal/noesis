package test.noesis.analysis.structure;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import ikor.collection.DynamicList;
import ikor.collection.List;
import noesis.BasicNetwork;
import noesis.Network;
import noesis.analysis.NodeScore;
import noesis.analysis.structure.PercolationCentrality;
import noesis.model.regular.CompleteNetwork;
import noesis.model.regular.StarNetwork;

public class PercolationCentralityTest 
{
	public final double EPSILON = 1e-3;

	// Examples from the original paper: https://doi.org/10.1371/journal.pone.0053095
	
	Network network;
	
	double[] networkPercolation = {0.1, 0.2, 0.2, 0.2, 0.3, 0.2, 0.5, 0.5};
	double[] networkScores = {0, 0, 0, 0.625, 0.605, 0.667, 0, 0};
	
	double[] networkPercolation2 = {0.3, 0.5, 0.5, 0.2, 0.3, 0.2, 0.1, 0.1};
	double[] networkScores2 = {0, 0, 0, 0.825, 0.535, 0.4, 0, 0};
	
	double[] networkPercolationZero = {0, 0, 0, 0, 0, 0, 0, 0};
	double[] networkScoresZero = {0, 0, 0, 0, 0, 0, 0, 0};
	
	// All nodes are disconnected so there are no percolated paths
	
	Network disconnected;
	
	double[] disconnectedPercolation = {0, 0.5, 0.5, 0.5, 0};
	double[] disconnectedScores = {0, 0, 0, 0, 0};
	
	// There are no percolated paths for less than three nodes
	
	Network twoNodes;
	
	double[] twoNodesPercolation = {0.5, 0.5};
	double[] twoNodesScores = {0, 0};
	
	// All nodes are connected so there are no percolated paths
	
	Network complete;
	
	double[] completePercolation = {0, 0.5, 0 , 0.5, 0};
	double[] completeScores = {0, 0, 0, 0, 0};
	
	// All percolated paths go through the central node
	
	Network star;
	
	double[] starPercolation = {0.3, 1.0, 0.0, 0.4, 0.0};
	double[] starScores = {1, 0, 0, 0, 0};
	
	// Only one node has a non-zero percolation value in the second star, so nodes in that component are set to zero
	
	Network twoStars;
	
	double[] twoStarsPercolation = {0.3, 0.5, 0.5 , 0, 0, 0.5};
	double[] twoStarsScores = {1, 0, 0, 0, 0, 0};
	
	// All percolated paths go through the central nodes in each component
	
	double[] twoStarsPercolation2 = {0.1, 0.4, 0.5 , 0.3, 0.3, 0.4};
	double[] twoStarsScores2 = {1, 0, 0, 1, 0, 0};
	
	// JUnit test cases
	
	@Before
	public void setUp() throws Exception 
	{
		network = new BasicNetwork();
		network.setSize(8);
		
		network.add2(0,3);
		network.add2(1,3);
		network.add2(2,3);
		
		network.add2(3,4);
		network.add2(4,5);
		
		network.add2(5,6);
		network.add2(5,7);
		
		disconnected = new BasicNetwork();
		disconnected.setSize(5);
		
		twoNodes = new BasicNetwork();
		twoNodes.setSize(2);
		twoNodes.add2(0, 1);
		
		complete = new CompleteNetwork(5);
		
		star = new StarNetwork(5);
		
		twoStars = new BasicNetwork();
		twoStars.setSize(6);
		
		twoStars.add2(0, 1);
		twoStars.add2(0, 2);
		twoStars.add2(3, 4);
		twoStars.add2(3, 5);
	}
	
	private void checkPercolationCentrality (Network net, double[] percolation, double[] values)
	{
		List<Double> percolationValues = new DynamicList<Double>();
		
		for (double value : percolation)
			percolationValues.add(value);
		
		PercolationCentrality task = new PercolationCentrality(net, percolationValues);
		NodeScore centrality = task.getResult();
		
		assertEquals( values.length, net.size() );
		for (int i=0; i<net.size(); i++)
			assertEquals ( values[i], centrality.get(i), EPSILON); 
	}
	
	@Test
	public void testPaperCase1 ()
	{	
		checkPercolationCentrality(network, networkPercolation, networkScores);
	}
	
	@Test
	public void testPaperCase2 ()
	{	
		checkPercolationCentrality(network, networkPercolation2, networkScores2);
	}
	
	@Test
	public void testZero ()
	{	
		checkPercolationCentrality(network, networkPercolationZero, networkScoresZero);
	}
	
	
	@Test
	public void testDisconnected ()
	{	
		checkPercolationCentrality(disconnected, disconnectedPercolation, disconnectedScores);
	}
	
	@Test
	public void testTwoNodes ()
	{	
		checkPercolationCentrality(twoNodes, twoNodesPercolation, twoNodesScores);
	}
	
	@Test
	public void testComplete ()
	{	
		checkPercolationCentrality(complete, completePercolation, completeScores);
	}
	
	@Test
	public void testStar ()
	{	
		checkPercolationCentrality(star, starPercolation, starScores);
	}
	
	@Test
	public void testTwoStars ()
	{	
		checkPercolationCentrality(twoStars, twoStarsPercolation, twoStarsScores);
	}
	
	@Test
	public void testTwoStars2 ()
	{	
		checkPercolationCentrality(twoStars, twoStarsPercolation2, twoStarsScores2);
	}
}
