package test.noesis.analysis.structure.links.prediction;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses( { test.noesis.analysis.structure.links.prediction.CommonNeighborsScoreTest.class,
						test.noesis.analysis.structure.links.prediction.AdamicAdarScoreTest.class,
						test.noesis.analysis.structure.links.prediction.ResourceAllocationScoreTest.class,})
public class AllTests {

}