package test.noesis.analysis.structure.links.prediction;

import static org.junit.Assert.assertEquals;
import ikor.math.Matrix;
import noesis.Network;
import noesis.analysis.structure.links.prediction.local.AdamicAdarScore;

import org.junit.Before;
import org.junit.Test;

public class AdamicAdarScoreTest extends LinkPredictionTest {

	public final double EPSILON = 1e-3;

	Network  network;

	@Before
	public void setUp() throws Exception 
	{
		network = buildNetwork();
	}
	
	@Test
	public void test() {
		AdamicAdarScore method = new AdamicAdarScore(network);
		Matrix score = method.call();
		
		assertEquals(3.2631735, score.get(0, 1), EPSILON);
		assertEquals(0.9102392, score.get(0, 2), EPSILON);
	}
	
}
