package test.noesis.analysis.structure.links.prediction;

import noesis.BasicNetwork;

public abstract class LinkPredictionTest {

	protected BasicNetwork buildNetwork() {
		BasicNetwork net = new BasicNetwork(); 
		net.setSize(5);
		
		net.add2(0, 2);
		net.add2(0, 3);
		net.add2(0, 4);
		
		net.add2(2, 3);
		
		net.add2(2, 1);
		net.add2(3, 1);
		net.add2(4, 1);
		
		return net;
	}
	
}
