package test.noesis.analysis.structure.links.prediction;

import static org.junit.Assert.assertEquals;
import ikor.math.Matrix;
import noesis.Network;
import noesis.analysis.structure.links.prediction.local.CommonNeighborsScore;

import org.junit.Before;
import org.junit.Test;

public class CommonNeighborsScoreTest extends LinkPredictionTest {

	public final double EPSILON = 1e-3;

	Network  network;

	@Before
	public void setUp() throws Exception 
	{
		network = buildNetwork();
	}
	
	@Test
	public void test() {
		CommonNeighborsScore method = new CommonNeighborsScore(network);
		Matrix score = method.call();
		
		assertEquals(3, score.get(0, 1), EPSILON);
		assertEquals(1, score.get(0, 2), EPSILON);
	}
	
}
