package test.noesis.analysis.structure;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import noesis.BasicNetwork;
import noesis.Network;
import noesis.analysis.NodeScore;
import noesis.analysis.structure.Coreness;
import noesis.model.regular.CompleteNetwork;
import noesis.model.regular.StarNetwork;

public class CorenessTest 
{	
	// Example from https://chaoslikehome.wordpress.com/2015/05/19/k-pruning-algorithm/
	Network  network;
	int[] networkCoreness = { 1, 3, 1, 3, 3, 3, 2, 2, 2, 1, 1, 1};
	
	// Example from https://doi.org/10.1039/C2MB25230A
	Network  network2;
	int[] network2Coreness = { 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 3, 3, 3, 3};
	
	Network  disconnected;
	int[] disconnectedCoreness = {0, 0, 0, 0, 0};

	Network  star;
	int[] starCoreness = {1, 1, 1, 1, 1};
	
	Network  complete;
	int[] completeCoreness = {5, 5, 5, 5, 5, 5};

	
	@Before
	public void setUp() throws Exception 
	{
		network = new BasicNetwork();
		network.setSize(12);

		network.add2(0, 1);
		network.add2(2, 1);
		
		network.add2(1, 3);
		network.add2(1, 4);
		network.add2(1, 5);
		network.add2(3, 4);
		network.add2(3, 5);
		network.add2(4, 5);
		
		network.add2(5, 6);
		network.add2(5, 7);
		network.add2(6, 7);
		
		network.add2(5, 8);
		network.add2(4, 8);
		
		network.add2(8, 9);
		
		network.add2(4, 10);
		network.add2(10, 11);
		
			
		network2 = new BasicNetwork();
		network2.setSize(14);
		
		network2.add2(0, 13);
		network2.add2(1, 10);
		network2.add2(2, 7);
		network2.add2(3, 5);
		network2.add2(4, 5);
		network2.add2(5, 6);
		network2.add2(5, 12);
		network2.add2(7, 10);
		network2.add2(7, 11);
		network2.add2(8, 9);
		network2.add2(8, 11);
		network2.add2(9, 11);
		network2.add2(10, 11);
		network2.add2(10, 12);
		network2.add2(10, 13);
		network2.add2(11, 12);
		network2.add2(11, 13);
		network2.add2(12, 13);
		
		
		disconnected = new BasicNetwork();
		disconnected.setSize(5);
		
		
		star = new StarNetwork(5);
		
		complete = new CompleteNetwork(6);
	}
	
	
	private void checkCoreness (Network net, int[] values)
	{
		Coreness task = new Coreness(net);
		NodeScore kshell = task.getResult();
		
		assertEquals( values.length, net.size() );
		for (int i=0; i<net.size(); i++)
			assertEquals ( values[i], (int) kshell.get(i)); 
	}
	
	@Test
	public void testExampleNetwork()
	{
		checkCoreness(network, networkCoreness);
	}
	
	@Test
	public void testExampleNetwork2()
	{
		checkCoreness(network2, network2Coreness);
	}
	
	@Test
	public void testDisconnected()
	{
		checkCoreness(disconnected, disconnectedCoreness);
	}
	
	@Test
	public void testStar()
	{
		checkCoreness(star, starCoreness);
	}
	
	@Test
	public void testComplete()
	{
		checkCoreness(complete, completeCoreness);
	}
}
