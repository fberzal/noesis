package test.noesis.analysis.structure;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import noesis.BasicNetwork;
import noesis.Network;
import noesis.analysis.NodeScore;
import noesis.analysis.structure.CrossCliqueConnectivity;
import noesis.model.regular.CompleteNetwork;
import noesis.model.regular.StarNetwork;

public class CrossCliqueConnectivityTest 
{
	Network netBowtie;
	Network empty;
	Network star;
	Network complete;
	
	@Before
	public void setUp() throws Exception 
	{
		netBowtie = createBowtie();
		empty = new BasicNetwork();
		star = new StarNetwork(4);
		complete = new CompleteNetwork(4);
	}
	
	public Network createBowtie() 
	{
		Network netBowtie = new BasicNetwork(); 
		netBowtie.setSize(7);
		
		netBowtie.add2(0, 1);
		netBowtie.add2(1, 2);
		netBowtie.add2(0, 2);

		netBowtie.add2(2, 3);
		netBowtie.add2(3, 4);

		netBowtie.add2(4, 5);
		netBowtie.add2(4, 6);
		netBowtie.add2(5, 6);
		
		return netBowtie;
	}
	
	@Test
	public void testBowtie()
	{
		CrossCliqueConnectivity cc = new CrossCliqueConnectivity(netBowtie);
		NodeScore score = cc.call();
		
		assertEquals(7, score.size());
		assertEquals(4, (int)score.get(0));
		assertEquals(4, (int)score.get(1));
		assertEquals(5, (int)score.get(2));
		assertEquals(3, (int)score.get(3));
		assertEquals(5, (int)score.get(4));
		assertEquals(4, (int)score.get(5));
		assertEquals(4, (int)score.get(6));
	}
	
	@Test
	public void testEmpty()
	{
		CrossCliqueConnectivity cc = new CrossCliqueConnectivity(empty);
		NodeScore score = cc.call();
		
		assertEquals(0, score.size());
	}
	
	@Test
	public void testStar()
	{
		CrossCliqueConnectivity cc = new CrossCliqueConnectivity(star);
		NodeScore score = cc.call();
		
		assertEquals(4, score.size());
		assertEquals(4, (int)score.get(0));
		assertEquals(2, (int)score.get(1));
		assertEquals(2, (int)score.get(2));
		assertEquals(2, (int)score.get(3));
	}
	
	@Test
	public void testComplete()
	{
		CrossCliqueConnectivity cc = new CrossCliqueConnectivity(complete);
		NodeScore score = cc.call();
		
		assertEquals(4, score.size());
		assertEquals(8, (int)score.get(0));
		assertEquals(8, (int)score.get(1));
		assertEquals(8, (int)score.get(2));
		assertEquals(8, (int)score.get(3));
	}
}
