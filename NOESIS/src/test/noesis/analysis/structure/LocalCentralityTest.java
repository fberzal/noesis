package test.noesis.analysis.structure;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import noesis.BasicNetwork;
import noesis.Network;
import noesis.analysis.NodeScore;
import noesis.analysis.structure.LocalCentrality;
import noesis.model.regular.CompleteNetwork;
import noesis.model.regular.StarNetwork;

public class LocalCentralityTest 
{	
	// Example from the original paper: https://doi.org/10.1016/j.physa.2011.09.017
	
	Network  network;
	int[] networkScores = {145, 92, 101, 92, 67, 104, 92, 101, 92, 111, 166, 157, 157,
		166, 156, 158, 158, 148, 119, 158, 148, 170, 200};
	
	Network  disconnected;
	int[] disconnectedScores = { 0, 0, 0, 0 };
	
	Network  star;
	int[] starScores = {9, 9, 9, 9};
	
	Network  complete;
	int[] completeScores = {64, 64, 64, 64, 64};
	
	
	@Before
	public void setUp() throws Exception 
	{
		network = new BasicNetwork();
		network.setSize(23);
		
		network.add2(0, 1);
		network.add2(0, 2);
		network.add2(0, 3);
		network.add2(0, 4);
		network.add2(0, 5);
		network.add2(0, 6);
		network.add2(0, 7);
		network.add2(0, 8);
		
		network.add2(1, 2);
		network.add2(2, 3);
		network.add2(6, 7);
		network.add2(7, 8);
		
		network.add2(5, 9);
		
		network.add2(9, 10);
		network.add2(9, 22);
		
		network.add2(10, 20);
		network.add2(10, 22);
		network.add2(10, 11);
		
		network.add2(20, 19);
		network.add2(20, 18);
		network.add2(20, 16);
		
		network.add2(18, 19);
		network.add2(18, 17);
		
		network.add2(16, 19);
		network.add2(16, 17);
		
		network.add2(15, 16);
		
		network.add2(19, 22);
		network.add2(22, 21);
		network.add2(22, 13);
		
		network.add2(11, 13);
		network.add2(11, 12);
		network.add2(11, 14);
		
		network.add2(12, 13);
		network.add2(12, 21);
		
		network.add2(13, 14);
		network.add2(12, 21);
		
		network.add2(21, 17);
		network.add2(21, 15);
		
		network.add2(14, 15);
		network.add2(12, 14);
		
		network.add2(17, 15);
		
		
		disconnected = new BasicNetwork();
		disconnected.setSize(4);
		
		star = new StarNetwork(4);
		
		complete = new CompleteNetwork(5);
	}
	
	private void checkLocalCentrality (Network net, int[] values)
	{
		LocalCentrality task = new LocalCentrality(net);
		NodeScore centrality = task.getResult();
		
		assertEquals( values.length, net.size() );
		for (int i=0; i<net.size(); i++)
			assertEquals ( values[i], (int) centrality.get(i)); 
	}
	
	@Test
	public void testNetwork()
	{
		checkLocalCentrality(network, networkScores);
	}
	
	@Test
	public void testDisconnected()
	{
		checkLocalCentrality(disconnected, disconnectedScores);
	}
	
	@Test
	public void testStar()
	{
		checkLocalCentrality(star, starScores);
	}
	
	@Test
	public void testComplete()
	{
		checkLocalCentrality(complete, completeScores);
	}
}
