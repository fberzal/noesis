package test.noesis.analysis.structure;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import noesis.Network;
import noesis.analysis.NodeScore;
import noesis.analysis.structure.OutDegree;
import noesis.analysis.structure.RobustnessCoefficient;
import noesis.model.regular.CompleteNetwork;
import noesis.model.regular.IsolateNetwork;
import noesis.model.regular.StarNetwork;

public class RobustnessCoefficientTest 
{

	public final double EPSILON = 1e-9;

	
	@Before
	public void setUp() throws Exception 
	{
	}
	
	private void checkRobustnessCoefficient (Network net, NodeScore values, double[] coeff)
	{
		RobustnessCoefficient task = new RobustnessCoefficient(net,values);
		NodeScore robustness = task.getResult();
		
		for (int i=0; i<net.size(); i++)
			assertEquals ( coeff[i], robustness.get(i), EPSILON); 
	}
	

	@Test
	public void testSingleNode ()
	{
		Network net = new CompleteNetwork(1);

		OutDegree degreeTask = new OutDegree(net);
		NodeScore degree = degreeTask.call();
		
		checkRobustnessCoefficient(net,degree, new double[] {1.0});
	}

	@Test
	public void testCompleteNetwork ()
	{
		Network net = new CompleteNetwork(5);

		OutDegree degreeTask = new OutDegree(net);
		NodeScore degree = degreeTask.call();
		
		checkRobustnessCoefficient(net,degree, new double[] {4.0, 3.0, 2.0, 1.0, 1.0});
	}

	@Test
	public void testDisconnectedNetwork ()
	{
		Network net = new IsolateNetwork(5);
		
		OutDegree degreeTask = new OutDegree(net);
		NodeScore degree = degreeTask.call();
		
		checkRobustnessCoefficient(net,degree, new double[] {1.0, 1.0, 1.0, 1.0, 1.0});
	}

	@Test
	public void testStarNetwork5 ()
	{
		Network net = new StarNetwork(5);
		
		OutDegree degreeTask = new OutDegree(net);
		NodeScore degree = degreeTask.call();
		
		checkRobustnessCoefficient(net,degree, new double[] {1.0, 4.0, 3.0, 2.0, 1.0});
	}

	@Test
	public void testStarNetwork10 ()
	{
		Network net = new StarNetwork(10);
		
		OutDegree degreeTask = new OutDegree(net);
		NodeScore degree = degreeTask.call();
		
		checkRobustnessCoefficient(net,degree, new double[] {1.0, 9.0, 8.0, 7.0, 6.0, 5.0, 4.0, 3.0, 2.0, 1.0});
	}
	
}
