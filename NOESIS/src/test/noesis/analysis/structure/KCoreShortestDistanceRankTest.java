package test.noesis.analysis.structure;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import noesis.BasicNetwork;
import noesis.Network;
import noesis.analysis.NodeScore;
import noesis.analysis.structure.KCoreShortestDistanceRank;
import noesis.model.regular.CompleteNetwork;
import noesis.model.regular.StarNetwork;

public class KCoreShortestDistanceRankTest 
{	
	// Example from https://doi.org/10.1016/j.physa.2013.10.047
	Network  network;
	int[] networkScore = {9,9,9,9,8,8,7,7,6,6,5,5,5,5,3,6,4,4,4,4,4,4,4,2,1,1};
	
	Network  empty;
	int[] emptyScore = {};
	
	Network  disconnected;
	int[] disconnectedScore = {1, 1, 1, 1, 1};

	Network  star;
	int[] starScore = {2, 1, 1, 1, 1};
	
	Network  complete;
	int[] completeScore = {1, 1, 1, 1, 1, 1};
	
	Network  threeComponents;
	int[] threeComponentsScore = {2, 1, 1, 2, 1, 1, 2, 1, 1};

	
	@Before
	public void setUp() throws Exception 
	{
		network = new BasicNetwork();
		network.setSize(26);
		
		network.add2(0, 1);
		network.add2(0, 2);
		network.add2(0, 3);
		network.add2(1, 2);
		network.add2(1, 3);
		network.add2(2, 3);
		
		network.add2(0, 4);
		network.add2(1, 4);
		
		network.add2(1, 5);
		network.add2(3, 5);
		
		network.add2(0, 6);
		network.add2(0, 7);
		network.add2(6, 7);
		
		network.add2(0, 8);
		network.add2(0, 9);
		
		network.add2(4, 10);
		network.add2(4, 11);
		
		network.add2(5, 12);
		network.add2(5, 13);
		network.add2(13, 14);
		
		network.add2(2, 15);
		
		network.add2(15, 16);
		network.add2(15, 17);
		network.add2(15, 18);
		network.add2(15, 19);
		network.add2(15, 20);
		network.add2(15, 21);
		network.add2(15, 22);
		
		network.add2(22, 23);
		
		network.add2(23, 24);
		network.add2(23, 25);

		
		empty = new BasicNetwork();
				
		disconnected = new BasicNetwork();
		disconnected.setSize(5);
		
		star = new StarNetwork(5);
		
		complete = new CompleteNetwork(6);
		
		
		threeComponents = new BasicNetwork();
		threeComponents.setSize(9);
		
		threeComponents.add2(0, 1);
		threeComponents.add2(0, 2);
		
		threeComponents.add2(3, 4);
		threeComponents.add2(3, 5);
		
		threeComponents.add2(6, 7);
		threeComponents.add2(6, 8);
	}
	
	private void checkKCoreShortestDistanceRank (Network net, int[] values)
	{
		KCoreShortestDistanceRank task = new KCoreShortestDistanceRank(net);
		NodeScore rank = task.getResult();
		
		assertEquals( values.length, net.size() );
		for (int i=0; i<net.size(); i++)
			assertEquals ( values[i], (int) rank.get(i)); 
	}
	
	@Test
	public void testExampleNetwork()
	{
		checkKCoreShortestDistanceRank(network, networkScore);
	}
	
	@Test
	public void testEmpty()
	{
		checkKCoreShortestDistanceRank(empty, emptyScore);
	}
	
	@Test
	public void testDisconnected()
	{
		checkKCoreShortestDistanceRank(disconnected, disconnectedScore);
	}
	
	@Test
	public void testStar()
	{
		checkKCoreShortestDistanceRank(star, starScore);
	}
	
	@Test
	public void testComplete()
	{
		checkKCoreShortestDistanceRank(complete, completeScore);
	}
	
	@Test
	public void testThreeComponents()
	{
		checkKCoreShortestDistanceRank(threeComponents, threeComponentsScore);
	}
}
