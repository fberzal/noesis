package test.noesis;

import static org.junit.Assert.*;
import noesis.Attribute;
import noesis.AttributeNetwork;
import noesis.LinkAttribute;

import org.junit.Before;
import org.junit.Test;


public class AttributeNetworkTest 
{
	private static final int NETWORK_SIZE = 10;
	private static final int NETWORK_LINKS = NETWORK_SIZE*(NETWORK_SIZE-1)/2;

	AttributeNetwork net;
	
	@Before
	public void setUp() throws Exception 
	{
		net = new AttributeNetwork();
		net.addNodeAttribute( new Attribute("node") );
		net.addLinkAttribute( new LinkAttribute(net,"link") );
		
		for (int i=0; i<NETWORK_SIZE; i++) {
			net.add(i);
			net.setNodeAttribute("node", i, "Node "+i);
		}

		for (int i=0; i<NETWORK_SIZE; i++) {
			for (int j=i+1; j<NETWORK_SIZE; j++) {
				net.add(i,j);
				net.setLinkAttribute("link", i, j, "Link "+i+"->"+j);
			}
		}
	}
	
	// Constructor
	
	@Test
	public void testConstructor() 
	{
		assertEquals( NETWORK_SIZE, net.size() );
		assertEquals( NETWORK_LINKS, net.links() );
		
		for (int i=0; i<NETWORK_SIZE; i++) {
			assertEquals ( i, net.inDegree(i) );
			assertEquals ( NETWORK_SIZE-i-1, net.outDegree(i) );
			
			assertEquals ( i, net.index(i) );
			assertEquals ( "Node "+i, net.getNodeAttribute("node").get(i));
			
			for (int j=0; j<net.inDegree(i); j++) {
				assertEquals ( j, net.inLink(i,j));
				assertEquals ( "Link "+j+"->"+i, net.getLinkAttribute("link").get(j,i));
			}
			
			for (int j=0; j<net.outDegree(i); j++) {
				assertEquals ( i+j+1, net.outLink(i,j));
				assertEquals ( "Link "+i+"->"+(i+j+1), net.getLinkAttribute("link").get(i,i+j+1));
			}
		}
		
		assertEquals ( null, net.getNodeAttribute("node").get(NETWORK_SIZE));
	}
	
	// Add

	@Test
	public void testAddNode ()
	{
		net.add(NETWORK_SIZE);
		net.setNodeAttribute("node", NETWORK_SIZE, "New node");
		
		assertEquals( NETWORK_SIZE+1, net.size() );
		assertEquals( NETWORK_LINKS, net.links() );
		
		for (int i=0; i<NETWORK_SIZE; i++) {
			assertEquals ( i, net.inDegree(i) );
			assertEquals ( NETWORK_SIZE-i-1, net.outDegree(i) );
			assertEquals ( "Node "+i, net.getNodeAttribute("node").get(i));
		}
		
		assertEquals ( 0, net.inDegree(NETWORK_SIZE));
		assertEquals ( 0, net.outDegree(NETWORK_SIZE));
		assertEquals ( NETWORK_SIZE, net.index(NETWORK_SIZE) );
		assertEquals ( "New node", net.getNodeAttribute("node").get(NETWORK_SIZE));
	}

	@Test
	public void testAddLinks ()
	{
		for (int i=1; i<NETWORK_SIZE; i++) {
			net.add(i,0);
			net.setLinkAttribute("link", i, 0, "New link "+i);
		}
		
		assertEquals( NETWORK_SIZE, net.size() );
		assertEquals( NETWORK_LINKS + (NETWORK_SIZE-1), net.links() );
		
		assertEquals ( NETWORK_SIZE-1, net.inDegree(0) );
		assertEquals ( NETWORK_SIZE-1, net.outDegree(0) );
		
		for (int i=1; i<NETWORK_SIZE; i++) {
			assertEquals ( i, net.inDegree(i) );
			assertEquals ( NETWORK_SIZE-i, net.outDegree(i) );
		}
		
		for (int i=1; i<NETWORK_SIZE; i++) {
			assertEquals ( "New link "+i, net.getLinkAttribute("link").get(i,0));
		}		
	}

	@Test
	public void testAddNodeWithLinks ()
	{
		net.add(NETWORK_SIZE);

		for (int i=0; i<NETWORK_SIZE; i++) {
			net.add(NETWORK_SIZE, i);
			net.add(i, NETWORK_SIZE);
			net.setLinkAttribute("link", i, NETWORK_SIZE, "New direct link "+i);
			net.setLinkAttribute("link", NETWORK_SIZE, i, "New reverse link "+i);
		}
		
		assertEquals( NETWORK_SIZE + 1, net.size() );
		assertEquals( NETWORK_LINKS + 2*NETWORK_SIZE, net.links() );
		
		for (int i=0; i<NETWORK_SIZE; i++) {
			assertEquals ( i+1, net.inDegree(i) );
			assertEquals ( NETWORK_SIZE-i, net.outDegree(i) );
		}
		
		assertEquals ( NETWORK_SIZE, net.inDegree(NETWORK_SIZE));
		assertEquals ( NETWORK_SIZE, net.outDegree(NETWORK_SIZE));
		
		for (int i=0; i<NETWORK_SIZE; i++) {
			assertEquals ( "New direct link "+i, net.getLinkAttribute("link").get(i,NETWORK_SIZE));
			assertEquals ( "New reverse link "+i, net.getLinkAttribute("link").get(NETWORK_SIZE,i));
		}		
	}

	// Remove
	
	@Test
	public void testRemoveNodeWithOutLinks ()
	{
		net.remove(0);
		
		assertEquals( NETWORK_SIZE-1, net.size() );
		assertEquals( NETWORK_LINKS -(NETWORK_SIZE-1), net.links() );
		assertEquals( NETWORK_LINKS -(NETWORK_SIZE-1), net.getLinkAttribute("link").size() );

		// Previous last node, now at removed node position
		
		assertEquals ( (NETWORK_SIZE-1)-1, net.inDegree(0) );
		assertEquals ( 0, net.outDegree(0) );
		assertEquals ( "Node "+(NETWORK_SIZE-1), net.getNodeAttribute("node").get(0));

		// Remaining nodes

		for (int i=1; i<NETWORK_SIZE-1; i++) {
			assertEquals ( i-1, net.inDegree(i) );
			assertEquals ( NETWORK_SIZE-i-1, net.outDegree(i) );
			assertEquals ( "Node "+i, net.getNodeAttribute("node").get(i));
		}		
		
		// Adjusted links
		
		for (int i=0; i<net.inDegree(0); i++) {
			assertEquals ( i+1, net.inLink(0,i));
			assertEquals ( "Link "+(i+1)+"->"+(NETWORK_SIZE-1), net.getLinkAttribute("link").get(i+1,0));
		}
		
		for (int i=1; i<NETWORK_SIZE-1; i++) {
			assertEquals ( 0, net.outLink(i, net.outDegree(i)-1 ));
			assertEquals ( "Link "+i+"->"+(NETWORK_SIZE-1), net.getLinkAttribute("link").get(i,0));
		}
	}

	@Test
	public void testRemoveNodeWithInLinks ()
	{
		net.remove(NETWORK_SIZE-1);
		
		assertEquals( NETWORK_SIZE-1, net.size() );
		assertEquals( NETWORK_LINKS-(NETWORK_SIZE-1), net.links() );

		for (int i=0; i<NETWORK_SIZE-1; i++) {
			assertEquals ( i, net.inDegree(i) );
			assertEquals ( NETWORK_SIZE-i-2, net.outDegree(i) );
			assertEquals ( "Node "+i, net.getNodeAttribute("node").get(i));
		}
	}
	
	@Test
	public void testClear ()
	{
		net.clear();

		assertEquals( 0, net.size() );
		assertEquals( 0, net.links() );
		
		assertEquals( 0, net.getNodeAttribute("node").size() );
		assertEquals( 0, net.getLinkAttribute("link").size() );
	}

	@Test
	public void testAddNodeAndRemove ()
	{
		net.add(NETWORK_SIZE);
		net.setNodeAttribute("node", NETWORK_SIZE, "New node");

		for (int i=0; i<NETWORK_SIZE; i++) {
			net.add(NETWORK_SIZE, i);
			net.add(i, NETWORK_SIZE);
			net.setLinkAttribute("link", i, NETWORK_SIZE, "New direct link "+i);
			net.setLinkAttribute("link", NETWORK_SIZE, i, "New reverse link "+i);
		}

		assertEquals( NETWORK_SIZE+1, net.size() );
		assertEquals( NETWORK_LINKS+2*NETWORK_SIZE, net.links() );
		assertEquals( NETWORK_SIZE+1, net.getNodeAttribute("node").size() );
		assertEquals( NETWORK_LINKS+2*NETWORK_SIZE, net.getLinkAttribute("link").size() );
		assertEquals ( "New node", net.getNodeAttribute("node").get(NETWORK_SIZE));
		
		net.remove(NETWORK_SIZE);

		assertEquals( NETWORK_SIZE, net.size() );
		assertEquals( NETWORK_LINKS, net.links() );
		assertEquals( NETWORK_SIZE, net.getNodeAttribute("node").size() );
		assertEquals( NETWORK_LINKS, net.getLinkAttribute("link").size() );
		
		for (int i=0; i<NETWORK_SIZE; i++) {
			assertEquals ( i, net.inDegree(i) );
			assertEquals ( NETWORK_SIZE-i-1, net.outDegree(i) );
		}
		
		assertEquals ( null, net.getNodeAttribute("node").get(NETWORK_SIZE));		
	}
	
	@Test
	public void testAddNodeAndClear ()
	{
		net.add(NETWORK_SIZE);
		net.setNodeAttribute("node", NETWORK_SIZE, "New node");

		for (int i=0; i<NETWORK_SIZE; i++) {
			net.add(NETWORK_SIZE, i);
			net.add(i, NETWORK_SIZE);
			net.setLinkAttribute("link", i, NETWORK_SIZE, "New direct link "+i);
			net.setLinkAttribute("link", NETWORK_SIZE, i, "New reverse link "+i);			
		}

		assertEquals( NETWORK_SIZE+1, net.size() );
		assertEquals( NETWORK_LINKS+2*NETWORK_SIZE, net.links() );
		assertEquals( NETWORK_SIZE+1, net.getNodeAttribute("node").size() );
		assertEquals( NETWORK_LINKS+2*NETWORK_SIZE, net.getLinkAttribute("link").size() );
		
		assertEquals ( "New node", net.getNodeAttribute("node").get(NETWORK_SIZE));
		
		net.clear();

		assertEquals( 0, net.size() );
		assertEquals( 0, net.links() );
		assertEquals( 0, net.getNodeAttribute("node").size() );
		assertEquals( 0, net.getLinkAttribute("link").size() );
		
		assertEquals ( null, net.getNodeAttribute("node").get(NETWORK_SIZE));				
	}


	@Test
	public void testAddAndRemoveLinks ()
	{
		for (int i=1; i<NETWORK_SIZE; i++) {
			net.add(i,0);
			net.setLinkAttribute("link", i, 0, "New link "+i);
		}

		assertEquals( NETWORK_SIZE, net.size() );
		assertEquals( NETWORK_LINKS+NETWORK_SIZE-1, net.links() );
		assertEquals( NETWORK_SIZE, net.getNodeAttribute("node").size() );
		assertEquals( NETWORK_LINKS+NETWORK_SIZE-1, net.getLinkAttribute("link").size() );

		for (int i=1; i<NETWORK_SIZE; i++)
			net.remove(i,0);
		
		assertEquals( NETWORK_SIZE, net.size() );
		assertEquals( NETWORK_LINKS, net.links() );
		assertEquals( NETWORK_SIZE, net.getNodeAttribute("node").size() );
		assertEquals( NETWORK_LINKS, net.getLinkAttribute("link").size() );
		
		for (int i=0; i<NETWORK_SIZE; i++) {
			assertEquals ( i, net.inDegree(i) );
			assertEquals ( NETWORK_SIZE-i-1, net.outDegree(i) );
		}
	}

	@Test
	public void testRemoveFirstLinks ()
	{
		for (int i=0; i<NETWORK_SIZE-1; i++)
			net.remove(i, i+1);
		
		assertEquals( NETWORK_SIZE, net.size() );
		assertEquals( NETWORK_LINKS-(NETWORK_SIZE-1), net.links() );
		assertEquals( NETWORK_SIZE, net.getNodeAttribute("node").size() );
		assertEquals( NETWORK_LINKS-(NETWORK_SIZE-1), net.getLinkAttribute("link").size() );
		
		assertEquals ( 0, net.inDegree(0) );
		assertEquals ( NETWORK_SIZE-2, net.outDegree(0) );

		for (int i=1; i<NETWORK_SIZE-1; i++) {
			assertEquals ( i-1, net.inDegree(i) );
			assertEquals ( NETWORK_SIZE-i-2, net.outDegree(i) );
		}

		assertEquals ( NETWORK_SIZE-2, net.inDegree(NETWORK_SIZE-1) );
		assertEquals ( 0, net.outDegree(NETWORK_SIZE-1) );
	}

	@Test
	public void testRemoveLastLinks ()
	{
		for (int i=0; i<NETWORK_SIZE-1; i++)
			net.remove(i, NETWORK_SIZE-1);
		
		assertEquals( NETWORK_SIZE, net.size() );
		assertEquals( NETWORK_LINKS-(NETWORK_SIZE-1), net.links() );
		assertEquals( NETWORK_SIZE, net.getNodeAttribute("node").size() );
		assertEquals( NETWORK_LINKS-(NETWORK_SIZE-1), net.getLinkAttribute("link").size() );
		
		assertEquals ( 0, net.inDegree(0) );
		assertEquals ( NETWORK_SIZE-2, net.outDegree(0) );

		for (int i=1; i<NETWORK_SIZE-1; i++) {
			assertEquals ( i, net.inDegree(i) );
			assertEquals ( NETWORK_SIZE-i-2, net.outDegree(i) );
		}

		assertEquals ( 0, net.inDegree(NETWORK_SIZE-1) );
		assertEquals ( 0, net.outDegree(NETWORK_SIZE-1) );
	}
	
}
