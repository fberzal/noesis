![customLogo.gif](https://bitbucket.org/repo/ejpajK/images/1989966575-customLogo.gif)

# What is NOESIS? #

The **NOESIS** open-source framework for **network data mining** provides a large collection of network analysis techniques, including the analysis of network structural properties, community detection methods, link scoring and prediction techniques, and network visualization algorithms. NOESIS includes a complete graphical user interface, but it can also be included in other software projects as a lightweight library, since it is distributed under a permissive BSD license.

# Project web page #

[http://noesis.ikor.org/](http://noesis.ikor.org)

# Main features #

* Several network analysis measures: degree, eccentricity, average path length, closeness, decay, betweenness, PageRank, HITS, eigenvector centrality, Katz centrality, clustering coefficient, connected components, link betweenness, link embeddedness, link neighborhood overlap...

* Different supported network file formats: GML, GraphML, and GDF, among others. Networks can be exported to some image formats, including SVG, PNG, and JPEG format.

* A large number of random network models: Erdös-Renyi, Gilbert, Watts-Strogatz, Barabasi-Albert, and Price models. Other models, including regular models, are also available.

* Different community detection methods: Kernighan-Lin, Newman-Girvan, Radicchi, hierarchical, fast greedy, spectral, or BigCLAM community detection, among others.

* Several link scoring and prediction scores, such as Adamic-Adar score, Resource Allocation index, Katz score, or different random walk-based metrics.

# Graphical user interface #

Even though NOESIS is provided as a library to be reused in other software development projects, it also includes an easy-to-use graphical user interface. The NOESIS Network Analyzer can be used as an alternative to well-known SNA tools such as Gephi, Pajek, UCINET, or NodeXL. You can load and visualize your own networks and use all the data mining techniques implemented in the NOESIS framework using its GUI. Network visualization can be customized for nodes and links based on their attributes and different network layout algorithms are also available.

![NOESIS Network Analyzer](https://bitbucket.org/repo/ejpajK/images/3354227262-3294960002-noesis.png)


# External dependencies #

The NOESIS framework relies on the [iKor library](https://bitbucket.org/fberzal/ikor) of reusable software components, which provides a customizable collection framework, support for the execution of parallel algorithms, mathematical routines, and the application generator used to build the NOESIS GUI. This is the only external dependency of the NOESIS framework, a deliberate decision to keep it lightweight and facilitate its integration in other software projects.


# Software license #

NOESIS is distributed under the Simplified BSD License below:


> Copyright (c) 2015, Fernando Berzal (berzal@acm.org) & Víctor Martínez (victormg@acm.org).
> All rights reserved.
>
> Redistribution and use in source and binary forms, with or without
> modification, are permitted provided that the following conditions are met:
>
> 1. Redistributions of source code must retain the above copyright notice, this
>    list of conditions and the following disclaimer.
> 2. Redistributions in binary form must reproduce the above copyright notice,
>    this list of conditions and the following disclaimer in the documentation
>    and/or other materials provided with the distribution.
>
> THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
> ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
> WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
> DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
> ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
> (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
> LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
> ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
> (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
> SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.



BSD licenses are a family of permissive free software licenses, imposing minimal restrictions on the redistribution of software. This license allows unlimited redistribution for any purpose as long as its copyright notices and the license's disclaimers of warranty are maintained. BSD Licenses allow proprietary use and allow the software released under the license to be incorporated into proprietary products (i.e. works based on this material may be released under a proprietary license as closed source software). See [Wikipedia](https://en.wikipedia.org/wiki/BSD_licenses).